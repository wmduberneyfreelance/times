<?php

namespace App;

use App\Account;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
        'name', 'id_client', 'status'
    ];

    public static function filterByClient(){
        $accounts = \DB::table('accounts')
        ->select(
            'accounts.*',
            'clients.name AS nameClient'
        )->orderBy('id', 'desc')
        ->join('clients', 'clients.id', '=', 'accounts.id_client')
        ->get();

        return $accounts;
    }

    public static function filterAndPaginateByClient($idClient, $itemPag){
      $accounts = \DB::table('accounts')
      ->select(
          'accounts.*'
      )
      ->where('accounts.id_client', 'LIKE', $idClient)
      ->orderBy('id', 'desc')
      // ->join('clients', 'clients.id', '=', 'accounts.id_client')
      ->paginate($itemPag);

      return $accounts;
    }

    public static function filterAndPaginate($name, $itemPag){
    	// return $accounts = Account::name($name)->orderBy('id', 'desc')->paginate($itemPag);

        if ($name == null){
            $accounts = \DB::table('accounts')
            ->select(
                'accounts.*',
                'clients.name AS nameClient'
            )->orderBy('id', 'desc')
            ->join('clients', 'clients.id', '=', 'accounts.id_client')
            ->paginate($itemPag);
        }else{
            $accounts = \DB::table('accounts')
            ->select(
                'accounts.*',
                'clients.name AS nameClient'
            )
            ->where('accounts.name', 'LIKE', "%$name%")
            ->orderBy('id', 'desc')
            ->join('clients', 'clients.id', '=', 'accounts.id_client')
            ->paginate($itemPag);
        }

        return $accounts;

    }

    public function scopeName($query, $name){
    	if (trim($name) != "")
    		$query->where('name', 'LIKE', "%$name%");
    }
}
