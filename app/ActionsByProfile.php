<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActionsByProfile extends Model
{

	protected $table = 'actions_by_profile';

    protected $fillable = [
        'id_profile', 'id_action'
    ];
}
