<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActionsProfile extends Model
{
    //
    protected $fillable = [
        'action', 'funcionality',
    ];

}
