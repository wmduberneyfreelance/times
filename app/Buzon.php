<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buzon extends Model
{
  protected $table = 'buzon';

  protected $fillable = [
    'id_sending_user', 'id_receiving_user', 'message', 'hours', 'readed', 'date_send'
  ];
}
