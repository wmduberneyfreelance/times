<?php

namespace App;

use App\Client;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{

    protected $fillable = [
        'name', 'status'
    ];

    public static function filterAndPaginate($name, $itemPag){
    	return $profiles = Client::name($name)->orderBy('id', 'asc')->paginate($itemPag);
    }

    public function scopeName($query, $name){
    	if (trim($name) != "")
    		$query->where('name', 'LIKE', "%$name%");
    }
}
