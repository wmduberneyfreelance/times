<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	protected $table = 'comments';

  protected $fillable = [
    'id_user', 'id_ot', 'name_user', 'comments'
  ];
}
