<?php

namespace App\Http\Controllers\Admin;

use App\Account;
use App\Client;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CreateAccountRequest;
use App\Http\Requests\EditAccountRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AccountController extends Controller
{
    /**
     * Cargar listadado de usuarios
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $accounts = Account::filterAndPaginate($request->get('searchNameAccount'), 15);
        $data = array('accounts' => $accounts);

        return view('admin.accounts.showAccounts', $data);
    }

    public function create()
    {
        $clients = Client::all();
        $data = array(
            'clients' => $clients
        );

        return view('admin.accounts.addAccount', $data);
    }

    public function store(CreateAccountRequest $request)
    {
        $newAccount = Account::create([
            'name' => $request->nameAccount, 
            'id_client' => $request->idClient, 
            'status' => $request->stateAccount,
        ]);
        return redirect()->route('loadaccounts');
    }

    public function show($id)
    {
        $account = Account::find($id);
        $clients = Client::all();

        if ($account == null)
            return redirect()->route('loadaccounts');

        $data = array(
            'clients' => $clients,
            'account' => $account, 
            'idAccountUpdate' => $id,
        );

        return view('admin.accounts.editAccount', $data);
    }

    public function update(EditAccountRequest $request, $id)
    {
        $account = Account::findOrFail($id);

        $newData = [
            'name' => $request->nameAccount,
            'id_client' => $request->idClient,
            'status' => $request->stateAccount
        ];

        $account->fill($newData);
        $account->save();

        return redirect()->route('loadaccounts');

    }

    public function destroy($id, Request $request)
    {
        $account = Account::findOrFail($id);
        $account->delete();

        $messageSuc = 'La cuenta "'. $account->name .'" fue eliminada satisfactoriamente.';

        if ($request->ajax()){
            return response()->json([
                'id' => $client->id,
                'message' => $messageSuc
            ]);
        }

        Session::flash('message', $messageSuc); //La información solo se carga la primera vez en la pagina
        return redirect()->route('loadaccounts');
    }
}
