<?php

namespace App\Http\Controllers\Admin;

use App\Client;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CreateClientRequest;
use App\Http\Requests\EditClientRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ClientController extends Controller
{
    public function index(Request $request)
    {
        $clients = Client::filterAndPaginate($request->get('searchNameClient'), 15);
        $data = array('clients' => $clients);

        return view('admin.clients.showClients', $data);
    }

    public function create()
    {
        return view('admin.clients.addClient');
    }

    public function store(CreateClientRequest $request)
    {
        $newClient = Client::create([
            'name' => $request->nameClient, 
            'status' => $request->stateClient
        ]);
        return redirect()->route('loadclients');
    }

    public function show($id)
    {
        $client = Client::find($id);

        if ($client == null)
            return redirect()->route('loadclients');

        $data = array(
            'client' => $client, 
            'idClientUpdate' => $id,
        );

        return view('admin.clients.editClient', $data);
    }

    public function update(EditClientRequest $request, $id)
    {
        $client = Client::findOrFail($id);

        $newData = [
            'name' => $request->nameClient,
            'status' => $request->stateClient
        ];

        $client->fill($newData);
        $client->save();

        return redirect()->route('loadclients');

    }

    public function destroy($id, Request $request)
    {
        $client = Client::findOrFail($id);
        $client->delete();

        $messageSuc = 'El cliente "'. $client->name .'" fue eliminado satisfactoriamente.';

        if ($request->ajax()){
            return response()->json([
                'id' => $client->id,
                'message' => $messageSuc
            ]);
        }

        Session::flash('message', $messageSuc); //La información solo se carga la primera vez en la pagina
        return redirect()->route('loadclients');
    }
}
