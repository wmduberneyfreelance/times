<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CreateOtTypeRequest;
use App\Http\Requests\EditOtTypeRequest;
use App\OtType;
use App\MetadataOtType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OtTypesController extends Controller
{
  public function index(Request $request)
  {
    $projectsTypes = OtType::filterAndPaginate($request->get('searchNameTypeP'), 15);
    $data = array('projectsTypes' => $projectsTypes);
    return view('admin.ot_types.showOtTypes', $data);
  }

  public function create()
  {
    return view('admin.ot_types.addOtType');
  }

  private function getStringValuesField($obj, $type){
    $totalValues = count($obj);
    $strRet = "";

    for ($i=0; $i<$totalValues; $i++){
      if ($type == "values"){
        $strRet .= $obj[$i]->value . ",";
      }else if ($type == "texts"){
        $strRet .= $obj[$i]->text . ",";
      }
    }

    $strRet = substr($strRet, 0, -1);

    return $strRet;
  }
  
  public function detailottype(Request $request){
    $keyF = $request->keyType;
    if ($keyF != ""){
      $metaDataFields = MetadataOtType::where('id_ot_type', $keyF)->orderBy('id', 'asc')->get();
      return response()->json(['message' => 'success', 'dataFields' => $metaDataFields]);
    }else{
      return response()->json(['message' => 'no_data']);
    }
  }

  public function store(CreateOtTypeRequest $request)
  {
    $newProjectType = OtType::create([
      'name' => $request->nameProType, 
      'email' => $request->emailDeparmentProType, 
      'deparment' => $request->deparmentProType, 
      'description' => $request->descriptionProType, 
      'status' => $request->stateProType,
    ])->id;

    $fieldsOtType = (array) json_decode($request->fieldsOtType);
    $totalFields = count($fieldsOtType);
    for ($i=0; $i<$totalFields; $i++){
      $nameF = $fieldsOtType[$i]->name;
      $typeF = $fieldsOtType[$i]->type;
      $posiF = $fieldsOtType[$i]->position;
      $valuF = "";
      $textF = "";
      if ($typeF == "select" || $typeF == "select_mult" || $typeF == "checkbox" || $typeF == "radio_button"){
        $valuF = $this->getStringValuesField($fieldsOtType[$i]->values, "values");
        $textF = $this->getStringValuesField($fieldsOtType[$i]->values, "texts");
      }

      $newMetaDataOtType = MetadataOtType::create([
        'id_ot_type' => $newProjectType, 
        'name' => $nameF, 
        'type' => $typeF, 
        'position' => $posiF, 
        'values' => $valuF, 
        'texts' => $textF
      ]);
    }

    // echo "<pre>";
    // print_r($fieldsOtType);
    // echo "</pre>";

    return redirect()->route('loadtypesot');
  }

  public function show($id)
  {
    $projectType = OtType::find($id);
    

    if ($projectType == null)
      return redirect()->route('loadtypesot');

    $data = array(
        'projectType' => $projectType, 
        'idProjectTypeUpdate' => $id,
    );

    return view('admin.ot_types.editOtType', $data);
  }

  public function update(EditOtTypeRequest $request, $id)
  {
    $projectType = OtType::findOrFail($id);

    $newData = [
      'name' => $request->nameProType,
      'description' => $request->descriptionProType,
      'status' => $request->stateProType
    ];

    $projectType->fill($newData);
    $projectType->save();

    return redirect()->route('loadtypesot');
  }

  public function destroy($id, Request $request)
  {
    $projectType = OtType::findOrFail($id);
    $projectType->delete();

    $messageSuc = 'La tipo de proyecto "'. $projectType->name .'" fue eliminado satisfactoriamente.';

    if ($request->ajax()){
      return response()->json([
        'id' => $client->id,
        'message' => $messageSuc
      ]);
    }

    Session::flash('message', $messageSuc); //La información solo se carga la primera vez en la pagina
    return redirect()->route('loadtypesot');
  }

}
