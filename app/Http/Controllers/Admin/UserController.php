<?php

namespace App\Http\Controllers\Admin;

use App\ActionsByProfile;
use App\ActionsProfile;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\EditUserRequest;
use App\Profile;
use App\User;
use Auth;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller
{
  public function demoExcel(Request $request){
    // \Excel::load('files/example.xlsx', function($reader) {
    //     // reader methods
    // });

    $excel = \Excel::load('files/ipc_37.xlsx');
    $data = array('dataExcel' => $excel->toArray());

    return view('admin.showImportExcel', $data);
  }

  /**
   * Cargar listadado de usuarios
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $users = User::filterAndPaginate($request->get('searchNameUser'), 15);
    $data = array('users' => $users);

    return view('admin.showUsers', $data);
  }

  /**
   * Crear nuevo usuario
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $profiles = Profile::select(['id', 'name'])->get();
    $data = array('profiles' => $profiles);

    return view('admin.addUser', $data);
  }

  /**
   * Guardar nuevo usuario
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(CreateUserRequest $request)
  {
    $newUser = User::create([
      'id_team' => '0',
      'name' => $request->nameUser, 
      'position' => $request->positionUser,
      'gender' => $request->genderUser,
      'email' => $request->emailUser, 
      'password' => bcrypt($request->passwordUser), 
      'rol' => $request->profileUser, 
      'id_profile' => null, 
      'status' => $request->stateUser
    ]);

    return redirect()->route('loadusers');
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(EditUserRequest $request, $id)
  {
    $user = User::findOrFail($id);
    if ($request->passwordUser != "" && $request->passwordUser != null){
      $newData = [
        'name' => $request->nameUser,
        'position' => $request->positionUser,
        'gender' => $request->genderUser,
        'email' => $request->emailUser, 
        'password' => bcrypt($request->passwordUser), 
        'status' => $request->stateUser,
        'rol' => $request->profileUser
      ];
    }else{
      $newData = [
        'name' => $request->nameUser,
        'position' => $request->positionUser,
        'gender' => $request->genderUser,
        'email' => $request->emailUser, 
        'status' => $request->stateUser,
        'rol' => $request->profileUser
      ];
    }
    $user->fill($newData);
    $user->save();

    return redirect()->route('loadusers');
  }

  /**
   * Cargar detalle de un usuario suministrando el ID.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $user = User::find($id);

    if ($user == null)
      return redirect()->route('loadusers');

    // 'user', 'executive', 'developer', 'creative', 'moderator', 'admin', 'superadmin'

    $dataProfile = Profile::where('id', $user->id_profile)->get();
    $profiles = Profile::select(['id', 'name'])->get();

    $data = array(
        'dataProfile' => $dataProfile, 
        'profiles' => $profiles,
        'user' => $user, 
        'idUserUpdate' => $id,
    );

    return view('admin.editUser', $data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }

  public function destroyProfile($id, Request $request)
  {
    $profile = Profile::findOrFail($id);
    $profile->delete();

    $messageSuc = 'El perfil "'. $profile->name .'" fue eliminado satisfactoriamente.';

    if ($request->ajax()){
      return response()->json([
          'id' => $profile->id,
          'message' => $messageSuc
      ]);
      // return $messageSuc;
    }

    // Profile::destroy($id);
    // Session::set('message', 'El registro fue eliminado.'); //La información persiste
    Session::flash('message', $messageSuc); //La información solo se carga la primera vez en la pagina

    return redirect('admin');
  }
}
