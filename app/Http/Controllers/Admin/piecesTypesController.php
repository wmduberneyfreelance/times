<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CreatePieceTypeRequest;
use App\Http\Requests\EditPieceTypeRequest;
use App\PiecesTypes;
use App\MetadataOtType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PiecesTypesController extends Controller
{
  public function index(Request $request){
    $piecesTypes = PiecesTypes::filterAndPaginate($request->get('searchNameTypeP'), 15);
    $data = array('piecesTypes' => $piecesTypes);
    return view('admin.pieces_types.showPiecesTypes', $data);
  }


  public function create(){
    return view('admin.pieces_types.addPieceType');
  }
  
  public function store(CreatePieceTypeRequest $request){
    $newPieceType = PiecesTypes::create([
      'name' => $request->namePieceType, 
      'description' => $request->descriptionPieceType, 
      'status' => $request->statePieceType
    ])->id;
    
    Session::flash('message', 'El tipo de pieza ha sido creado <strong>satisfactoriamente</strong>');

    return redirect()->route('loadtypespieces');
  }

  public function show($id){
    $pieceType = PiecesTypes::find($id);

    if ($pieceType == null)
      return redirect()->route('loadtypespieces');

    $data = array(
      'pieceType' => $pieceType, 
      'idPieceTypeUpdate' => $id
    );

    return view('admin.pieces_types.editPieceType', $data);
  }

  public function update(EditPieceTypeRequest $request, $id){
    $pieceType = PiecesTypes::findOrFail($id);

    $newData = [
      'name' => $request->namePieceType,
      'description' => $request->descriptionPieceType,
      'status' => $request->statePieceType
    ];

    $pieceType->fill($newData);
    $pieceType->save();

    return redirect()->route('loadtypespieces');
  }

  public function destroy($id, Request $request){
    $pieceType = PiecesTypes::findOrFail($id);
    $pieceType->delete();

    $messageSuc = 'El tipo de pieza "'. $pieceType->name .'" fue eliminado satisfactoriamente.';

    if ($request->ajax()){
        return response()->json([
            'id' => $client->id,
            'message' => $messageSuc
        ]);
    }

    Session::flash('message', $messageSuc); //La información solo se carga la primera vez en la pagina
    return redirect()->route('loadtypespieces');
  }
}
