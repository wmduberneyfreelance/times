<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditProfileRequest;
use App\Http\Requests\CreateProfileRequest;
use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ActionsProfile;
use App\ActionsByProfile;
use App\Profile;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
// use Illuminate\Support\Facades\Redirect;


class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $profiles = Profile::filterAndPaginate($request->get('searchNameProfile'), 15);

        // $profiles = Profile::name($request->get('searchNameProfile'))->orderBy('id', 'desc')->paginate(2);
        // $profiles = Profile::all()->orderBy('name', 'desc');
        // $profiles = Profile::orderBy('id', 'desc')->paginate(5);
        // $profiles = Profile::paginate(5);
        // $profiles = \DB::table('profiles')->orderBy('id', 'desc')->paginate(15);
        
        // dd($profiles);
        
        $data = array('profiles' => $profiles);
        return view('admin.showProfiles', $data);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permits = ActionsProfile::all();
        $actions = ActionsProfile::select('action')->groupBy('action')->get();
        $funcionalities = ActionsProfile::select('funcionality')->groupBy('funcionality')->get();

        $data = array('permits' => $permits, 'actions' => $actions, 'funcionalities' => $funcionalities);

        return view('admin.addProfile', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    // public function updateProfile(EditProfileRequest $request, $id)
    public function updateProfile(EditProfileRequest $request, $id)
    {
        $profile = Profile::findOrFail($id);

        $newData = [
            'id_team' => '0',
            'name' => $request->nameProfile, 
            'description' => $request->descriptionProfile, 
            'status' => $request->stateProfile
        ];


        //Guardo Perfil
        $profile->fill($newData);
        $profile->save();

        $deleteAction = ActionsByProfile::where('id_profile', $id)->delete();

        if ($request->permsSelect != null){
            $myActions = explode(',', $request->permsSelect);

            // $prevAction->delete();
            // dd($prevAction);

            for ($i=0; $i<count($myActions);  $i++){
                $newActionsByProfile = ActionsByProfile::create([
                    'id_profile' => $id,
                    'id_action' => $myActions[$i]
                ]);
            }
        }

        return redirect()->back();
    }

    /***** Validación con inyección de dependencias *******/
    public function storeProfile(CreateProfileRequest $request)
    // public function storeProfile(Request $request)
    {

        // $rules = [
        //     'nameProfile' => 'required|unique:profiles,name',
        //     'stateProfile' => 'required|in:active,inactive',
        //     'descriptionProfile' => 'required|min:8',
        // ];

        /***** Validación con inyección de dependencias *******/
        // $this->validate($request, $rules);


        /***** Validación con facade *******/
        // $data = $request->all();
        // $validator = Validator::make($data, $rules);

        // if ($validator->fails()){
        //     return \Redirect::back()
        //         // ->withErrors($validator->errors())
        //         ->withErrors($validator->messages())
        //         ->withInput();
        // }

        $newProfile = Profile::create([
            'id_team' => '0',
            'name' => $request->nameProfile, 
            'description' => $request->descriptionProfile, 
            'status' => $request->stateProfile
        ]);

        if ($request->permsSelect != null){
            $myActions = explode(',', $request->permsSelect);

            for ($i=0; $i<count($myActions);  $i++){
                $newActionsByProfile = ActionsByProfile::create([
                    'id_profile' => $newProfile->id,
                    'id_action' => $myActions[$i]
                ]);
            }
        } 

        return redirect('admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function showProfile($id)
    {
        $profile = Profile::find($id);
        if ($profile == null)
            return redirect('admin');


        $permits = ActionsProfile::all();
        $actions = ActionsProfile::select('action')->groupBy('action')->get();
        $actionsProfile = ActionsByProfile::where('id_profile', $id)->get();
        $funcionalities = ActionsProfile::select('funcionality')->groupBy('funcionality')->get();


        $data = array(
            'profile' => $profile, 
            'permits' => $permits, 
            'actions' => $actions, 
            'funcionalities' => $funcionalities, 
            'actionsProfile' => $actionsProfile,
            'idProfileUpdate' => $id,
        );

        return view('admin.editProfile', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function destroyProfile($id, Request $request)
    {
        // abort(500);

        $profile = Profile::findOrFail($id);
        $profile->delete();

        $messageSuc = 'El perfil "'. $profile->name .'" fue eliminado satisfactoriamente.';

        if ($request->ajax()){
            return response()->json([
                'id' => $profile->id,
                'message' => $messageSuc
            ]);
            // return $messageSuc;
        }

        // Profile::destroy($id);
        // Session::set('message', 'El registro fue eliminado.'); //La información persiste
        Session::flash('message', $messageSuc); //La información solo se carga la primera vez en la pagina


        return redirect('admin');

    }
}
