<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon;
use Auth;
use App\Post;
use App\Http\Requests;
use App\Http\Requests\CreatePostRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class BlogController extends Controller
{

	public function index(){
	  $user = Auth::user();

	  $todayD = Carbon\Carbon::now();
	  $today = Carbon\Carbon::parse($todayD)->formatLocalized('%A');
	  $today = $this->getNameDay($today);
	  $month = $this->getNameMonth(($todayD->month-1));

	  $postsData = \DB::table('posts')
	  ->select(
	      'posts.*',
	      'users.name AS userName',
	      'users.email AS userEmail',
	      'users.rol AS userRol'
	  )
	  // ->where('posts.id_user', '=', 'users.id')
	  ->orderBy('posts.post_date', 'desc')
	  ->join('users', 'posts.id_user', '=', 'users.id')
	  ->get();



	  $data = array(
	    'user' => $user,
	    'today' => $today,
	    'numberDay' => $todayD->day,
	    'year' => $todayD->year,
	    'month' => $month,
	    'postsData' => $postsData
	  );

	  return view('blog', $data);
	}

	public function store(CreatePostRequest $request){
    $idUser = Auth::id();
    $user = Auth::user();

    $todayD = Carbon\Carbon::now();

    $newPOST = Post::create([
      'id_user' => $idUser,
      'post' => $request->postMessage,
      'post_date' => $todayD
    ]);

    Session::flash('message', 'Hemos recibido tus <strong>comentarios</strong>');

    return redirect()->route('blog');

	}

	private function getNameDay($today){
	  if ($today == 'Sunday'){ $today = "Domingo";
	  }else if ($today == 'Monday'){ $today = "Lunes";
	  }else if ($today == 'Tuesday'){ $today = "Martes";
	  }else if ($today == 'Wednesday'){ $today = "Miércoles";
	  }else if ($today == 'Thursday'){ $today = "Jueves";
	  }else if ($today == 'Friday'){ $today = "Viernes";
	  }else if ($today == 'Saturday'){ $today = "Sábado";
	  }
	  return $today;
	}

	private function getNameMonth($indice){
	  $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
	  return $months[$indice];
	}

}
