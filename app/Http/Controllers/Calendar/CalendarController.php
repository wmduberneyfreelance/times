<?php


namespace App\Http\Controllers\Calendar;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon;
use Mail;
use Auth;
use App\Ot;
use App\Tracker;

class CalendarController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */

  public function editEventUser(Request $request){
    // $idUser = Auth::id();

    $idEvent = trim($request->eventId);
    $dateSt = trim($request->dateStart);
    $dateDn = trim($request->dateDone);
    
    $event = Tracker::findOrFail($idEvent);

    if ($dateSt == ""){
      $newData = [
        'date_done' => $dateDn
      ];
    }else{
      $newData = [
        'date_start' => $dateSt,
        'date_done' => $dateDn
      ];
    }

    $event->fill($newData);
    $event->save();

    return response()->json([
      "message" => 'success'
    ]);
    
  }

  public function saveEventUser(Request $request){
    $idUser = Auth::id();
    define('MSJ',  'message');

    $msj = trim($request->titleEvent);
    $datS = trim($request->startDate);
    $datD = trim($request->doneDate);

    if ($msj == "" || $datS == "" || $datD == ""){
      return response()->json([
        MSJ => 'blocked'
      ]);
    }

    Tracker::create([
      'id_project' => '0',
      'id_ot' => '0',
      'id_user' => $idUser,
      MSJ => $msj,
      'open' => 0,
      'date_start' => $datS,
      'date_done' => $datD
    ]);
    
    return response()->json([
      MSJ => 'success'
    ]);
  }

  public function cronJobMail(){
    $now = Carbon\Carbon::now();
    // $otsPendientes = Ot::where('tentative_date', '<', $now)->orderBy('id', 'desc')->get();

    $otsPendientes = \DB::table('ots')
            ->leftJoin('users', 'ots.id_user', '=', 'users.id')
            ->where('ots.tentative_date', '<', $now)
            ->where('ots.status', 'asignada')
            ->orWhere('ots.status', 'nueva')
            ->orWhere('ots.status', 'ajustes')
            ->get(['users.name AS userName', 'ots.*']);

    // 'nueva','asignada','completa','ajustes','cancelada'
    $userCreator = "Demo";
    // dd($otsPendientes);

    Mail::send('emails.statusOT', [
      'user' => "", 
      'ower' => "Juan Esteban Agudelo", 
      'otsPendientes' => $otsPendientes, 
      'project' => "Project", 
      'account' => "Cuenta"
    ], function ($m) use ($userCreator) {
      $m->from('makesystemssas@gmail.com', 'MAKE SYSTEMS');
      // $m->to("duberney112@gmail.com", "WM Duberney")->subject('Informe diario OT\'s MAKE.');
      $m->to("jeagudelo@aplegal.com.co", "Juan Esteban Agudelo")->subject('Informe diario OT\'s MAKE.');
    });
    
    return "Oka v2";
  }
  
  public function showEvents(){
    $idUser = Auth::id();
    // $tracker = Tracker::all();
    $tracker = Tracker::where('id_user', '=', $idUser)->get();
    
    return response()->json([
      'message' => 'success',
      'data' => $tracker
    ]);
  }

  public function index()
  {
    $idUser = Auth::id();
    // $otsData = Ot::filterAndPaginateByUser($idUser, 250);
    $tracker = Tracker::all();

    // $todayD = Carbon\Carbon::now();
    // $today = Carbon\Carbon::parse($todayD)->formatLocalized('%A');
    // $today = $this->getNameDay($today);
    // $month = $this->getNameMonth(($todayD->month-1));

    $data = array(
      'idUser' => $idUser,
      'tracker' => $tracker
      // 'otsData' => $otsData,
      // 'today' => $today,
      // 'numberDay' => $todayD->day,
      // 'year' => $todayD->year,
      // 'month' => $month
    );

    return view('calendar.index', $data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      //
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
      //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      //
  }

  private function getNameDay($today){
    if ($today == 'Sunday'){ $today = "Domingo";
    }else if ($today == 'Monday'){ $today = "Lunes";
    }else if ($today == 'Tuesday'){ $today = "Martes";
    }else if ($today == 'Wednesday'){ $today = "Miércoles";
    }else if ($today == 'Thursday'){ $today = "Jueves";
    }else if ($today == 'Friday'){ $today = "Viernes";
    }else if ($today == 'Saturday'){ $today = "Sábado";
    }
    return $today;
  }
  private function getNameMonth($indice){
    $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    return $months[$indice];
  }  
}
