<?php

namespace App\Http\Controllers;

use Carbon;
use App\User;
use App\Buzon;
use App\Notification;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\CreateMessageBuzonRequest;
use Auth;
use App\Ot;

class HomeController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function indexExecutive(){
    $user = Auth::user();

    $currentUser = $user->id;
    $idUser = $user->id;

    $todayD = Carbon\Carbon::now();
    $today = Carbon\Carbon::parse($todayD)->formatLocalized('%A');
    $today = $this->getNameDay($today);
    $month = $this->getNameMonth(($todayD->month-1));

    $ots = Ot::filterAndPaginateByExecutive($idUser, 15);

    $data = array(
      'ots' => $ots,
      'today' => $today,
      'numberDay' => $todayD->day,
      'year' => $todayD->year,
      'month' => $month
    );

    return view('homeExecutive', $data);
  }

  public function loadMessageBuzon(CreateMessageBuzonRequest $request){
    $user = Auth::user();

    $messages = Buzon::orWhere(
      [
        ['id_sending_user', '=', $user->id],
        ['id_receiving_user', '=',  $request->idReceivingUser],
      ]
    )
    ->orWhere(
      [
        ['id_receiving_user', '=', $user->id],
        ['id_sending_user', '=',  $request->idReceivingUser],
      ]
    )
    ->orderBy('id', 'asc')->get();

    $userSending = User::find($user->id);
    $userReceiving = User::find($request->idReceivingUser);

    return response()->json([
      'status' => 'loaded',
      'messages' => $messages,
      'sending_user' => $userSending,
      'receiving_user' => $userReceiving
    ]);
  }

  public function sendMessageBuzon(CreateMessageBuzonRequest $request){
    $user = Auth::user();
    $carbon_today= Carbon\Carbon::now();
    $hour = $carbon_today->format('H:i');
    $dateSend = $carbon_today->format('Y-m-d H:i:s');

    $newBuzon = Buzon::create([
      'id_sending_user' => $user->id,
      'id_receiving_user' => $request->idReceivingUser,
      'message' => $request->messageBuzon,
      'hours' => $hour,
      'readed' => 0,
      'date_send' => $dateSend
    ]);


    return response()->json([
      'status' => 'sent'
    ]);
  }

  public function buzon(){
    $user = Auth::user();
    $dataUsers = User::all();

    $todayD = Carbon\Carbon::now();
    $today = Carbon\Carbon::parse($todayD)->formatLocalized('%A');
    $today = $this->getNameDay($today);
    $month = $this->getNameMonth(($todayD->month-1));

    $data = array(
      'today' => $today,
      'numberDay' => $todayD->day,
      'year' => $todayD->year,
      'month' => $month,
      'dataUsers' => $dataUsers
    );

    return view('buzon', $data);
  }

  public function index()
  {
    $user = Auth::user();
    if ($user->rol == "executive"){
      return redirect()->route('homeExecutive');
    }

    $currentUser = $user->id;
    $idUser = $user->id;

    $todayD = Carbon\Carbon::now();
    $today = Carbon\Carbon::parse($todayD)->formatLocalized('%A');
    $today = $this->getNameDay($today);
    $month = $this->getNameMonth(($todayD->month-1));

    $ots = Ot::filterAndPaginateByUser($idUser, 15);

    $data = array(
      'ots' => $ots,
      'today' => $today,
      'numberDay' => $todayD->day,
      'year' => $todayD->year,
      'month' => $month
    );

    return view('home', $data);
  }

  private function getNameDay($today){
    if ($today == 'Sunday'){ $today = "Domingo";
    }else if ($today == 'Monday'){ $today = "Lunes";
    }else if ($today == 'Tuesday'){ $today = "Martes";
    }else if ($today == 'Wednesday'){ $today = "Miércoles";
    }else if ($today == 'Thursday'){ $today = "Jueves";
    }else if ($today == 'Friday'){ $today = "Viernes";
    }else if ($today == 'Saturday'){ $today = "Sábado";
    }
    return $today;
  }

  private function getNameMonth($indice){
    $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    return $months[$indice];
  }

}
