<?php

namespace App\Http\Controllers\Mails;

use Carbon;
use Auth;
use App\Ot;
use App\Client;
use App\Account;
use App\Project;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminMailsController extends Controller
{
  public function index(Request $request){
    $clients = Client::filterAndPaginate($request->get('searchNameClient'), 15);
    

    $todayD = Carbon\Carbon::now();
    $today = Carbon\Carbon::parse($todayD)->formatLocalized('%A');
    $today = $this->getNameDay($today);
    $month = $this->getNameMonth(($todayD->month-1));

    $data = array(
      'clients' => $clients,
      'today' => $today,
      'numberDay' => $todayD->day,
      'year' => $todayD->year,
      'month' => $month
    );
    return view('mail.editorBigmail', $data);
  }



  private function getNameDay($today){
    if ($today == 'Sunday'){ $today = "Domingo";
    }else if ($today == 'Monday'){ $today = "Lunes";
    }else if ($today == 'Tuesday'){ $today = "Martes";
    }else if ($today == 'Wednesday'){ $today = "Miércoles";
    }else if ($today == 'Thursday'){ $today = "Jueves";
    }else if ($today == 'Friday'){ $today = "Viernes";
    }else if ($today == 'Saturday'){ $today = "Sábado";
    }
    return $today;
  }

  private function getNameMonth($indice){
    $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    return $months[$indice];
  }
    
}
