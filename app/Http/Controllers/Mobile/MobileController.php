<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Ot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MobileController extends Controller
{
    

    function loadots(Request $request){
        // $request->emailUser = "duberney112@gmail.com";
        // $request->passwordUser = "123456";

    	$ots = Ot::all()->where('id_user', 1);
        $loginSuccess = false;

        if (Auth::attempt(array('email' => $request->emailUser, 'password' => $request->passwordUser)))
        {
            // echo csrf_token();
            // dd(csrf_token());
            $loginSuccess = true;
        }

        $data = array(
            'ots' => $ots,
            'user' => $request->emailUser, 
            'pass' => $request->passwordUser,
            'loginS' => $loginSuccess
        );


    	if ($request->ajax()){
			return $data;    	    
    	}else{
    		return $data;
    	}
    	// return view('home', $data);
    }
}
