<?php

namespace App\Http\Controllers\Ot;


use Storage;
use File;
use Carbon;
use Auth;
use Mail;
use App\Project;
use App\Account;
use App\User;
use App\OtType;
use App\TimeByOt;
use App\Ot;
use App\Comment;
use App\Tracker;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CreateOtRequest;
use App\Http\Requests\EditOtRequest;
use App\Http\Requests\EditOtRequestModerator;
use App\Http\Requests\EditStatusOtRequest;
use App\Http\Requests\EditDoneOtRequest;
use App\Libraries\TestClass;
use App\Libraries\UploadHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class OtController extends Controller{
  public function __construct(){
    // echo "ok forever";
  }

  public function create(Request $request){
    $otTypes = OtType::where('status', 'active')->orderBy('id', 'desc')->get();
    $idProject = $request->idProject;
    $idPieceByProject = $request->idPieceByProject;
    $idUser = Auth::id();

    $users = User::all();
    $data = array(
    	'users' => $users,
      'idProject' => $idProject,
      'idUser' => $idUser,
    	'otTypes' => $otTypes,
      'idPieceByProject' => $idPieceByProject,
    );
    return view('ot.addOt', $data);
  }

  public function pruebaemail(Request $request){
    $user = Auth::user();


    try {
      Mail::send('emails.testEmail', [
        'user' => $user, 
        'ower' => 'DEVELOPERS'
      ], function ($m) use ($user) {
       $m->from('mailer@bigholding.com.co', 'BIGOTES');
       $m->to("duberney112@gmail.com", "WM Duberney")->subject('Tienes un email de prueba de Bigotes');
      });
    } catch (Exception $e) {
      echo $e;
    }

    return "Mail send";
  }

  public function showOtsUser(Request $request){
    $idUser = Auth::id();
    $otsData = Ot::filterAndPaginateByUser($idUser, 15);

    $todayD = Carbon\Carbon::now();
    $today = Carbon\Carbon::parse($todayD)->formatLocalized('%A');
    $today = $this->getNameDay($today);
    $month = $this->getNameMonth(($todayD->month-1));

    $data = array(
      'idUser' => $idUser,
      'otsData' => $otsData,
      'today' => $today,
      'numberDay' => $todayD->day,
      'year' => $todayD->year,
      'month' => $month
    );

    return view('ot.showOtsUser', $data);
  }

  public static function unZip(){
    $fullfilename = '/Applications/MAMP/htdocs/sites/BIG/2016/bigotes/admin-bigotes/public/files/projects/1/1/bigmail.zip';
    $pathBigotes = '/Applications/MAMP/htdocs/sites/BIG/2016/bigotes/admin-bigotes/public/files/projects/1/1/bigotes/';

    if (File::exists($fullfilename)) {
      if (!File::exists($pathBigotes)){
        $folder = File::makeDirectory($pathBigotes);
      }
      $zipper = new \Chumper\Zipper\Zipper;
      $zipper->zip($fullfilename)->extractTo($pathBigotes);
    }
    return "Extracted";
  }

  public function showDetailOtsUser(Request $request){
    $idUser = Auth::id();
    $idOt = $request->idOT;
    $user = Auth::user();

    $trackers = Tracker::where('id_ot', $idOt)->orderBy('id', 'desc')->get();
    $trackerOpen = '';
    $comments = Comment::where('id_ot', $idOt)->orderBy('id', 'desc')->get();
    // $this->unZip();
    
    foreach ($trackers as $tracker) {
      if ($tracker->open == 0){
        $done = Carbon\Carbon::parse($tracker->date_done);
        $ini = Carbon\Carbon::parse($tracker->date_start);
        $totalDuration = $done->diff($ini)->format('%H:%I:%S');
        $tracker->time = $totalDuration;
      }else{
        $now = Carbon\Carbon::now();
        $ini = Carbon\Carbon::parse($tracker->date_start);
        $trackerOpen = $now->diff($ini)->format('%H:%I:%S');
        $tracker->time = 0;
      }
    }
    
    // echo "<pre>";
    // print_r($trackers);
    // echo "</pre>";
    // exit();
    
    // dd ($trackers);

    $ots = \DB::table('ots')
    ->where('ots.id', '=', $idOt)
    ->where('ots.id_user', '=', $idUser)
    ->count();

    if ($ots==1){
      $otData = \DB::table('ots')
        ->select(
            'ots.*',
            'ot_types.name AS nameTypeOT',
            'projects.name AS nameProject',
            'projects.id AS idProject',
            'accounts.name AS nameAccount',
            'clients.name AS nameClient'
        )
        ->where('ots.id', '=', $idOt)
        ->join('ot_types', 'ots.id_ot_type', '=', 'ot_types.id')
        ->join('projects', 'ots.id_project', '=', 'projects.id')
        ->join('accounts', 'projects.id_account', '=', 'accounts.id')
        ->join('clients', 'accounts.id_client', '=', 'clients.id')
        ->get();
      $dataStatusOTS = \DB::table('ots')
       ->select('status', \DB::raw('count(*) as total'))
       ->where('ots.id_project', '=', $otData[0]->idProject)
       ->groupBy('status')
       ->get();
      $path = public_path();

      $executive = \DB::table('users')
      ->where('users.id', '=', $otData[0]->id_user_creator)
      ->first();

      $data = array(
        'idUser' => $idUser,
        'user' => $user,
        'otData' => $otData,
        'path' => $path,
        'dataStatusOTS' => $dataStatusOTS,
        'comments' => $comments,
        'trackers' => $trackers,
        'trackerOpen' => $trackerOpen,
        'executive' => $executive

      );
      return view('ot.showOtDetail', $data);
    }else{

    }    
  }

  public function show(Request $request){
    $otTypes = OtType::where('status', 'active')->orderBy('id', 'desc')->get();
    $idProject = $request->idProject;
    $idUser = Auth::id();
    $idOt = $request->idOt;
    $otData = Ot::find($idOt);
    $otDataTime = TimeByOt::where('id_ot', $idOt)->orderBy('id', 'desc')->first();
    if ($otDataTime == null){
      $otDataTime['date_assigned'] = "";
      $otDataTime['hours'] = "";
    }
    // dd($otDataTime);
    // dd($otData);
    // dd($otDataTime);
    $user = Auth::user();
    $path = public_path();
    $comments = Comment::where('id_ot', $idOt)->orderBy('id', 'desc')->get();

    $dataStatusOTS = \DB::table('ots')
     ->select('status', \DB::raw('count(*) as total'))
     ->where('ots.id_project', '=', $idProject)
     ->groupBy('status')
     ->get();

    $users = User::all();
    $infoUserCreator = User::where('id', $otData->id_user_creator)->first();

    $data = array(
      'user' => $user,
      'users' => $users,
      'infoUserCreator' => $infoUserCreator,
      'idProject' => $idProject,
      'idUser' => $idUser,
      'otData' => $otData,
      'otTypes' => $otTypes,
      'path' => $path,
      'comments' => $comments,
      'dataStatusOTS' => $dataStatusOTS,
      'otDataTime' => $otDataTime
    );

    return view('ot.showOt', $data);
  }
  
  public function deleteOT(Request $request){
    $idOT = $request->idOT;
    $idProject = $request->idProject;

    $ot = Ot::findOrFail($idOT);
    $deleteComments = Comment::where('id_ot', $idOT)->delete();
    $deleteTimeByOt = TimeByOt::where('id_ot', $idOT)->delete();

    $ot->delete();


    Session::flash('message', "<strong>La orden de trabajo ha sido eliminada.</strong>"); //La información solo se carga la primera vez en la pagina
    return redirect()->route('showproject', $idProject);
  }

  public function setTracker(Request $request){
    $user = Auth::user();
    $idOT = $request->idOT;

    $otData = Ot::findOrFail($request->idOT);

    if (!$otData){
      return response()->json([
        'status' => 'blocked'
      ]);
    }

    $log = "";
    // $traker = Tracker::where('id_ot', $idOt)->orderBy('id', 'desc')->get();
    $trackAbirtos = \DB::table('trakers')
      ->where('trakers.id_ot', '=', $idOT)
      ->where('trakers.open', '=', 1)
      ->count();

    if ($trackAbirtos>0){
      try{
        $now = Carbon\Carbon::now();
        $trakers = \DB::table('trakers')
          ->where('trakers.id_ot', '=', $idOT)
          ->where('trakers.open', '=', 1)
          ->update(['open' => '0', 'date_done' => $now]);
      }
      catch(\Exception $e){
        // catch code
      }
    }else{
      $msj = $request->msj;
      if ($msj == ""){
        return response()->json([
          'status' => 'blocked'
        ]);
      }
      $now = Carbon\Carbon::now();
      $newTracker = Tracker::create([
        'id_user' => $user->id,
        'id_ot' => $idOT,
        'id_project' => $otData->id_project,
        'message' => $msj,
        'open' => 1,
        'date_start' => $now
      ]);
    }

    return response()->json([
      'status' => 'success'
    ]);
  }

  public function setBigmail(Request $request){
    $user = Auth::user();

    if ($user->rol != "superadmin" && $user->rol != "developer"){
      return response()->json([
        'status' => 'blocked',
        'des' => 'User Rol'
      ]);
    }

    $otData = Ot::findOrFail($request->idOT);
    $bgM = $request->swBigMail;
    $newData = [
      'bigmail' => $bgM
    ];

    $otData->fill($newData);
    $otData->save();

    return response()->json([
      'status' => 'success'
    ]);
  }

  public function updateStatusDone(EditDoneOtRequest $request){
    $user = Auth::user();
    $idOt = $request->idOtAdjust;
    $otData = Ot::findOrFail($idOt);
    $userCreator = User::findOrFail($otData->id_user_creator);

    $com = trim($request->commentsAdjust);
    if ($com != ""){
      $newComment = Comment::create([
        'id_user' => $user->id,
        'id_ot' => $idOt,
        'name_user' => $user->name,
        'comments' => $com
      ]);
    }
    
    $newData = [
      'status' => 'completa',
    ];

    $project = Project::find($otData->id_project);
    $account = Account::find($project->id_account);

    // Mail::send('emails.doneOT', [
    //   'user' => $user, 
    //   'ower' => $userCreator->name, 
    //   'otData' => $otData, 
    //   'project' => $project, 
    //   'account' => $account
    // ], function ($m) use ($userCreator) {
    //   $m->from('mailer@bigholding.com.co', 'BIGOTES');
    //   $m->to($userCreator->email, $userCreator->name)->subject('Se ha completado una OT creada por ti.');
    // });

    $otData->fill($newData);
    $otData->save();

    Session::flash('message', 'La orden de trabajo ha sido completada <strong>satisfactoriamente</strong>');

    return redirect()->route('loadots');
  }

  public function updateStatus(EditStatusOtRequest $request){
    $user = Auth::user();
    $idProject = $request->idProjectAdjust;
    $idOt = $request->idOtAdjust;
    $otData = Ot::findOrFail($idOt);
    $otData->version++;

    $newData = [
      'status' => 'ajustes',
      'version' => $otData->version,
      'id_user' => null
    ];
    
    $newComment = Comment::create([
      'id_user' => $user->id,
      'id_ot' => $idOt,
      'name_user' => $user->name,
      'comments' => $request->commentsAdjust
    ]);

    $project = Project::find($otData->id_project);
    $account = Account::find($project->id_account);

    $dataOtType = OtType::find($otData->id_ot_type);

    // Mail::send('emails.ajustesOT', [
    //   'user' => $user, 
    //   'ower' => 'DEVELOPERS', 
    //   'project' => $project, 
    //   'account' => $account
    // ], function ($m) use ($user, $dataOtType, $account) {
    //  $m->from('mailer@bigholding.com.co', 'BIGOTES');
    //  $m->to($dataOtType->email, $dataOtType->departament)->subject($account->name . ' - Ha reabierto una OT para ajustes.');
    // });

    $otData->fill($newData);
    $otData->save();

    Session::flash('message', 'La orden de trabajo ha sido actualizada <strong>satisfactoriamente</strong>');

    return redirect()->route('showOt', [$idProject, $idOt]);
  }

  public function updateModerator(Request $request){
    $user = Auth::user();
    $idOt = $request->idOt;
    $otData = Ot::findOrFail($idOt);

    if ($request->userAsignOt == "")
      $request->userAsignOt = null;

    $com = trim($request->commentsGeneral);
    if ($com != ""){
      $newComment = Comment::create([
        'id_user' => $user->id,
        'id_ot' => $idOt,
        'name_user' => $user->name,
        'comments' => $com
      ]);      
    }

    $newData = [
      'id_user' => $request->userAsignOt,
      'status' => $request->otStatus,
    ];

    if ($request->otStatus == 'asignada'){
      if ($request->userAsignOt != null){
        $userAsign = User::find($request->userAsignOt);
        $project = Project::find($otData->id_project);
        $account = Account::find($project->id_account);

        // Mail::send('emails.asignadaOT', [
        //   'user' => $user, 
        //   'ower' => $user->name,
        //   'project' => $project, 
        //   'account' => $account,
        //   'otData' => $otData,
        //   ], function ($m) use ($userAsign, $account) {
        //  $m->from('mailer@bigholding.com.co', 'BIGOTES');

        //  $m->to($userAsign->email, $userAsign->name)->subject($account->name . ' - Tienes una nueva OT');
        // });
      }
    }

    $otData->fill($newData);
    $otData->save();

    Session::flash('message', 'La orden de trabajo ha sido actualizada <strong>satisfactoriamente</strong>');


    return redirect()->route('showOt', [$request->idProject, $idOt]);
  }

  public function update(EditOtRequest $request){
    $user = Auth::user();

    $idOt = $request->idOt;
    $otData = Ot::findOrFail($idOt);

    if ($request->userAsignOt == "")
      $request->userAsignOt = null;

    if ($request->otStatus == 'ajustes'){
      $otData->version++;
    }
    
    if ($request->otStatus == "asignada" && $request->otStatus != $otData->status){
      if ($request->userAsignOt != null){
        if ($request->dateCalendarUser && $request->hourCalendarUser){
          $newTimeByOt = TimeByOt::create([
            'id_user_creator' => $user->id,
            'id_assigned_user' => $request->userAsignOt,
            'id_ot' => $idOt,
            'hours' => $request->hourCalendarUser,
            'date_assigned' => $request->dateCalendarUser
          ]);
        }
      }
    }

    $com = trim($request->commentsGeneral);
    if ($com != ""){
      $newComment = Comment::create([
        'id_user' => $user->id,
        'id_ot' => $idOt,
        'name_user' => $user->name,
        'comments' => $com
      ]);      
    }

    $newData = [
      'id_ot_type' => $request->otType,
      'id_project' => $request->idProject,
      'id_user' => $request->userAsignOt,
      'name' => $request->nameOt,
      'status' => $request->otStatus,
      'priority' => $request->priorityOT,
      'complexity' => $request->complexityOT,
      'tentative_date' => $request->tentativeDateOT,
      'description' => $request->descriptionOT,
      'recommendations' => $request->recommendationsOT,
      'version' => $otData->version,
      // 'metadata' => $request->metadataOT
    ];

    if ($request->otStatus == 'asignada'){
      if ($request->userAsignOt != null){
        $userAsign = User::find($request->userAsignOt);
        $project = Project::find($otData->id_project);
        $account = Account::find($project->id_account);

        // Mail::send('emails.asignadaOT', [
        //   'user' => $user, 
        //   'ower' => $user->name,
        //   'project' => $project, 
        //   'account' => $account,
        //   'otData' => $otData,
        //   ], function ($m) use ($userAsign, $account) {
        //  $m->from('mailer@bigholding.com.co', 'BIGOTES');

        //  $m->to($userAsign->email, $userAsign->name)->subject($account->name . ' - Tienes una nueva OT');
        // });
      }
    }

    $otData->fill($newData);
    $otData->save();

    Session::flash('message', 'La orden de trabajo ha sido actualizada <strong>satisfactoriamente</strong>');


    return redirect()->route('showOt', [$request->idProject, $idOt]);
  }

  public function store(CreateOtRequest $request){
    $idUser = Auth::id();
    $user = Auth::user();

    $newOT = Ot::create([
      'id_ot' => str_random(20),
      'id_user_creator' => $idUser,
      'id_ot_type' => $request->otType,
      'id_piece_by_project' => $request->idPieceByProject,
      'id_project' => $request->idProject,
      'name' => $request->nameOt,
      'status' => 'nueva',
      'priority' => $request->priorityOT,
      'complexity' => $request->complexityOT,
      'tentative_date' => $request->tentativeDateOT,
      'description' => $request->descriptionOT,
      'recommendations' => $request->recommendationsOT,
      'metadata' => $request->metadataOT,
      'version' => 1,
      'bigmail' => 0
    ]);

    $project = Project::find($request->idProject);
    $account = Account::find($project->id_account);
    $dataOtType = OtType::find($request->otType);

    // Mail::send('emails.nuevaOT', [
    //   'user' => $user, 
    //   'ower' => 'DIRECTOR', 
    //   'project' => $project, 
    //   'account' => $account
    // ], function ($m) use ($user, $dataOtType, $account) {
    //  $m->from('mailer@bigholding.com.co', 'BIGOTES');
    //  $m->to($dataOtType->email, $dataOtType->departament)->subject($account->name .' - Ha creado una nueva OT');
    // });

    Session::flash('message', 'La orden de trabajo ha sido guardada <strong>satisfactoriamente</strong>');

    return redirect()->route('showOt', [$request->idProject, $newOT->id]);
  }

  public function unzipFiles(Request $request){
    $fullfilename = $request->urlFile;
    $pathBigotes = '/Applications/MAMP/htdocs/sites/BIG/2016/bigotes/admin-bigotes/public/files/projects/1/1/bigotes/';

    if (File::exists($fullfilename)) {
      if (!File::exists($pathBigotes)){
        $folder = File::makeDirectory($pathBigotes);
      }
      $zipper = new \Chumper\Zipper\Zipper;
      $zipper->zip($fullfilename)->extractTo($pathBigotes);
    }

    return response()->json([
      'status' => 'ok',
      'urlFile' => $fullfilename
    ]);
  }

  function getFilesByOt(Request $request){
    $idUser = $request->idUser;
    $idProject = $request->idProject;
    $idOt = $request->idOt;

    $directory = $idProject .'/'. $idOt .'/';
    $files = Storage::files($directory);
    return response()->json([
      $files => $files
    ]);

  }

  function uploadfiles(Request $request){
    $idUser = $request->idUser;
    $idProject = $request->idProject;
    $idOt = $request->idOt;
    $origin = $request->origin;

    $userInfo = User::findOrFail($idUser);

    //obtenemos el campo file definido en el formulario
    $file = $request->file('file');
    // dd($file);
    if (count($file)>0){
      $directory = $idProject .'/'. $idOt .'/';
      $files = Storage::files($directory);
      $baseCont = count($files);

      $todayD = Carbon\Carbon::now();
      // $today = Carbon\Carbon::parse($todayD)->formatLocalized('%A');
      // $today = $this->getNameDay($today);
      // $month = $this->getNameMonth(($todayD->month-1));


      for ($i=0; $i<count($file); $i++){
        // $nombre = $baseCont . "_" . $file[$i]->getClientOriginalName();
        $nombre = $todayD . "-|||-" . $userInfo->name . "-|||-" . $file[$i]->getClientOriginalName();

        $baseCont++;
        \Storage::disk('local')->put($idProject .'/'. $idOt .'/' . $nombre,  \File::get($file[$i]));
      }
    }

    if ($origin == "pro"){
      return redirect()->route('showOt', [$idProject, $idOt]);
    }else{
      return redirect()->route('loadotdetail', [$idOt]);
    }

    

  }

  private static function sortFunction( $a, $b ) {
    return strtotime($a["date"]) - strtotime($b["date"]);
  }


  public function loadfiles(Request $request){
    $idUser = $request->idUser;
    $idProject = $request->idProject;
    $idOt = $request->idOt;

    $directory = $idProject .'/'. $idOt .'/';

    $files = Storage::files($directory);
    $filesDone = array();

    for ($i=0; $i<count($files); $i++){
      $obj = $files[$i];
      $text = str_replace($idProject .'/'. $idOt .'/', "", $obj);
      $firstLetter = substr($text, 0, 1);
      if ($firstLetter != "."){
        $newArr = explode("-|||-", $text);

        $arrP = array('title' => $newArr[2], 'date' => $newArr[0], 'user' => $newArr[1] );
        array_push($filesDone, $arrP);
      }
    }

    usort($filesDone, array($this,'sortFunction'));
    $filesDone = array_reverse($filesDone);

    // dd($filesDone);

    return response()->json([
      'files' => $filesDone
    ]);

    // return view('ot.addOt', $data);
  }  

  // public function loadfiles(Request $request){
  //   $idUser = $request->idUser;
  //   $idProject = $request->idProject;
  //   $idOt = $request->idOt;

  //   $options = array(
  //     'upload_dir'=> public_path() .'/files/projects/'. $idProject .'/'. $idOt .'/',
  //     'upload_url'=> public_path() . '/files/projects/'. $idProject .'/'. $idOt .'/'
  //   );

  //   $upload_handler = new UploadHandler($options);
    
  //   return;

  //   // return view('ot.addOt', $data);
  // }

  private function getNameDay($today){
    if ($today == 'Sunday'){ $today = "Domingo";
    }else if ($today == 'Monday'){ $today = "Lunes";
    }else if ($today == 'Tuesday'){ $today = "Martes";
    }else if ($today == 'Wednesday'){ $today = "Miércoles";
    }else if ($today == 'Thursday'){ $today = "Jueves";
    }else if ($today == 'Friday'){ $today = "Viernes";
    }else if ($today == 'Saturday'){ $today = "Sábado";
    }
    return $today;
  }

  private function getNameMonth($indice){
    $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    return $months[$indice];
  }

  public function deleteFileByOT(Request $request){

    $type = $request->type;
    $file = $request->file;
    $path = public_path();
    $pathThumb = "";

    if (File::exists($path . $file)) {
      File::delete($path . $file);

      $arrPathFile = explode("/", $file);
      $indice = count($arrPathFile)-1;
      $nameFile = $arrPathFile[$indice];
      unset($arrPathFile[$indice]);
      $pathThumb = $path . implode("/", $arrPathFile) . "/thumbnail/" . $nameFile;
      if (File::exists($pathThumb)) {
        File::delete($pathThumb);
      }
    }

    return response()->json([
      'status' => 'delete_success'
    ]);
  }

  public function publishCommentsUser(Request $request){
    $user = Auth::user();

    $sendNoti = $request->sendNoti;

    $newComment = Comment::create([
      'id_user' => $user->id,
      'id_ot' => $request->idOT,
      'name_user' => $user->name,
      'comments' => $request->commentsUser
    ]);

    if ($sendNoti == "SI"){
      $infoOT = Ot::where('id', '=', $request->idOT)->first();
      $infoUserCreator = User::where('id', $infoOT->id_user_creator)->first();

      // Mail::send('emails.messageOT', [
      //   'user' => $user, 
      //   'infoOT' => $infoOT,
      //   'infoUserCreator' => $infoUserCreator, 
      //   'comments' => $request->commentsUser
      // ], function ($m) use ($infoUserCreator) {
      //   $m->from('mailer@bigholding.com.co', 'BIGOTES');
      //   $m->to($infoUserCreator->email, $infoUserCreator->name)->subject('Tienes un comentario en una OT creada por tí.');
      //   // $m->to("duberney112@gmail.com", "WM Duberney")->subject('Tienes un comentario en una OT creada por tí.');
      // });

    }

    return response()->json([
      'status' => 'publish'
    ]);

  }
}
