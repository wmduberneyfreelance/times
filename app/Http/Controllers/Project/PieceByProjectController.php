<?php

namespace App\Http\Controllers\Project;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Project;
use App\PiecesByProject;
use App\Ot;
use App\Http\Requests\CreatePieceByProjectRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PieceByProjectController extends Controller
{
  public function index(Request $request)
  {
  }

  public function create()
  {
  }

  public function deletePieza(Request $request){
    $idPieza = $request->idPieza;
    $idProject = $request->idProject;

    $pieza = PiecesByProject::findOrFail($idPieza);
    $totalOTs = Ot::where('id_piece_by_project', $pieza->id)->count();

    if ($totalOTs > 0){
      Session::flash('message', "No se puede eliminar la Pieza <strong>". $pieza->name ."</strong> porque aún contiene OT's.");
      return redirect()->route('showproject', $idProject);
    }else{
      Session::flash('message', "La Pieza <strong>". $pieza->name ."</strong> ha sido eliminada.");
      $pieza->delete();
      return redirect()->route('showproject', $idProject);
    }
  }

  public function store(CreatePieceByProjectRequest $request)
  {
    $newProject = PiecesByProject::create([
    	'id_project' => $request->idProjectPieceType,
    	'id_piece_type' => $request->idPieceType,
    	'name' => $request->namePiece
    ]);

    Session::flash('message', 'La pieza ha sido creada <strong>satisfactoriamente</strong>');

    return redirect()->route('showproject', $request->idProjectPieceType);
  }

  public function show($id)
  {
  }

  public function update(EditProjectRequest $request, $id)
  {
  }

  public function destroy($id, Request $request)
  {
  }

}
