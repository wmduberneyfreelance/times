<?php

namespace App\Http\Controllers\Project;

use Auth;
use App\Account;
use App\Client;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CreateProjectRequest;
use App\Http\Requests\EditProjectRequest;
use App\Project;
use App\Ot;
use App\PiecesTypes;
use App\PiecesByProject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProjectController extends Controller
{
  public function index(Request $request)
  {
    $user = Auth::user();
    $filterUser = "";
    if ($user->rol != 'superadmin' && $user->rol != 'moderator'){
      $filterUser = $user->id;
    }

    $clientProject = $request->get('clientProject');
    $accountProject = $request->get('accountProject');
    $executiveProject = $request->get('executiveProject');

    $projects = Project::filterAndPaginate($request->get('searchNameProject'), "complet", $filterUser, $clientProject, $accountProject, $executiveProject, 15);

    $clients = Client::where('status', 'active')->orderBy('name', 'asc')->get();
    $accounts = Account::where('status', 'active')->orderBy('name', 'asc')->get();

    $data = array(
    	'projects' => $projects,
    	'clients' => $clients,
    	'accounts' => $accounts
    );

    return view('project.showProjects', $data);
  }

  public function create()
  {
    // $clients = Client::where('status', 'active')->orderBy('name', 'asc')->get();
    // $projectsTypes = ProjectType::where('status', 'active')->orderBy('name', 'asc')->get();
    $accounts = Account::filterByClient();

    $data = array(
      // 'clients' => $clients,
    	'accounts' => $accounts
    );

    return view('project.addProject', $data);
  }

  public function store(CreateProjectRequest $request)
  {
    $user = Auth::user();

    $newProject = Project::create([
    	'id_account' => $request->idAccount,
      'id_user_creator' => $user->id,
    	'tentative_date' => $request->tentativeDateProject,
    	'name' => $request->nameProject,
    	'status' => 'new'
    ]);
    return redirect()->route('loadprojects');
  }

  public function show($id)
  {
    $user = Auth::user();
    $project = Project::find($id);

    // Validar permisos del usuario
    if ($user->rol != 'superadmin' && $user->rol != 'moderator'){
      if ($project->id_user_creator != $user->id){
        return redirect()->route('loadprojects');
      }
    }

    $ots = Ot::filterAndPaginateByProject($id, 1000);
    $piecesTypes = PiecesTypes::all();

    $piecesTypesByProject = \DB::table('pieces_by_project')
     ->select(
      'pieces_by_project.*',
      'pieces_types.name AS namePiece'
    )
     ->where('pieces_by_project.id_project', '=', $id)
     ->join('pieces_types', 'pieces_by_project.id_piece_type', '=', 'pieces_types.id')
     ->get();

    $dataStatusOTS = \DB::table('ots')
     ->select('status', \DB::raw('count(*) as total'))
     ->where('ots.id_project', '=', $id)
     ->groupBy('status')
     ->get();

    $dataClient = \DB::table('projects')
    ->select(
        'clients.name AS nameClient'
    )
    ->where('projects.id', '=', $project->id)
    ->join('accounts', 'projects.id_account', '=', 'accounts.id')
    ->join('clients', 'accounts.id_client', '=', 'clients.id')
    ->get();


    $nameClient = $dataClient[0]->nameClient;
    $accounts = Account::filterByClient();

    if ($project == null)
        return project()->route('loadprojects');

    $colors = ["#155263", "#539092", "#8BCFCC", "#AEE8E6", "#F5F5F5", "#F1F4F6"];
    $iColor = 0;


    $data = array(
      'user' => $user,
      'project' => $project,
      'nameClient' => $nameClient,
    	'accounts' => $accounts,
    	'idProjectUpdate' => $id,
      'ots' => $ots,
      'dataStatusOTS' => $dataStatusOTS,
      'piecesTypes' => $piecesTypes,
      'piecesTypesByProject' => $piecesTypesByProject, 
      'colors' => $colors,
      'iColor' => $iColor,
    );

    return view('project.editProject', $data);
  }

  public function update(EditProjectRequest $request, $id)
  {
    $project = Project::findOrFail($id);
    $newData = [
      // 'id_client' => $request->idClient,
      'id_account' => $request->idAccount,
      'id_project_type' => $request->idProTyp,
      'tentative_date' => $request->tentativeDateProject,
      'status' => $request->statusProject,
      'name' => $request->nameProject
    ];

    $project->fill($newData);
    $project->save();

    Session::flash('message', 'El proyecto ha sido actualizado <strong>¡SATISFACTORIAMENTE!</strong>');

    return redirect()->route('showproject', $id);
  }

  public function destroy($id, Request $request)
  {
    $idProject = $request->idProject;
    $project = Project::findOrFail($idProject);
    // $project->delete();

    $totalOTs = Ot::where('id_project', $project->id)->count();
    $totalPiezas = PiecesByProject::where('id_project', $project->id)->count();

    if ($totalOTs > 0){
      Session::flash('message', "No se puede eliminar el proyecto <strong>". $project->name ."</strong> porque aún contiene OT's.");

      return redirect()->route('showproject', $idProject);
      
    }else if ($totalPiezas > 0){
      Session::flash('message', "No se puede eliminar el proyecto <strong>". $project->name ."</strong> porque aún contiene piezas asociadas.");

      return redirect()->route('showproject', $idProject);
    }else{
      Session::flash('message', "El proyecto <strong>". $project->name ."</strong> ha sido eliminado.");
      $project->delete();
      return redirect()->route('loadprojects');
    }
  }
}
