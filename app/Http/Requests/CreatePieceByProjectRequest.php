<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreatePieceByProjectRequest extends Request
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
      return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'idProjectPieceType' => 'required|exists:projects,id',
      'idPieceType' => 'required|exists:pieces_types,id',
      'namePiece' => 'required|min:3',
    ];
  }
}
