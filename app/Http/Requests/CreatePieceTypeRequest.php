<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreatePieceTypeRequest extends Request
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
      return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'namePieceType' => 'required|min:3',
      'descriptionPieceType' => 'required|min:8',
      'statePieceType' => 'required|in:active,inactive',
    ];
  }
}
