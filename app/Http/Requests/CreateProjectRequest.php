<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateProjectRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'idAccount' => 'required|exists:accounts,id',
            // 'idUser' => 'required|exists:users,id',
            // 'tentativeDateProject' => 'required|date_format:YYYY-MM-DD',
            'tentativeDateProject' => 'required',
            'nameProject' => 'required|min:3',
        ];
    }
}
