<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nameUser' => 'required',
            'stateUser' => 'required|in:active,inactive',
            'emailUser' => 'required|unique:users,email',
            'profileUser' => 'required|in:user,executive,developer,creative,moderator,admin,superadmin',
            'genderUser' => 'in:masculine,female',
            'passwordUser' => 'required',
            'rePasswordUser' => 'required',
        ];
    }
}
