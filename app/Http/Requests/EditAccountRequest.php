<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditAccountRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'nameAccount' => 'required',
           'idClient' => 'required|exists:clients,id',
           'stateAccount' => 'required|in:active,inactive',
       ];
    }
}
