<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditOtRequest extends Request
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      // 'id_user' => 'required|exists:users,id',
      'nameOt' => 'required|min:3',
      'priorityOT' => 'required|in:baja,media,alta',
      'otStatus' => 'required|in:nueva,asignada,completa,ajustes,cancelada',

      'otType' => 'required|exists:ot_types,id',
      'idProject' => 'required|exists:projects,id',
      'complexityOT' => 'required|in:baja,media,alta',
      
      'dateCalendarUser' => 'required_if:otStatus,asignada|date',
      'hourCalendarUser' => 'required_if:otStatus,asignada|numeric',

      // 'tentative_date',
      // 'description',
      // 'recommendations',
      // 'metadata'
    ];
  }
}
