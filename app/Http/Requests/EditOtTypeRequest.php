<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditOtTypeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nameProType' => 'required|min:3',
            'descriptionProType' => 'required|min:8',
            'deparmentProType' => 'required|in:developer,creative,other',
            'emailDeparmentProType' => 'required|min:8',
            'stateProType' => 'required|in:active,inactive',
        ];
    }
}
