<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::get('/', function () {
  // return view('welcome');
  // return redirect()->route('iniciar');
  // return redirect('login');
  return redirect('home');
});


Route::get('login', [
	'uses' => 'Auth\AuthController@getLogin',
	'as' => 'iniciar'
]);


// Route::get('appMobile/loadOtsByUser', 'Mobile\MobileController@loadots');
Route::post('appMobile/loadOtsByUser', 'Mobile\MobileController@loadots');

Route::group(['middleware' => 'web'], function () {
  Route::auth();
  // Route::get('/home', 'HomeController@index');
  Route::get('/home', [ 'uses' => 'HomeController@index', 'as' => 'home']);
  Route::get('/buzon', [ 'uses' => 'HomeController@buzon', 'as' => 'buzon']);
  Route::post('/buzon/sendMessage', [ 'uses' => 'HomeController@sendMessageBuzon', 'as' => 'buzonsend']);
  Route::post('/buzon/LoadMessage', [ 'uses' => 'HomeController@loadMessageBuzon', 'as' => 'buzonload']);

});


Route::group(['prefix' => 'api/v1', 'middleware' => 'auth:api'], function () {
  Route::get('/ot', 'Mobile\MobileController@loadots');
  Route::get('/ot/{idOt}', 'Mobile\MobileController@loadot');
});

/*
|--------------------------------------------------------------------------
| Rutas publicas
|--------------------------------------------------------------------------
*/

Route::get('cronjob', [ 'uses' => 'Calendar\CalendarController@cronJobMail', 'as' => 'calendarJob']);

/*
|--------------------------------------------------------------------------
| Rutas para los usuarios genericos de la plataforma, creativos, desarrolladores
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => ['web', 'auth']], function () {
  //Rutas para las OT'S
  Route::get('loadots', [ 'uses' => 'Ot\OtController@showOtsUser', 'as' => 'loadots']);
  Route::get('loadot/{idOT}', [ 'uses' => 'Ot\OtController@showDetailOtsUser', 'as' => 'loadotdetail']);

  Route::post('loadot/setbigmail/', [ 'uses' => 'Ot\OtController@setBigmail', 'as' => 'setbigmail']);
  Route::post('loadot/settracker/', [ 'uses' => 'Ot\OtController@setTracker', 'as' => 'settracker']);

  Route::get('calendar', [ 'uses' => 'Calendar\CalendarController@index', 'as' => 'calendar']);
  Route::post('eventscalendar', [ 'uses' => 'Calendar\CalendarController@showEvents', 'as' => 'eventscalendar']);

  Route::post('calendarNewEventUser', [ 'uses' => 'Calendar\CalendarController@saveEventUser', 'as' => 'calendarNewEventUser']);
  Route::post('editEventUser', [ 'uses' => 'Calendar\CalendarController@editEventUser', 'as' => 'editEventUser']);
  

  //Prueba email
  Route::get('demo/sendemail/', [ 'uses' => 'Ot\OtController@pruebaemail', 'as' => 'pruebaemail']);

  //Rutas para los tipos de piezas por proyecto
  Route::post('project/addPieceByProject', [ 'uses' => 'Project\PieceByProjectController@store', 'as' => 'addPieceByProject' ]);

  //Rutas para el feedback
  Route::get('blog', [ 'uses' => 'BlogController@index', 'as' => 'blog']);
  Route::post('blog/save', [ 'uses' => 'BlogController@store', 'as' => 'savepost' ]);

  //Rutas para los ejecutivos
  Route::get('/home-executive', [ 'uses' => 'HomeController@indexExecutive', 'as' => 'homeExecutive' ]);


  //Rutas para los documentos de las OT
  Route::get('projects/loadfilesbyot/{idUser}/{idProject}/{idOt}/', [ 'uses' => 'Ot\OtController@loadfiles', 'as' => 'loadfilesbyot']);
  Route::post('projects/loadfilesbyot/{idUser}/{idProject}/{idOt}/', [ 'uses' => 'Ot\OtController@loadfiles', 'as' => 'loadfilesbyot']);
  Route::post('projects/loadfilesbyot/unzipfile/', [ 'uses' => 'Ot\OtController@unzipFiles', 'as' => 'unzipfilesbyot']);


  //Temporal
  Route::post('projects/uploadfile/{idUser}/{idProject}/{idOt}/', [ 'uses' => 'Ot\OtController@uploadfiles', 'as' => 'uploaddemo']);



  //Rutas para las OT
  Route::post('projects/editStatusOtDone', [ 'uses' => 'Ot\OtController@updateStatusDone', 'as' => 'editotstatusdone' ]);
  Route::post('projects/editStatusOt', [ 'uses' => 'Ot\OtController@updateStatus', 'as' => 'editotstatus' ]);
  Route::post('projects/ot/publishComments', [ 'uses' => 'Ot\OtController@publishCommentsUser', 'as' => 'publishComments' ]);
  Route::post('projects/ot/deleteFileByOt', [ 'uses' => 'Ot\OtController@deleteFileByOT', 'as' => 'deleteFileByOt' ]);


});

/*
|--------------------------------------------------------------------------
| Rutas para los usuarios ejecutivos, estos pueden administrar proyectos y ordenes de trabajo
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => ['web', 'auth', 'is_moderator']], function () {
  //Rutas para las OT
  Route::get('projects/newOt/{idProject}/{idPieceByProject}', [ 'uses' => 'Ot\OtController@create', 'as' => 'newOt']);
  Route::get('projects/loadOt/{idProject}/{idOt}', [ 'uses' => 'Ot\OtController@show', 'as' => 'showOt']);
  Route::post('projects/newOt/saveOT', [ 'uses' => 'Ot\OtController@store', 'as' => 'saveot' ]);

  Route::post('projects/newOt/editOT/{idOt}', [ 'uses' => 'Ot\OtController@update', 'as' => 'editot' ]);
  Route::post('projects/newOt/editotModerator/{idOt}', [ 'uses' => 'Ot\OtController@updateModerator', 'as' => 'editotModerator' ]);




  //Rutas para proyectos
  Route::get('projects/loadProjects', [ 'uses' => 'Project\ProjectController@index', 'as' => 'loadprojects']);
  Route::get('projects/showProject/{id}', [ 'uses' => 'Project\ProjectController@show', 'as' => 'showproject' ]);
  Route::get('projects/newProject', [ 'uses' => 'Project\ProjectController@create', 'as' => 'newproject']);
  Route::post('projects/saveProject', [ 'uses' => 'Project\ProjectController@store', 'as' => 'saveproject' ]);
  Route::post('projects/editProject/{id}', [ 'uses' => 'Project\ProjectController@update', 'as' => 'editproject' ]);



  //Rutas para tipos de ot
  Route::post('admin/loaddetailtypesot', [ 'uses' => 'Admin\OtTypesController@detailottype', 'as' => 'loaddetailtypesot']);
  
});


/*
|--------------------------------------------------------------------------
| Rutas para los usuarios administradores
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => ['web', 'auth', 'is_admin']], function () {
  // Route::auth();
  Route::get('admin', [ 'uses' => 'AdminController@index', 'as' => 'admin' ]);

  //Eliminar OT solo el admin puede hacerlo
  Route::get('deleteOT/{idProject}/{idOT}', [ 'uses' => 'Ot\OtController@deleteOT', 'as' => 'deleteOT' ]);
  Route::get('deletePiezaOT/{idProject}/{idPieza}', [ 'uses' => 'Project\PieceByProjectController@deletePieza', 'as' => 'deletePieza' ]);

  Route::get('deleteProject/{idProject}', [ 'uses' => 'Project\ProjectController@destroy', 'as' => 'destroyproject' ]);

  //Rutas para los mails
  Route::get('adminmails', [ 'uses' => 'Mails\AdminMailsController@index', 'as' => 'loadclientsmails']);
  
  //Rutas para los perfiles
  Route::get('admin/newprofile', [ 'uses' => 'AdminController@create', 'as' => 'newprofile' ]);
  Route::get('admin/showprofile/{id}', [ 'uses' => 'AdminController@showProfile', 'as' => 'showprofile' ]);
  Route::post('admin/storeProfile', [ 'uses' => 'AdminController@storeProfile', 'as' => 'saveprofile' ]);
  Route::post('admin/editProfile/{id}', [ 'uses' => 'AdminController@updateProfile', 'as' => 'editprofile' ]);
  Route::get('admin/destroyProfile/{id}', [ 'uses' => 'AdminController@destroyProfile', 'as' => 'destroyprofile' ]);
  Route::post('admin/destroyProfile/{id}', [ 'uses' => 'AdminController@destroyProfile', 'as' => 'destroyprofile' ]);

  //Rutas para los usuarios
  Route::get('admin/loadusers', [ 'uses' => 'Admin\UserController@index', 'as' => 'loadusers']);
  Route::get('admin/showuser/{id}', [ 'uses' => 'Admin\UserController@show', 'as' => 'showuser' ]);
  Route::get('admin/newuser', [ 'uses' => 'Admin\UserController@create', 'as' => 'newuser' ]);
  Route::post('admin/storeUser', [ 'uses' => 'Admin\UserController@store', 'as' => 'saveuser' ]);
  Route::post('admin/editUser/{id}', [ 'uses' => 'Admin\UserController@update', 'as' => 'edituser' ]);
  Route::get('admin/destroyUser/{id}', [ 'uses' => 'AdminController@destroyProfile', 'as' => 'destroyuser' ]);
  Route::post('admin/destroyUser/{id}', [ 'uses' => 'AdminController@destroyProfile', 'as' => 'destroyuser' ]);

  //Rutas para los clientes
  Route::get('admin/loadclients', [ 'uses' => 'Admin\ClientController@index', 'as' => 'loadclients']);
  Route::get('admin/showclient/{id}', [ 'uses' => 'Admin\ClientController@show', 'as' => 'showclient' ]);
  Route::get('admin/newclient', [ 'uses' => 'Admin\ClientController@create', 'as' => 'newclient' ]);
  Route::post('admin/storeClient', [ 'uses' => 'Admin\ClientController@store', 'as' => 'saveclient' ]);
  Route::post('admin/editClient/{id}', [ 'uses' => 'Admin\ClientController@update', 'as' => 'editclient' ]);
  Route::get('admin/destroyClient/{id}', [ 'uses' => 'Admin\ClientController@destroy', 'as' => 'destroyclient' ]);
  
  //Rutas para las cuentas
  Route::get('admin/loadaccounts', [ 'uses' => 'Admin\AccountController@index', 'as' => 'loadaccounts']);
  Route::get('admin/showAccount/{id}', [ 'uses' => 'Admin\AccountController@show', 'as' => 'showaccount' ]);
  Route::get('admin/newAccount', [ 'uses' => 'Admin\AccountController@create', 'as' => 'newaccount' ]);
  Route::post('admin/storeAccount', [ 'uses' => 'Admin\AccountController@store', 'as' => 'saveaccount' ]);
  Route::post('admin/editAccount/{id}', [ 'uses' => 'Admin\AccountController@update', 'as' => 'editaccount' ]);
  Route::get('admin/destroyAccount/{id}', [ 'uses' => 'Admin\AccountController@destroy', 'as' => 'destroyaccount' ]);

  //Rutas para tipos de piezas
  Route::get('admin/loadtypespieces', [ 'uses' => 'Admin\PiecesTypesController@index', 'as' => 'loadtypespieces']);
  Route::get('admin/newPieceType', [ 'uses' => 'Admin\PiecesTypesController@create', 'as' => 'newpiecetype' ]);
  Route::post('admin/storePieceType', [ 'uses' => 'Admin\PiecesTypesController@store', 'as' => 'savepiecetype' ]);
  Route::get('admin/showPieceType/{id}', [ 'uses' => 'Admin\PiecesTypesController@show', 'as' => 'showpiecetype' ]);
  Route::post('admin/editPieceType/{id}', [ 'uses' => 'Admin\PiecesTypesController@update', 'as' => 'editpiecetype' ]);
  Route::get('admin/destroyPieceType/{id}', [ 'uses' => 'Admin\PiecesTypesController@destroy', 'as' => 'destroypiecepeot' ]);

  //Rutas para tipos de ot
  Route::get('admin/loadtypesot', [ 'uses' => 'Admin\OtTypesController@index', 'as' => 'loadtypesot']);
  Route::get('admin/showOtType/{id}', [ 'uses' => 'Admin\OtTypesController@show', 'as' => 'showottype' ]);
  Route::get('admin/newOtType', [ 'uses' => 'Admin\OtTypesController@create', 'as' => 'newottype' ]);
  Route::post('admin/storeOtType', [ 'uses' => 'Admin\OtTypesController@store', 'as' => 'saveottype' ]);
  Route::post('admin/editOtType/{id}', [ 'uses' => 'Admin\OtTypesController@update', 'as' => 'edittypeot' ]);
  Route::get('admin/destroyOtType/{id}', [ 'uses' => 'Admin\OtTypesController@destroy', 'as' => 'destroytypeot' ]);
  

 
  //Rutas cargar excel's
  Route::get('admin/loadUserExcel', [ 'uses' => 'Admin\UserController@demoExcel', 'as' => 'loaduserExcel' ]);
});













