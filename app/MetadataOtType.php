<?php

namespace App;

use App\MetadataOtType;
use Illuminate\Database\Eloquent\Model;

class MetadataOtType extends Model
{
	protected $table = 'metadata_ot_type';

	protected $fillable = [
		'id_ot_type', 'name', 'type', 'position', 'values', 'texts'
	];
}
