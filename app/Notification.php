<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
  protected $table = 'notifications';

  protected $fillable = [
    'id_user', 'message', 'url_message', 'readed', 'date_send'
  ];
}
