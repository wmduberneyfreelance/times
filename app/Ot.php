<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ot extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'id_ot', 'id_user', 'id_user_creator', 'id_ot_type', 'id_piece_by_project', 'id_project', 'name', 'status', 'priority', 'complexity', 'tentative_date', 'description', 'recommendations', 'metadata', 'version', 'bigmail'
  ];

  public static function filterAndPaginateBigmailByProject($idUser, $idProject, $itemPag){
    $ots = \DB::table('ots')
    ->select(
        'ots.*',
        'ot_types.name AS nameTypeOT',
        'projects.name AS nameProject',
        'accounts.name AS nameAccount',
        'clients.name AS nameClient'
    )
    ->where('ots.id_user', '=', $idUser)
    ->where('ots.id_project', '=', $idProject)
    ->where('ots.bigmail', '=', 1)
    ->orderBy('ots.id', 'desc')
    ->join('ot_types', 'ots.id_ot_type', '=', 'ot_types.id')
    ->join('projects', 'ots.id_project', '=', 'projects.id')
    ->join('accounts', 'projects.id_account', '=', 'accounts.id')
    ->join('clients', 'accounts.id_client', '=', 'clients.id')
    ->paginate($itemPag);

    return $ots;
  }

  public static function filterAndPaginateByUser($idUser, $itemPag){
    $ots = \DB::table('ots')
    ->select(
        'ots.*',
        'ot_types.name AS nameTypeOT',
        'projects.name AS nameProject',
        'accounts.name AS nameAccount',
        'clients.name AS nameClient'
    )
    ->where('ots.id_user', '=', $idUser)
    ->where('ots.status', '=', 'asignada')
    ->orderBy('ots.id', 'desc')
    ->join('ot_types', 'ots.id_ot_type', '=', 'ot_types.id')
    ->join('projects', 'ots.id_project', '=', 'projects.id')
    ->join('accounts', 'projects.id_account', '=', 'accounts.id')
    ->join('clients', 'accounts.id_client', '=', 'clients.id')
    ->paginate($itemPag);

    return $ots;
  }

  public static function filterAndPaginateByProject($idProject, $itemPag){
    $ots = \DB::table('ots')
    ->select(
      'ots.*',
      'ot_types.name AS nameTypeOT'
    )
    ->where('ots.id_project', '=', $idProject)
    ->join('ot_types', 'ots.id_ot_type', '=', 'ot_types.id')
    ->orderBy('id', 'desc')
    ->paginate($itemPag);

    return $ots;
  }

  public static function filterAndPaginateByExecutive($idUser, $itemPag){
    $ots = \DB::table('ots')
    ->select(
        'ots.*',
        'ot_types.name AS nameTypeOT',
        'projects.name AS nameProject',
        'accounts.name AS nameAccount',
        'clients.name AS nameClient'
    )
    ->where('ots.id_user_creator', '=', $idUser)
    ->where('ots.status', '<>', "completa")
    ->join('ot_types', 'ots.id_ot_type', '=', 'ot_types.id')
    ->join('projects', 'ots.id_project', '=', 'projects.id')
    ->join('accounts', 'projects.id_account', '=', 'accounts.id')
    ->join('clients', 'accounts.id_client', '=', 'clients.id')
    ->orderBy('id', 'desc')
    ->paginate($itemPag);

    return $ots;
  }

  public static function filterAndPaginate($name, $itemPag){
    if ($name == null){
      $ots = \DB::table('ots')
      ->select(
          'ots.*'
      )->orderBy('id', 'desc')
      ->paginate($itemPag);
    }else{
      $ots = \DB::table('ots')
      ->select(
          'ots.*'
      )
      ->where('ots.name', 'LIKE', "%$name%")
      ->orderBy('id', 'desc')
      ->paginate($itemPag);
    }
    return $ots;
  }

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [
      // 'password', 'remember_token',
  ];
}
