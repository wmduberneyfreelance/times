<?php

namespace App;

use App\OtType;
use Illuminate\Database\Eloquent\Model;

class OtType extends Model
{
	protected $table = 'ot_types';


    protected $fillable = [
        'name', 'email', 'deparment', 'description', 'status'
    ];

    public static function filterAndPaginate($name, $itemPag){
    	return $accounts = OtType::name($name)->orderBy('id', 'desc')->paginate($itemPag);
    }

    public function scopeName($query, $name){
    	if (trim($name) != "")
    		$query->where('name', 'LIKE', "%$name%");
    }
}
