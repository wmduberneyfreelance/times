<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PiecesByProject extends Model
{
  protected $table = 'pieces_by_project';

  protected $fillable = [
    'id_project', 'id_piece_type', 'name'
  ];
}
