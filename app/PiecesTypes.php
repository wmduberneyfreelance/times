<?php

namespace App;

use App\PiecesTypes;
use Illuminate\Database\Eloquent\Model;

class PiecesTypes extends Model
{
	protected $table = 'pieces_types';

  protected $fillable = [
    'name', 'description', 'status'
  ];

  public static function filterAndPaginate($name, $itemPag){
    return $piecesTypes = PiecesTypes::name($name)->orderBy('id', 'desc')->paginate($itemPag);
  }

  public function scopeName($query, $name){
    if (trim($name) != ""){
      $query->where('name', 'LIKE', "%$name%");
    }
  }
}
