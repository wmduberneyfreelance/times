<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{

    protected $fillable = [
        'id_team', 'name', 'description', 'status',
    ];

    public static function filterAndPaginate($name, $itemPag){
    	return $profiles = Profile::name($name)->orderBy('id', 'desc')->paginate($itemPag);
    }

    public function scopeName($query, $name){
    	if (trim($name) != "")
    		$query->where('name', 'LIKE', "%$name%");
    }

    public function scopeType($query, $type){
    	
    }
    
    // public function getNameProfileAttribute(){
    //     return "Hola " . $this->name;
    // }    

    // public function getNameAttribute(){
    //     return $this->name;
    // }


    // public function user(){
    //     return $this->hasMany('App\User');
    // }
}


