<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
	protected $fillable = [
	    'id_client', 'id_account', 'id_user_creator', 'tentative_date', 'name', 'status'
	];

  public static function filterAndPaginateByAccount($idAccount, $itemPag){
    $project = \DB::table('projects')
    ->select(
        'projects.*'
    )
    ->where('projects.id_account', '=', $idAccount)
    ->orderBy('id', 'asc')
    ->paginate($itemPag);

    return $project;
  }

  public static function filterAndPaginateByUser($name, $idUser, $itemPag){
    if ($name == null){
      $profiles = \DB::table('projects')
      ->select(
          'projects.*',
          'clients.name AS nameClient',
          'accounts.name AS nameAccount',
          \DB::raw('(SELECT count(*) FROM ots WHERE (status = "nueva" OR status = "ajustes") AND id_project = projects.id) as totalOTS')
      )->where('projects.id_user_creator', '=', $idUser)
      ->where('projects.status', '<>', "complet")
      // ->orderBy('projects.id', 'desc')
      ->orderBy('totalOTS', 'desc')
      ->join('accounts', 'accounts.id', '=', 'projects.id_account')
      ->join('clients', 'clients.id', '=', 'accounts.id_client')
      ->paginate($itemPag);
    }else{
      $profiles = \DB::table('projects')
      ->select(
        'projects.*',
        'clients.name AS nameClient',
        'accounts.name AS nameAccount',
        \DB::raw('(SELECT count(*) FROM ots WHERE (status = "nueva" OR status = "ajustes") AND id_project = projects.id) as totalOTS')
      )
      ->where('projects.status', '<>', "complet")
      ->where('projects.name', 'LIKE', "%$name%")
      ->where('projects.id_user_creator', '=', $idUser)
      // ->orderBy('id', 'desc')
      ->orderBy('totalOTS', 'desc')
      ->join('accounts', 'accounts.id', '=', 'projects.id_account')
      ->join('clients', 'clients.id', '=', 'accounts.id_client')
      ->paginate($itemPag);
    }
    return $profiles;
  }

  public static function filterAndPaginate($name, $status, $idUser, $clientProject, $accountProject, $executiveProject, $itemPag){
    $projects = Project::select(
      'projects.*',
        'clients.name AS nameClient',
        'accounts.name AS nameAccount',
        \DB::raw('(SELECT count(*) FROM ots WHERE (status = "nueva" OR status = "ajustes") AND id_project = projects.id) as totalOTS')
    )
    ->status($status, "<>")
    ->status("cancel", "<>")
    ->name($name)
    ->user($idUser)
    ->account($accountProject)
    ->orderBy('totalOTS', 'desc')
    ->join('accounts', 'accounts.id', '=', 'projects.id_account')
    ->join('clients', 'clients.id', '=', 'accounts.id_client')
    ->paginate($itemPag);

    return $projects;
  }

  public function scopeName($query, $name){
  	if (trim($name) != "")
  		$query->where('projects.name', 'LIKE', "%$name%");
  }

  public function scopeStatus($query, $status, $opetation){
    if (trim($status) != ""){
      $query->where('projects.status', $opetation, $status);
    }
  }

  public function scopeUser($query, $idUser){
    if (trim($idUser) != ""){
      $query->where('projects.id_user_creator', "=", $idUser);
    }
  }

  public function scopeAccount($query, $account){
    if (trim($account) != "")
      $query->where('id_account', '=', $account);
  }
}
