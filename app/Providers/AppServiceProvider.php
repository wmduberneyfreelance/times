<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
// use View;
// use Auth;
// use App\Notification;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      // $idUser = Auth::id();
      // $notifications = Notification::where('id_user', $idUser)->count();
      // View::share('wmkey', $notifications);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
