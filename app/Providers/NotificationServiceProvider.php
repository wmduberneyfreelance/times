<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;

class NotificationServiceProvider extends ServiceProvider
{
  // protected $defer = true;
  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot()
  {
    // View::share('wmkey', 'valuewm');
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register()
  {
      //
  }
}
