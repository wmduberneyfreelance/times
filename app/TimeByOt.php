<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeByOt extends Model
{
  protected $table = 'time_by_ots';

  protected $fillable = [
      'id_user_creator', 'id_assigned_user', 'id_ot', 'hours', 'date_assigned'
  ];
}
