<?php

namespace App;

// use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Authenticatable;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Caffeinated\Shinobi\Traits\ShinobiTrait;


// class User extends Authenticatable
class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{

  // use Notifiable;
  // use ShinobiTrait;
  use Authenticatable, CanResetPassword, ShinobiTrait;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'id_team', 'name', 'position', 'gender', 'email', 'password', 'rol', 'id_profile', 'status'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
  ];


  public function isAdmin(){
    return $this->rol === 'superadmin';
  }
  public function isModerator(){
    if ($this->rol === 'moderator' || $this->rol === 'executive' || $this->rol === 'superadmin'){
      return true;
    }else{
      return false;
    }
    // return $this->rol === 'moderator';
  }


  public static function filterAndPaginate($name, $itemPag){
      if ($name == null){
          $users = \DB::table('users')
          ->select(
              'users.*',
              'profiles.name AS nameProfile'
          )->orderBy('id', 'asc')
          ->leftJoin('profiles', 'profiles.id', '=', 'users.id_profile')
          ->paginate($itemPag);
      }else{
          $users = \DB::table('users')
          ->select(
              'users.*',
              'profiles.name AS nameProfile'
          )
          ->where('users.name', 'LIKE', "%$name%")
          ->orderBy('id', 'asc')
          ->leftJoin('profiles', 'profiles.id', '=', 'users.id_profile')
          ->paginate($itemPag);
      }

      return $users;
  }

  public function scopeName($query, $name){
    if (trim($name) != "")
      $query->where('name', 'LIKE', "%$name%");
  }

  public function profile(){
    $this->hasOne('Profile');
  }




    
}
