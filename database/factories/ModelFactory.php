<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        // 'id_profile' => '0',
        'status' => 'active',
        'remember_token' => str_random(10),
        'rol' => $faker->randomElement(['user', 'executive', 'developer', 'moderator']),
    ];
});


$factory->define(App\Ot::class, function (Faker\Generator $faker) {
    return [
        'id_ot' => str_random(10),
        'id_user' => $faker->randomElement(['1', '2', '3', '4', '5', '6', '7']),
        'name' => $faker->name,
        'status' => $faker->randomElement(['nueva', 'asignada', 'completa', 'cancelada']),
    ];
});


$factory->define(App\ActionsProfile::class, function (Faker\Generator $faker) {
    return [
        'action' => str_random(10),
        'funcionality' => str_random(10),
    ];
});


$factory->define(App\Profile::class, function (Faker\Generator $faker) {
    return [
        'id_team' => '0',
        'name' => str_random(10),
        'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        'status' => 'active',
    ];
});


$factory->define(App\Client::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'status' => $faker->randomElement(['active', 'inactive']),
    ];
});

$factory->define(App\Account::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'id_client' => $faker->randomElement(['1', '2', '3', '4', '5', '6', '7', '8']),
        'status' => $faker->randomElement(['active', 'inactive']),
    ];
});


$factory->define(App\OtType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'status' => $faker->randomElement(['active', 'inactive']),
    ];
});


$factory->define(App\piecesTypes::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'status' => $faker->randomElement(['active', 'inactive']),
    ];
});


