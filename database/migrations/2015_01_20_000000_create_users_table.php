<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_team');
            $table->string('name');
            $table->string('position', 80)->nullable();
            $table->enum('gender', ['masculine', 'female'])->nullable();
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->enum('rol', ['user', 'executive', 'developer', 'creative', 'moderator', 'admin', 'superadmin']);
            $table->integer('id_profile')->unsigned()->nullable();
            $table->enum('status', ['active', 'inactive']);

            $table->string('api_token', 60)->unique()->nullable();

            $table->rememberToken();
            $table->timestamps();


            $table->foreign('id_profile')->references('id')->on('profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
