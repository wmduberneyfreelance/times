<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionsByProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actions_by_profile', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_profile')->unsigned();
            $table->integer('id_action')->unsigned();
            $table->timestamps();

            $table->foreign('id_profile')->references('id')->on('profiles')->onDelete('cascade');
            $table->foreign('id_action')->references('id')->on('actions_profiles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('actions_by_profile');
    }
}
