<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_account')->unsigned();
            $table->integer('id_user_creator')->unsigned();
            $table->date('tentative_date');
            $table->string('name', 120);
            $table->enum('status', ['new', 'complet', 'pending', 'process', 'trafic', 'cancel'])->nullable();
            $table->timestamps();

            $table->foreign('id_account')->references('id')->on('accounts');
            $table->foreign('id_user_creator')->references('id')->on('users');
            
           });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
