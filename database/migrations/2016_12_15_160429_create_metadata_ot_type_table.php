<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetadataOtTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
      Schema::create('metadata_ot_type', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('id_ot_type')->unsigned();
        $table->string('name', 120);
        $table->enum('type', ['text', 'select', 'select_mult', 'textarea', 'checkbox', 'radio_button']);
        $table->integer('position')->unsigned()->nullable();
        $table->mediumText('values');
        $table->mediumText('texts');
        $table->timestamps();

        $table->foreign('id_ot_type')
                ->references('id')->on('ot_types')
                ->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('metadata_ot_type');
    }
}
