<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePiecesByProjectTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('pieces_by_project', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('id_project')->unsigned();
      $table->integer('id_piece_type')->unsigned();
      $table->string('name', 80);
      $table->timestamps();

      $table->foreign('id_project')->references('id')->on('projects');
      $table->foreign('id_piece_type')->references('id')->on('pieces_types');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('pieces_by_project');
  }
}
