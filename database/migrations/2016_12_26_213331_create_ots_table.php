<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ots', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_ot', 20)->unique();
            $table->integer('id_user')->unsigned()->nullable();
            $table->integer('id_user_creator')->unsigned();
            $table->integer('id_ot_type')->unsigned();
            $table->integer('id_piece_by_project')->unsigned();
            $table->integer('id_project')->unsigned();
            $table->string('name', 80);
            $table->enum('status', ['nueva', 'asignada', 'completa', 'ajustes', 'cancelada']);
            $table->enum('priority', ['baja', 'media', 'alta']);
            $table->enum('complexity', ['baja', 'media', 'alta']);
            $table->date('tentative_date');
            // $table->string('departament', 20);
            $table->mediumText('description');
            $table->mediumText('recommendations');
            $table->text('metadata');
            $table->mediumInteger('version');
            $table->smallInteger('bigmail')->default(0);
            $table->timestamps();
            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_user_creator')->references('id')->on('users');
            $table->foreign('id_piece_by_project')->references('id')->on('pieces_by_project');
            $table->foreign('id_project')->references('id')->on('projects');
            $table->foreign('id_ot_type')->references('id')->on('ot_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ots');
    }
}
