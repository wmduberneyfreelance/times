<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('comments', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('id_user')->unsigned();
      $table->integer('id_ot')->unsigned();
      $table->string('name_user', 80);
      $table->text('comments');
      $table->timestamps();
      $table->foreign('id_user')->references('id')->on('users');
      $table->foreign('id_ot')->references('id')->on('ots');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('comments');
  }
}
