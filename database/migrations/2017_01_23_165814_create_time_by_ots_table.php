<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeByOtsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('time_by_ots', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('id_user_creator')->unsigned();
      $table->integer('id_assigned_user')->unsigned();
      $table->integer('id_ot')->unsigned();
      $table->smallInteger('hours');
      $table->dateTime('date_assigned');
      $table->timestamps();

      $table->foreign('id_user_creator')->references('id')->on('users');
      $table->foreign('id_assigned_user')->references('id')->on('users');
      $table->foreign('id_ot')->references('id')->on('ots');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('time_by_ots');
  }
}
