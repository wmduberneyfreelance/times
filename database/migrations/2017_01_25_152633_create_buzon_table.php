<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuzonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('buzon', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('id_sending_user')->unsigned();
        $table->integer('id_receiving_user')->unsigned();
        $table->mediumText('message');
        $table->string('hours', 10);
        $table->smallInteger('readed')->default(0);
        $table->dateTime('date_send');
        $table->timestamps();

        $table->foreign('id_sending_user')->references('id')->on('users');
        $table->foreign('id_receiving_user')->references('id')->on('users');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('buzon');
    }
}
