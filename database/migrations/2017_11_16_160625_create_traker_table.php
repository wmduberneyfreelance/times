<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrakerTable extends Migration
{
    public function up()
    {
      Schema::create('trakers', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('id_user')->unsigned();
        $table->integer('id_ot')->unsigned();
        $table->mediumText('message');
        $table->smallInteger('open')->default(0);
        $table->dateTime('date_start');
        $table->dateTime('date_done');
        $table->timestamps();

        $table->foreign('id_user')->references('id')->on('users');
        $table->foreign('id_ot')->references('id')->on('ots');
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('trakers');
    }
}
