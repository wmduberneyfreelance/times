<?php

use Illuminate\Database\Seeder;

class AccountTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		factory(App\Account::class, 20)->create();

    	$arrayNameAccounts = array("Sura Seguros", "Sura ARL", "Sura EPS", "Sura IPS", "Bancolombia Personas", "Bancolombia Preferencial", "Bancolombia Personal Plus", "Bancolombia Jóvenes", "Mastercard");
    	
    	for ($i = 0; $i<count($arrayNameAccounts); $i++){
    		$baseC = rand(1, 8);

		    factory(App\Account::class)->create([
    			'name' => $arrayNameAccounts[$i],
    			'id_client' => $baseC,
    	        'status' => 'active',
    		]);
    	}
    }
}
