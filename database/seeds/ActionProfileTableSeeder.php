<?php

use Illuminate\Database\Seeder;

class ActionProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$arrayActions = array("Crear", "Modificar", "Consultar", "Eliminar");
    	$arrayFunctionalities = array("Perfiles", "Usuarios", "Clientes", "Cuentas", "Proyecto", "Tipo de proyecto");

    	for ($i = 0; $i<count($arrayActions); $i++){
    		for ($j=0; $j<count($arrayFunctionalities); $j++){
			    factory(App\ActionsProfile::class)->create([
					'action' => $arrayActions[$i],
			        'funcionality' => $arrayFunctionalities[$j],
				]);
    		}
    	}
    }
}
