<?php

use Illuminate\Database\Seeder;

class ClientTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
  	$arrayNameClients = array("Mastercard", "Bancolombia", "Sura", "Protección", "Familia", "Visa", "Epm", "Coca Cola", "Jenos Pizza");

  	for ($i = 0; $i<count($arrayNameClients); $i++){
	    factory(App\Client::class)->create([
  			'name' => $arrayNameClients[$i],
  	        'status' => 'active',
  		]);
  	}

      // factory(App\Client::class, 30)->create();
  }
}
