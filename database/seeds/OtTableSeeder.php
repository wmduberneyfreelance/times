<?php

use Illuminate\Database\Seeder;

class OtTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(App\Ot::class)->create([
    		'id_ot' => str_random(10),
            'id_user' => '1',
    		'name' => "Orden de trabajo 1",
    		'status' => "nueva",
    	]);

        factory(App\Ot::class, 49)->create();
    }
}
