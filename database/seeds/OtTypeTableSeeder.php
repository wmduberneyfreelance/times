<?php

use Illuminate\Database\Seeder;

class OtTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$arrayNamePT = array("Wireframe", "Correo electrónico", "Correo Directo", "Landing pages", "Brandbook", "Sitio web", "Data cleaning", "Animación Flash");
    	
    	for ($i = 0; $i<count($arrayNamePT); $i++){
		    factory(App\OtType::class)->create([
    			'name' => $arrayNamePT[$i],
    	        'status' => 'active',
    		]);
    	}
    }
}
