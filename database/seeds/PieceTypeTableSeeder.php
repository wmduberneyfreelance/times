<?php

use Illuminate\Database\Seeder;

class PieceTypeTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
  	$arrayNamePT = array("Email", "Banner", "Landing Page", "Sitio", "One Page", "Generico", "Otra");
  	
  	for ($i = 0; $i<count($arrayNamePT); $i++){
	    factory(App\piecesTypes::class)->create([
  			'name' => $arrayNamePT[$i],
        'status' => 'active',
  		]);
  	}
  }
}
