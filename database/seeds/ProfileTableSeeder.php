<?php

use Illuminate\Database\Seeder;

class ProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrayProfiles = array("Ejecutivo", "Tráfico TI", "Tráfico creativos", "Tráfico Data", "Gerencia", "TI", "Creativo", "Data");

    	for ($i = 0; $i<count($arrayProfiles); $i++){
		    factory(App\Profile::class)->create([
				'id_team' => '0',
		        'name' => $arrayProfiles[$i],
		        'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
		        'status' => 'active',
			]);
    	}
    }
}
