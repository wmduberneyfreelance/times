<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run(){
  	factory(App\User::class)->create([
  		'name' => "WM Duberney",
  		'email' => "duberney112@gmail.com",
  		'password' => bcrypt("123456"),
  		'remember_token' => str_random(10),
          // 'id_profile' => null,
          'status' => 'active',
          'rol' => 'superadmin',
  	]);

    factory(App\User::class)->create([
      'name' => "Victor Vergara",
      'email' => "user0@gmail.com",
      'password' => bcrypt("123456"),
      'remember_token' => str_random(10),
      'status' => 'active',
      'rol' => 'superadmin',
    ]);

    factory(App\User::class)->create([
      'name' => "Ejecutiva robledo",
      'email' => "user1@gmail.com",
      'password' => bcrypt("123456"),
      'remember_token' => str_random(10),
      // 'id_profile' => null,
      'status' => 'active',
      'rol' => 'executive',
    ]); 

    factory(App\User::class)->create([
      'name' => "Victor Vergara",
      'email' => "user2@gmail.com",
      'password' => bcrypt("123456"),
      'remember_token' => str_random(10),
      // 'id_profile' => null,
      'status' => 'active',
      'rol' => 'creative',
    ]); 

    factory(App\User::class)->create([
      'name' => "Alejandro Benjumea",
      'email' => "user3@gmail.com",
      'password' => bcrypt("123456"),
      'remember_token' => str_random(10),
      // 'id_profile' => null,
      'status' => 'active',
      'rol' => 'developer',
    ]); 

    factory(App\User::class)->create([
      'name' => "Tráfico Creativos",
      'email' => "user4@gmail.com",
      'password' => bcrypt("123456"),
      'remember_token' => str_random(10),
      // 'id_profile' => null,
      'status' => 'active',
      'rol' => 'moderator',
    ]); 

    // factory(App\User::class, 49)->create();
  }
}