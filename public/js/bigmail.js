/*
  author: Jorge Alejandro Benjumea
  email: j.alejandro.benjumea@gmail.com
  Last update: 2016/12/20 07:58:10
*/
/*globals $, CodeMirror*/

Array.prototype.move = function (old_index, new_index) {
  if (new_index >= this.length) {
    var k = new_index - this.length;
    while ((k--) + 1) {
      this.push(undefined);
    }
  }
  this.splice(new_index, 0, this.splice(old_index, 1)[0]);
  return this; // for testing purposes
};

function isEmpty (str) {
  // Checking if a string is empty, null or undefined
  return (!str || 0 === str.length);
}

/**
 * [htmlCode description]
 * @param  {[type]} value [description]
 * @return {[type]}       [description]
 */
function htmlCode(value) {
  // Pass the arguments off to the parent constructor
  // in case there is any magic that the String constructor is going.
  String.apply(this, arguments);
  // Store the value of our string.
  this.value = value;
}
htmlCode.prototype = $.extend(
  // Extend a STRING object instance.
  new String(),
  // Define class functions.
  {
    getValue: function() {
        return(this.value);
    },
    setValue: function(value) {
        this.value = value ;
        // Return this object for method chaining.
        return(this);
    },
    // I convert the object to a string representation.
    // This needs to evaluate the string because it gets used in things like the replace() method.
    toString: function() {
        return(this.valueOf());
    },
    // I get the value of the string (in its fully evaluated format with integrated bindings).
    valueOf: function() {
        return this.value;
    },
    removeComments: function() {
      this.value = this.value.replace(/<!--[\s\S]*?-->/gi, '');
    },
    removeEmptyLines : function() {
      this.value = this.value.replace(/^\s*\n/gm, '');
    },
    clean: function() {
      this.removeComments();
      this.removeEmptyLines();
      return this.value;
    }
})

/**
 * [bigMail description]
 * @param  {[type]} html    [description]
 * @param  {[type]} options [description]
 * @return {[type]}         [description]
 */
function bigMail(html, settings) {
  var _ = this;

  if (isEmpty(html)) {
    console.error('No valid html code found');
    return;
  }

  _.defaults = {
    account: null,
    cutType: null,
    emailBackground: '#ffffff',
    emailTitle: 'untitled',
    images: {
      toLink: [],
      toText: []
    },
    preheader: null,
    legalAdvice: {
        header: '<tr><td>&nbsp;</td><td style="font-family: Arial, Helvetica, sans-serif; font-size: 0.7em; color:#666; text-align:center; background-color: #ffffff;"><p style="margin: 0 0 5px;"><br />Para asegurar la entrega de nuestras comunicaciones, por favor agregue <br />xxx@xxxxx.com a su libreta de direcciones.</p><p style="margin: 0 0 5px;">Si no puede ver correctamente las im&aacute;genes de este correo <a href="#">haga clic aqu&iacute;.</a><br /><br /></p></td><td>&nbsp;</td></tr>',
        footer: '<tr><td></td><td style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#666; text-align:center; background-color: #ffffff;"><p style="margin: 0 0 5px;"><br />Nota: para garantizar que la informaci&oacute;n de este correo llegue correctamente, <br />es posible que algunas palabras lleguen sin tilde para evitar la desconfiguraci&oacute;n <br />de algunos caracteres.</p><p style="margin: 0 0 5px;"><strong>Este correo electr&oacute;nico ha sido enviado a !*email*!.</strong> <br /><br /></p></td><td></td></tr>'
    }
  };

  _.initials = {
  };

  $.extend(_, _.initials);

  _.options = $.extend(true, _.defaults, settings);

  _.html = {
    initial: html
  };

  _.onWindowResize = $.proxy(_.onWindowResize, _);

  _.init();
}

bigMail.prototype.init = function() {
  var _, htmlString;

  _ = this;
  htmlString = _.html.initial;

  // Fix Tables Fireworks CS6 bug align property
  htmlString = htmlString.replace(/\salign=""left"/gi, '');

  // Get DOCTYPE, HEAD and BODY strings
  this.html.doctype = new htmlCode(htmlString.substring(0, htmlString.indexOf('<head>')));
  this.html.head = new htmlCode(htmlString.match(/<head[^>]*>[\s\S]*<\/head>/gi)[0]);
  this.html.body = new htmlCode(htmlString.match(/<body[^>]*>[\s\S]*<\/body>/gi)[0]);

  //   // Validate HTML code
  //   if (head === null || body === null) {
  //     return -1;
  //   }

  // Aditional info
  this.date = new Date(/(Created )(.*)(\-->)/.exec(this.html.head)[2].split(' ').move(6,3).join(' '));

  this.fwtable = {};
  /(<!-- fwtable )(.*)( -->)/
    .exec(this.html.body)[2]
    .split('" ')
      .forEach(function(meta) {
        _.fwtable[meta.substring(0, meta.indexOf('='))] = meta.substring(meta.indexOf('=')+1, meta.length).trim().replace(/"/g, '');
      });


  // Clean code
  this.html.doctype.clean();
  this.html.head.clean();
  this.html.body.clean();

  // Define jquery objects
  this.html.$head = $('<div />', {html: this.html.head.valueOf()});
  this.html.$body = $('<div />', {html: this.html.body.valueOf()});

  // Email title
  this.html.$head.find('title').html(_.options.emailTitle);

  // Remove style tag
  this.html.$head.find('style').remove();

  // Obtain current width
  this.width = this.html.$body.find("> table:first").attr("width");

  // Horizontal center align
  this.html.$body.find('> table:first').removeAttr('width').css({'width': '100%'})

  // Remove style property in tables
  this.html.$body.find("td table").removeAttr("style");

  // Cut type
  switch (_.options.cutType) {
    case "bancolombia":
      // Preheader
      if (_.options.preheader) {
        this.html.$body
          .find('> table:first')
          .prepend(
            $('<tr />').append(
              $('<td />')
                .html(_.options.preheader)
                .css({
                  'width': this.width + 'px',
                  'padding': '20px',
                  'backgroundColor': '#F8CF09',
                  'color': '#244786',
                  'font-family': 'arial, helvetica',
                  'font-size': 'smaller'
                })
            )
          );
      }

      // Fix in-line style attribute for image tags
      this.html.$body.find('img').each(function() {
        $(this.parentNode).css({'background-color': _.options.emailBackground, 'vertical-align': 'top', 'width': this.width + 'px'});
        // $(this).css({'width': '100%', 'display': 'block', 'border': 'none'}).removeAttr('id name width height');
        $(this).css({'width': '100%', 'display': 'block', 'border': 'none'}).removeAttr('id name width height');
      });

      // Tables
      this.html.$body.find('td table').each(function() {
        $(this.parentNode).css({'width': this.width + 'px'});
        $(this).removeAttr('width').css({'width': '100%'});
      });
      break;

    case "generic":
      // Clean images tags
      this.html.$body.find('img').each(function(){
        $(this)
          .css({"display": "block", "border": "none", 'height': this.height + 'px', 'width': this.width + 'px'})
          .removeAttr("id name height width");
      })
      break;

    case "none":
      break;

    default:
      console.error("Undefined cut type");
  }

  //
  this.html.$body.find('> table:first > tbody > tr').prepend('<td></td>').append('<td></td>');
  this.html.$body.find('> table:first tr:nth-of-type(1) td:empty').html('&nbsp;');

  // Inject legal advices
  this.html.$body.find('table:first > tbody').prepend(_.options.legalAdvice.header).append(_.options.legalAdvice.footer);

  // regExp, subString
  // [Delete tbody tags due to browser render, Delete medium word in border property, ]
  var reg = [[/<*.tbody>/gi, ''], [/medium none/gi, 'none'], [/></g, '>\n<']];
  var bodyHtml = this.html.$body.html();
  reg.forEach(function(value) {
    bodyHtml = bodyHtml.replace(value[0], value[1]);
  })

  // // New code assembly
  // doctype
  var newHead = '<head>' + this.html.$head.html() + '</head>\n';
  var newBody = '<body>' + bodyHtml + '\n</body>\n</html>';

  // var newHtmlString = doctype + newHead + newBody;
  this.html.output = this.html.doctype + '\n' + newHead + newBody;
};

bigMail.prototype.print = function($wrapper) {
  var tdSelected = [];

  function toggleArrayItem(a, v) {
    var i = a.indexOf(v);
    if (i === -1) {
      a.push(v);
    } else {
      a.splice(i, 1);
    }
  }
  
  $wrapper
    .empty()
    .append(this.html.$body)
    .find('img')
      .on('click', function(event) {
        $this = $(this);
        globalImage = $this;

        // toggleArrayItem(tdSelected, $(this).attr('src'));
        $this.toggleClass('selected');
        
        var position = {
          x: $this.offset().left,
          y: $this.offset().top
        };
        $(".editorBigmail").css({
          "display":"block",
          "margin-left": (event.pageX-$(".editorBigmail").height()) + "px",
          "margin-top": event.pageY + "px"
        });
      })
}

bigMail.prototype.iniEditor = function($wrapper) {
  $wrapper = $($wrapper);
  $wrapper.addClass("bigEditor");
  // console.log("Start editor");
}
bigMail.prototype.clean = function() {
 return this.html.output;
};

bigMail.prototype.onWindowResize = function() {

};


var globalFiles = null;
var globalImage = null;
var globalColors = {

}

/*****************************************************************************/
function iniBigMail() {
  var cadHtml = '<table class="table table-hover"><tr><th>#</th><th>Nombre</th><th></th><th></th></tr>';
  for (var i=0; i<globalFiles.files.length; i++){
    var obj = globalFiles.files[i];
    var nameFile = obj.name;
    var extFile = nameFile.substring(nameFile.length-4, nameFile.length);
    if (extFile == '.zip'){
      cadHtml += '<tr>' +
                    '<td>'+ (i+1) +'</td>' +
                    '<td>' +
                      '<div>' + nameFile  + '</div>' +
                    '</td>' +
                    '<td width="25">' +
                      '<a class="unzipFile" data-url="'+ obj.url +'" href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>' +
                    '</td>' +
                    '<td width="25">' +
                      '<a class="unzipFile" data-url="'+ obj.url +'" href="#"><i class="fa fa-file-archive-o" aria-hidden="true"></i></a>' +
                    '</td>' +
                  '</tr>';
    }
  }
  cadHtml += '</table>';
  $(".summaryZips").html(cadHtml);

  $(".unzipFile").on("click", function(evt){
    evt.preventDefault();
    var url = $(this).data("url");
    var path = $.trim($("#urlUnzipBigmail").val());
    unZipFiles(url, path);
  });
}


function iniEditorBigMail() {
  var example, mailExample;
  example = {
    code: '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><!-- saved from url=(0014)about:internet --><html xmlns="http://www.w3.org/1999/xhtml"><head><title>mail_prueba.png</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><style type="text/css">td img {display: block;}</style><!--Fireworks CS6 Dreamweaver CS6 target.  Created Wed Dec 21 08:45:14 GMT-0500 (COT) 2016--></head><body bgcolor="#ffffff"><table style="display: inline-table;" border="0" cellpadding="0" cellspacing="0" width="650"><!-- fwtable fwsrc="mail_prueba.png" fwpage="Página 1" fwbase="mail_prueba.png" fwstyle="Dreamweaver" fwdocid = "182379060" fwnested="1" --><tr><td><img name="mail_prueba_r1_c1" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r1_c1.jpg" width="650" height="170" id="mail_prueba_r1_c1" alt="" /></td></tr><tr><td><img name="mail_prueba_r2_c1" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r2_c1.jpg" width="650" height="201" id="mail_prueba_r2_c1" alt="" /></td></tr><tr><td><table style="display: inline-table;" align=""left" border="0" cellpadding="0" cellspacing="0" width="650"><tr><td><img name="mail_prueba_r3_c1" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r3_c1.jpg" width="71" height="36" id="mail_prueba_r3_c1" alt="" /></td><td><img name="mail_prueba_r3_c2" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r3_c2.jpg" width="526" height="36" id="mail_prueba_r3_c2" alt="" /></td><td><img name="mail_prueba_r3_c7" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r3_c7.jpg" width="53" height="36" id="mail_prueba_r3_c7" alt="" /></td></tr></table></td></tr><tr><td><table style="display: inline-table;" align=""left" border="0" cellpadding="0" cellspacing="0" width="650"><tr><td><img name="mail_prueba_r4_c1" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r4_c1.jpg" width="71" height="165" id="mail_prueba_r4_c1" alt="" /></td><td><img name="mail_prueba_r4_c2" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r4_c2.jpg" width="526" height="165" id="mail_prueba_r4_c2" alt="" /></td><td><img name="mail_prueba_r4_c7" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r4_c7.jpg" width="53" height="165" id="mail_prueba_r4_c7" alt="" /></td></tr></table></td></tr><tr><td><img name="mail_prueba_r5_c1" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r5_c1.jpg" width="650" height="45" id="mail_prueba_r5_c1" alt="" /></td></tr><tr><td><table style="display: inline-table;" align=""left" border="0" cellpadding="0" cellspacing="0" width="650"><tr><td><img name="mail_prueba_r6_c1" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r6_c1.jpg" width="71" height="26" id="mail_prueba_r6_c1" alt="" /></td><td><img name="mail_prueba_r6_c2" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r6_c2.jpg" width="526" height="26" id="mail_prueba_r6_c2" alt="" /></td><td><img name="mail_prueba_r6_c7" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r6_c7.jpg" width="53" height="26" id="mail_prueba_r6_c7" alt="" /></td></tr></table></td></tr><tr><td><img name="mail_prueba_r7_c1" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r7_c1.jpg" width="650" height="134" id="mail_prueba_r7_c1" alt="" /></td></tr><tr><td><img name="mail_prueba_r8_c1" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r8_c1.jpg" width="650" height="26" id="mail_prueba_r8_c1" alt="" /></td></tr><tr><td><img name="mail_prueba_r9_c1" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r9_c1.jpg" width="650" height="14" id="mail_prueba_r9_c1" alt="" /></td></tr><tr><td><table style="display: inline-table;" align=""left" border="0" cellpadding="0" cellspacing="0" width="650"><tr><td><img name="mail_prueba_r10_c1" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r10_c1.jpg" width="169" height="36" id="mail_prueba_r10_c1" alt="" /></td><td><img name="mail_prueba_r10_c3" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r10_c3.jpg" width="481" height="36" id="mail_prueba_r10_c3" alt="" /></td></tr></table></td></tr><tr><td><table style="display: inline-table;" align=""left" border="0" cellpadding="0" cellspacing="0" width="650"><tr><td><img name="mail_prueba_r11_c1" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r11_c1.jpg" width="325" height="144" id="mail_prueba_r11_c1" alt="" /></td><td><img name="mail_prueba_r11_c5" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r11_c5.jpg" width="325" height="144" id="mail_prueba_r11_c5" alt="" /></td></tr></table></td></tr><tr><td><img name="mail_prueba_r12_c1" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r12_c1.jpg" width="650" height="40" id="mail_prueba_r12_c1" alt="" /></td></tr><tr><td><table style="display: inline-table;" align=""left" border="0" cellpadding="0" cellspacing="0" width="650"><tr><td><img name="mail_prueba_r13_c1" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r13_c1.jpg" width="229" height="106" id="mail_prueba_r13_c1" alt="" /></td><td><img name="mail_prueba_r13_c4" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r13_c4.jpg" width="156" height="106" id="mail_prueba_r13_c4" alt="" /></td><td><img name="mail_prueba_r13_c6" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r13_c6.jpg" width="265" height="106" id="mail_prueba_r13_c6" alt="" /></td></tr></table></td></tr><tr><td><img name="mail_prueba_r14_c1" src="http://ondata.com.co/pruebas/revision/test/mail_prueba_r14_c1.jpg" width="650" height="17" id="mail_prueba_r14_c1" alt="" /></td></tr></table></body></html>',
    options: {
      cutType: 'bancolombia', // 'generic'
      emailBackground: '#ffffff',
      emailTitle: 'Título de prueba',
      preheader: 'Preheader de prueba',
      legalAdvice: {
        header: '<!-- legal prueba header -->',
        footer: '<!-- legal prueba footer -->'
      }
    }
  };
  mailExample = new bigMail(example.code, example.options);
  // console.log(mailExample);
  // console.log(mailExample.clean());
  mailExample.print($('#email-preview'));
  mailExample.iniEditor('#email-preview');
  iniColorpicker();

  $(".buttonAceptEditor").on("click", function(evt){
    evt.preventDefault();
    $(".editorBigmail").css("display", "none");
    $td = globalImage.parent();
    $td.empty();
    $td.html($(".editorBigmail textarea").val());
    console.log(globalColors);
    $td.css({
      "text-align": "left",
      "background-color": globalColors.bgColor,
      "color": globalColors.txtColor
    });

    // globalImage.css({
    //   "opacity":"0.3"
    // });
    console.log("Juemadre! que hago");
  });
}

function iniColorpicker(){
  $('#bgColor').ColorPicker({
    color: '#0000ff',
    onShow: function (colpkr) {
      $(colpkr).fadeIn(500);
      return false;
    },
    onHide: function (colpkr) {
      $(colpkr).fadeOut(500);
      return false;
    },
    onChange: function (hsb, hex, rgb) {
      globalColors.bgColor = '#' + hex;
      $('#bgColor div').css('backgroundColor', '#' + hex);
    }
  });

  $('#textColor').ColorPicker({
    color: '#0000ff',
    onShow: function (colpkr) {
      $(colpkr).fadeIn(500);
      return false;
    },
    onHide: function (colpkr) {
      $(colpkr).fadeOut(500);
      return false;
    },
    onChange: function (hsb, hex, rgb) {
      globalColors.txtColor = '#' + hex;
      $('#textColor div').css('backgroundColor', '#' + hex);
    }
  });
}
