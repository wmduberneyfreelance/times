var swPeticionDelete = 0;
var contFields = 0;
var objSerField = [];
var swRequestFields = 0;
var swRequestBigmail = 0;
var swRequestTracker = 0;
var swRequestUnzipFiles = 0;
var globalCountFieldsVal = 0;
var swRequestCommentsUser = 0;
var swRequestDeleteFile = 0;
var swRequestEventUser = 0;
var swRequestEditEvent = 0;
var mainSWMetadata = 0;
var iniTracker = 0;
var swRequestMessageBuzon = 0;
var swRequestLoadMessageBuzon = 0;

var tiempo = {
  hora: 0,
  minuto: 0,
  segundo: 0
};

$(document).ready(function(evt){
	$(window).bind("resize", function() {
    updateMask();
	});

	$(window).load(function(e){
    updateMask();
	});

  $(".search-query").on("keyup", function(evt){
    filterChatUser();
  });

  $(".plusCalendar").on("click", function(evt){
    showAddNewEvent("");
  })

  $(".closeMaskEvent").on("click", function(evt){
    closeModalEvent("NO");
  });

  $(document).keyup(function(e) {
    if (e.keyCode == 27) { // escape key maps to keycode `27`
      closeModalEvent("NO");
    }
  });

  var tiempo_corriendo = null;

  $("#btn-comenzar").click(function(){
    if ( $(this).text() == 'Comenzar' )
    {

        $(this).text('Detener');                        
        tiempo_corriendo = setInterval(function(){
            // Segundos
            tiempo.segundo++;
            if(tiempo.segundo >= 60)
            {
                tiempo.segundo = 0;
                tiempo.minuto++;
            }      

            // Minutos
            if(tiempo.minuto >= 60)
            {
                tiempo.minuto = 0;
                tiempo.hora++;
            }

            $("#hour").text(tiempo.hora < 10 ? '0' + tiempo.hora : tiempo.hora);
            $("#minute").text(tiempo.minuto < 10 ? '0' + tiempo.minuto : tiempo.minuto);
            $("#second").text(tiempo.segundo < 10 ? '0' + tiempo.segundo : tiempo.segundo);
        }, 1000);
        if (iniTracker == 1){
          setTrackerOT('open');
        }
    }
    else 
    {
      if (iniTracker == 1){
        setTrackerOT('open');
      }
      $(this).text('Comenzar');
      clearInterval(tiempo_corriendo);
    }
  })

  var cTime = $("#timeCurrentTracker").val();
  
  if (cTime != 0 && cTime != undefined){
    var aT = cTime.split(":");
    tiempo.hora = parseInt(aT[0]);
    tiempo.minuto = parseInt(aT[1]);
    tiempo.segundo = parseInt(aT[2]);
    $("#btn-comenzar").click();
  }
  iniTracker = 1;
  

  $('#modalDeletePieza').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var recipient = button.data('url') 
    var modal = $(this);
    $("#linkDeletePieza").attr("href", recipient);
  });

  $("h4.panel-title").on("dblclick",function(){
    $("h4.panel-title").find(".actionPieza").slideToggle();
  });


  $(".itemUserChat").on("click", function(evt){
    var idUser = $(this).data('iduser');
    var nameUser = $(this).data('nameuser');
    loadChatUser(idUser, nameUser);
  });

  $("#accountProject").on("change", function(evt){
    $("#mainFormShowProjects").submit();
  });

  $("#btnAddEvent").on("click", function(evt){
    evt.preventDefault();
    addNewEventCalendarUser();
  });
  

  $(".linkFilterAccount").on("click", function(evt){
    evt.preventDefault();
    var idAccount = $(this).data("id");
    $("#accountProject").val(idAccount);
    $("#accountProject").trigger( "change" );
  });

  // $("#otType").val("");

  var rUser = $("#genericColorRol").val();
  $(".mainRightZone").addClass(rUser);

	updateMask();

  $(".btnSubmitCommentsUser").on("click", function(evt){
    evt.preventDefault();
    if ($.trim($("#commentsByUser").val()) != ""){
      sendAjaxComment();
    }
  })

  $("#isBigMail").on("change", function(evt){
    var st = $("#isBigMail").prop( "checked");
    setBigMail(st);
  });

  $(".buttonPreviewFields").on("click", function(evt){
    evt.preventDefault();
    preloadInfoFormOT();
  });

  $("#typeFieldDinamic").on("change", function(evt){
    var op = $(this).val();
    loadOptions(op);
  });

  $("#otType").on("change", function(evt){
    var op = $(this).val();
    if (op != ""){
      $("#renderMetaData").css("display", "none");
      loadDataFormFileds(op);
    }else{
      globalCountFieldsVal = 0;
      mainSWMetadata = 0;
      $(".displayFormDinamic").html("");
      $("#mainDisplayFormFields").css("display", "none");
      $("#renderMetaData").css("display", "block");
    }
  });

  $("#buttonSendMessageBuzon").on("click", function(evt){
    evt.preventDefault();
    sendMessageBuzonAjax();
  });

  $("#btnAddPieceByProject").on("click", function(evt){
    evt.preventDefault();
    $("#addFormPieceByProject").submit();
  });

  $(".contentTypeProjects .closeButton, .closeMask").on("click", function(evt){
	 $(".maskFields").css("display", "none");
  });

  $(".btnCloseMaskPiece").on("click", function(evt){
    evt.preventDefault();
   $(".maskPiecesTypes").css("display", "none");
  });

  $(".btnCloseMaskDoneOT").on("click", function(evt){
    evt.preventDefault();
   $(".maskDoneOT").css("display", "none");
  });

  $(".btnCloseMaskAdjustOT").on("click", function(evt){
    evt.preventDefault();
   $(".maskAdjustOT").css("display", "none");
  });

  $(".btnLoadMaskDoneOT").on("click", function(evt){
    evt.preventDefault();
    loadMask(".maskDoneOT", ".formDoneOT");
  });

  $(".btnLoadMaskAdjustOT").on("click", function(evt){
    evt.preventDefault();
    loadMask(".maskAdjustOT", ".formAdjustOT");
  });

  $(".btnLoadMaskPiece").on("click", function(evt){
    evt.preventDefault();
    loadMask(".maskPiecesTypes", ".formPieceType");
  });

  $("#buttonSerializeFields").on("click", function(evt){
    evt.preventDefault();
    renderFieldsView();
  });

  $("#btnAddFieldTypeOT").on("click", function(evt){
    evt.preventDefault();
    loadMaskAddField();
  });

  $("#linkAddValueOptions").on("click", function(evt){
    evt.preventDefault();
    addField();
  });

  $("#btnSavePostBlog").on("click", function(evt){
    evt.preventDefault();
    $("#savePostform").submit();
  });
  
  $("#btnSaveOt").on("click", function(evt){
    evt.preventDefault();
    if (validateSendFormMetaData()){
      var metaData = JSON.stringify($("#formDinamicOT").serializeArray());
      $("#metadataOT").val(metaData);
      $("#saveFormOT").submit();
    }
  });

  $("#btnAdjustOT").on("click", function(evt){
    evt.preventDefault();
    $("#updateFormAdjustOT").submit();
  });


  $("#btnEditOt").on("click", function(evt){
    evt.preventDefault();
    var metaData = JSON.stringify($("#formDinamicOT").serializeArray());
    $("#metadataOT").val(metaData);
    $("#updateFormOT").submit();
  });

  $("#btnDoneOT").on("click", function(evt){
    evt.preventDefault();
    $("#updateFormDoneOT").submit();
  });

  $("#btnSavePieceType").on("click", function(evt){
    evt.preventDefault();
    $("#saveFormPieceType").submit();
  });

  $("#btnUpdatePieceType").on("click", function(evt){
    evt.preventDefault();
    $("#editFormPieceType").submit();
  });


	
	$("#btnSaveProject").on("click", function(evt){
		evt.preventDefault();
		$("#saveFormProject").submit();
	});

	$("#btnUpdateProject").on("click", function(evt){
		evt.preventDefault();
		$("#editFormProject").submit();
	});

	$("#btnSaveProType").on("click", function(evt){
		evt.preventDefault();
    $("#fieldsOtType").val(JSON.stringify(objSerField));
		$("#saveFormProjectType").submit();
	});

	$("#btnUpdateProType").on("click", function(evt){
		evt.preventDefault();
		$("#editFormProType").submit();
	});

	$("#btnSaveAccount").on("click", function(evt){
		evt.preventDefault();
		$("#saveFormAccount").submit();
	});

	
	$("#btnUpdateAccount").on("click", function(evt){
		evt.preventDefault();
		$("#editFormAccount").submit();
	});


	$("#btnSaveClient").on("click", function(evt){
		evt.preventDefault();
		$("#saveFormClient").submit();
	});

	$("#btnUpdateClient").on("click", function(evt){
		evt.preventDefault();
		$("#editFormClient").submit();
	});

	$("#btnSaveUser").on("click", function(evt){
		evt.preventDefault();
		$("#saveFormUser").submit();
	});
	
	$("#btnUpdateUser").on("click", function(evt){
		evt.preventDefault();
		$("#editFormUser").submit();
	});

	$("#btnUpdateProfile").on("click", function(evt){
		evt.preventDefault();

		var cboxes = document.getElementsByName('optionsPermit[]');
	    var len = cboxes.length;
	    var perm = new Array();
	    for (var i=0; i<len; i++) {
	    	if (cboxes[i].checked){
	    		perm.push(cboxes[i].value);
	    	}
	    }
	    $("#permsSelect").val(perm);

		$("#editFormProfile").submit();
	});

	$(".btnDelete").on("click", function(evt){
		evt.preventDefault();
		return;
		
		if (swPeticionDelete == 0){
			swPeticionDelete = 1;
			var row = $(this).parents("tr");
			var idProfile = row.data("id");
			var form = $("#deleteFormProfile");
			var url = form.attr("action").replace(":USER_ID", idProfile);
			var data = form.serialize();
			// var idProfile = $(this).parent().parent().data("id");
			
			$.post(url, data, function(result){
				alert(result.message);
				row.fadeOut();
				swPeticionDelete = 0;
			}).fail(function(evt){
				alert("No ha sido posible eliminar el perfil, intentanlo mas tarde.");
			});
		}
	});
});

function closeModalEvent(SWR){
  $(".maskNewEvent").css({ "display": "none"})
  $('.newEvent').removeClass('animated2 fadeInDown');
  if (SWR == "SI"){
    window.location.reload();
  }
  
}
function showAddNewEvent(date){
  if (date != ""){
    var dateNew = new Date(date);
    $('#datetimepicker1').data("DateTimePicker").date(dateNew);
    var doneDate = dateNew.setHours(dateNew.getHours() + 2);
    $('#datetimepicker2').data("DateTimePicker").date(new Date(doneDate));

  }
  $(".maskNewEvent").css({
    "opacity": "0",
    "display": "block"
  })
  $(".newEvent").css({
    "display": "none"
  })
  
  $(".maskNewEvent").animate({
    "opacity": "1"
  }, 700, function(evt){
    $(".newEvent").css({
      "display": "block"
    })
    $('.newEvent').addClass('animated2 fadeInDown');
  })
}



function filterChatUser(){
  var cadFind = $.trim($(".search-query").val());
  cadFind = cadFind.toLowerCase();

  var arrObj = $("ul.listMainUserChat li");
  for (var i=0; i<arrObj.length; i++){
    var objBase = $(arrObj[i]);
    var text = objBase.find(".header_sec strong").text();
    text = text.toLowerCase();

    if (text.indexOf(cadFind) > -1) {
      objBase.removeClass("hidden");
    }else{
      objBase.addClass("hidden");
      
    }
  }
}

function loadChatUser(idUser, nameUser){
  $(".nameUserCurrent").html('<i class="material-icons">record_voice_over</i> Estás hablando con <strong>' + nameUser + '</strong>');
  $("#idReceivingUser").val(idUser);
  loadAjaxMessagesUser();
}

function validateSendFormMetaData(){
  if (mainSWMetadata == 1){
    return true;
  }
  var metaData = JSON.stringify($("#formDinamicOT").serializeArray());
  var arrData = $("#formDinamicOT").serializeArray();
  var countVal = 0;
  var swErr = 0;
  var arrName = new Array();
  for (var i=0; i<arrData.length; i++){
    var objBase = arrData[i];
    if ($.trim(objBase.value) != ""){
      var e = arrName.indexOf(objBase.name);
      if (e == -1){
        countVal++;
        arrName.push(objBase.name);
      }
    }
  }

  if (globalCountFieldsVal > countVal){
    swErr = 1;
    var cadHtml = '<div class="alert alert-danger" role="alert">Por favor válida la siguiente información:<br><br><ul class="itemsErrors"><li>Todos los campos de <strong>Tipo de OT</strong> son Obligatorios</li></ul></div>';
    $(".mainCustomMessages").html(cadHtml);
  }else{
    $(".mainCustomMessages").html("");
  }

  // console.log(metaData);
  // console.log("Fields validados --> " + arrName);
  // console.log(globalCountFieldsVal + " >>>> " + countVal);

  if (globalCountFieldsVal == 0){
    swErr = 1;
    $("#otType").val("");
    $("#otType").parent().parent().parent().addClass("has-error");
  }else{
    $("#otType").parent().parent().parent().removeClass("has-error");
  }

  if (swErr == 1){
    return false;
  }else{
    return true;
  }  
}

function preloadInfoFormOT(){
  var nFileds = objSerField.length;
  var cadHtml = "";
  var contBreak = 0;

  for (var i=0; i<nFileds; i++){
    var fBase = objSerField[i];
    var field = '';
    switch(fBase.type){
      case "text":
        field = '<input type="text" name="'+ fBase.name +'" class="form-control">';
        break;
      case "select":
        field = '<select name="'+ fBase.name +'" class="form-control">';
        var arrV = fBase.values;
        for (var j=0; j<arrV.length; j++){
          field += '<option value="'+ arrV[j].value  +'">'+ arrV[j].text +'</option>';
        }
        field += '</select>';
        break;
      case "select_mult":
        field = '<select name="'+ fBase.name +'" class="form-control" multiple>';
        var arrV = fBase.values;
        for (var j=0; j<arrV.length; j++){
          field += '<option value="'+ arrV[j].value  +'">'+ arrV[j].text +'</option>';
        }
        field += '</select>';
        break;
      case "textarea":
        field = '<textarea name="'+ fBase.name +'" class="form-control" rows="3"></textarea>';
        break;
      case "checkbox":
        field = '';
        var arrV = fBase.values;
        for (var j=0; j<arrV.length; j++){
          field += '<div class="checkbox">' +
            '<label>' +
              '<input type="checkbox" name="'+ fBase.name +'" value="'+ arrV[j].value  +'">' + 
              arrV[j].text +
            '</label>' +
          '</div>';
        }
        break;
      case "radio_button":
        field = '';
        var arrV = fBase.values;
        for (var j=0; j<arrV.length; j++){
          field += '<div class="radio">' +
            '<label>' +
              '<input type="radio" name="'+ fBase.name +'" value="'+ arrV[j].value  +'">' + 
              arrV[j].text +
            '</label>' +
          '</div>';
        }
        break;
    }
    contBreak++;
    cadHtml += '<div class="col-md-6">'+
                  '<div class="form-group">' + 
                    '<div class="nameFieldDinamic">'+ fBase.name +'</div>' + field +
                  '</div>' + 
                '</div>';
    if (contBreak >= 2){
      contBreak = 0;
      cadHtml += '<div class="clearfix"></div>';
    }
  }

  $(".prevFormRender").html(cadHtml);
  $(".mainPreviaFormTypeOT").removeClass("hidden");
}


function addNewEventCalendarUser(){
  if (swRequestEventUser == 0){
    var e = $.trim($("#titleEvent").val());
    var d1 = $.trim($("#dateCalendarUser").val());
    var d2 = $.trim($("#dateCalendarUser2").val());

    if (e == "" || d1 == "" || d2 == ""){
      alert("Todos los campos son obligatorios");
      return;
    }

    swRequestEventUser = 1;
    var fullPath = $.trim($("#ajaxNewEventUser").val());

    var dataString = {
      "key": "w8KRCHuIf78yPWeuJQht0yPzxSAi2ZZhLmP",
      "_token": $.trim($("input[name=_token]").val()),
      "titleEvent": e,
      "startDate": d1,
      "doneDate": d2
    };

    $.ajax({
      type: "POST",
      url: fullPath,
      data: dataString,
      datatype: 'json',
      cache: false,

      success: function(data){
        swRequestEventUser = 0;
        console.log(data);
        if (data.message == "success"){
          closeModalEvent("SI");
        }else if(data.message == "blocked"){
          console.log("Bloqueado");
        }
      },
      error: function(e)
      {
        swRequestEventUser = 0;
      }
  });

  }
}

function sendMessageBuzonAjax(){
  if (swRequestMessageBuzon == 0){
    var m = $.trim($("#messageBuzon").val());
    if (m == "")
      return;

    $(".textSending").css("display", "block");
    $("#buttonSendMessageBuzon").css("opacity", "0.5");

    swRequestMessageBuzon = 1;

    var fullPath = $.trim($("#urlSendMessageBuzonAjax").val());

    var dataString = {
      "key": "w8KRCHuIf78yPWeuJQht0yPzxSAi2ZZhLmP",
      "_token": $.trim($("input[name=_token]").val()),
      "messageBuzon": m,
      "idReceivingUser": $.trim($("#idReceivingUser").val())
    };

    $.ajax({
        type: "POST",
        url: fullPath,
        data: dataString,
        datatype: 'json',
        cache: false,

        success: function(data){
          swRequestMessageBuzon = 0;
          if (data.status == "sent"){
            $("#messageBuzon").val("");
            $(".textSending").css("display", "none");
            $("#buttonSendMessageBuzon").css("opacity", "1");
            loadAjaxMessagesUser();
          }
        },
        error: function(e)
        {
          swRequestMessageBuzon = 0;
        }
    });
  }
}

function updateTimeEvent(title, dateStart, dateEnd, idEvent){
  if (swRequestEditEvent == 0){
    swRequestEditEvent = 1;
    var fullPath = $.trim($("#ajaxEditEventUser").val());

    var dataString = {
      "key": "w8KRCHuIf78yPWeuJQht0yPzxSAi2ZZhLmP",
      "_token": $.trim($("input[name=_token]").val()),
      "eventId": idEvent,
      "dateStart": dateStart,
      "dateDone": dateEnd
    };

    console.log(dataString);

    $.ajax({
        type: "POST",
        url: fullPath,
        data: dataString,
        datatype: 'json',
        cache: false,

        success: function(data){
          swRequestEditEvent = 0;
          console.log(data);
        },
        error: function(e)
        {
          swRequestEditEvent = 0;
        }
    });
  }
}

function loadAjaxMessagesUser(){
  if (swRequestLoadMessageBuzon == 0){
    swRequestLoadMessageBuzon = 1;
    var fullPath = $.trim($("#urlLoadMessageBuzonAjax").val());

    var dataString = {
      "key": "w8KRCHuIf78yPWeuJQht0yPzxSAi2ZZhLmP",
      "_token": $.trim($("input[name=_token]").val()),
      "messageBuzon": "dont worry",
      "idReceivingUser": $.trim($("#idReceivingUser").val())
    };

    $.ajax({
        type: "POST",
        url: fullPath,
        data: dataString,
        datatype: 'json',
        cache: false,

        success: function(data){
          swRequestLoadMessageBuzon = 0;
          if (data.status == "loaded"){
            displayChat(data.messages, data.sending_user, data.receiving_user);
          }
        },
        error: function(e)
        {
          swRequestLoadMessageBuzon = 0;
        }
    });
  }
}

function displayChat(messages, sendingUser, receivingUser){
  var htmlChat = '<ul class="list-unstyled">';
  var totM = messages.length;
  for (var i=0; i<totM; i++){
    var classActive = "";
    $nameU = "";
    $rolU = "";
    if (messages[i].id_sending_user == sendingUser.id){
      $nameU = sendingUser.name;
      $rolU =  sendingUser.rol;
      classActive = "activeChatUser";
    }else{
      $nameU = receivingUser.name;
      $rolU =  receivingUser.rol;
    }

    hours =  getHourFormat(messages[i].hours);

    htmlChat += '<li class="left clearfix '+ classActive +'">' +
      '<span class="chat-img1 pull-left">' +
        '<img src="/img/avatares/avatar_'+ $rolU +'.png" alt="User Avatar" class="img-circle">' +
      '</span>' +
      '<div class="chat-body1 clearfix">' +
        '<p>'+ messages[i].message +'</p>' +
        '<div class="chat_time pull-right">'+ hours +'</div>' +
      '</div>' +
    '</li>';
  }
  htmlChat += '</ul>';

  $(".chat_area").html(htmlChat)
  $('.chat_area').scrollTop($('.chat_area')[0].scrollHeight);
}

function getHourFormat(hour){
  hours =  hour.substring(0, 2);
  min = hour.substring(3, 5);
  suffix = (hours >= 12)? 'pm' : 'am';
  hours = (hours > 12)? hours -12 : hours;
  hours = (hours == '00')? 12 : hours;
  hours = hours + ":" + min + " " + suffix;

  return hours;
}

function sendAjaxComment(){
  if (swRequestCommentsUser == 0){
    swRequestCommentsUser = 1;
    var fullPath = $.trim($("#urlPublishComments").val());
    var sendNoti = "NO";

    if ($('#checkNotifyComments').is(":checked")){
      sendNoti = "SI";
    }

    var dataString = {
      "key": "w8KRCHuIf78yPWeuJQht0yPzxSAi2ZZhLmP",
      "_token": $.trim($("input[name=_token]").val()),
      "idOT": $.trim($("#idOT").val()),
      "commentsUser": $.trim($("#commentsByUser").val()),
      "sendNoti": sendNoti
    };

    $.ajax({
        type: "POST",
        url: fullPath,
        data: dataString,
        datatype: 'json',
        cache: false,

        success: function(data){
          swRequestCommentsUser = 0;
          if (data.status == "publish"){
            $("#commentsByUser").val("");
            window.location.reload();
          }
        },
        error: function(e)
        {
          swRequestCommentsUser = 0;
        }
    });
  }
}

function setTrackerOT(mod){
  
  if (swRequestTracker == 0){
    swRequestTracker = 1;
    var fullPath = $.trim($("#urlAjaxTracker").val());

    var dataString = {
      "key": "w8KRCHuIf78yPWeuJQht0yPzxSAi2ZZhLmP",
      "msj": $.trim($("#textTaskHere").val()),
      "_token": $.trim($("input[name=_token]").val()),
      "idOT": $.trim($("#idOtAdjust").val())
    };

    $.ajax({
        type: "POST",
        url: fullPath,
        data: dataString,
        datatype: 'json',
        cache: false,

        success: function(data){
          swRequestTracker = 0;
          window.location.reload();
          if (data.status == "success"){
          }
        },
        error: function(e)
        {
          swRequestTracker = 0;
        }
    });
  }
}

function setBigMail(sw){
  if (swRequestBigmail == 0){
    swRequestBigmail = 1;
    var fullPath = $.trim($("#urlAjaxBigmail").val());

    $("#isBigMail").prop( "disabled", "true");
    $(".switch").addClass("disable");
    var swBig = 0;
    if (sw == false)
      swBig = 0;
    else
      swBig = 1;

    var dataString = {
      "key": "w8KRCHuIf78yPWeuJQht0yPzxSAi2ZZhLmP",
      "_token": $.trim($("input[name=_token]").val()),
      "idOT": $.trim($("#idOtAdjust").val()),
      "swBigMail": swBig
    };

    $.ajax({
        type: "POST",
        url: fullPath,
        data: dataString,
        datatype: 'json',
        cache: false,

        success: function(data){
          swRequestBigmail = 0;
          if (data.status == "success"){
            $("#isBigMail").prop( "disabled", "false");
            $(".switch").removeClass("disable");
            window.location.reload();
          }
        },
        error: function(e)
        {
          swRequestBigmail = 0;
        }
    });
  }
}

function loadMask(mask, content){
  $(mask).css({
    "display": "block",
    "opacity": "0"
  });

  $(content).css({
    "display": "none"
  });

  $(mask).animate({
    "opacity": "1"
  },700, function(evt){
    $(content).css({
      "display": "block"
    });

    $(content).addClass('animated bounceInDown');
  });
}

function iniProgressBar(){
  var d = JSON.parse($.trim($("#mainDataStatusOT").val()));
  var h = "";
  var totS = 0;

  for (var i=0; i<d.length; i++){
    totS += parseInt(d[i].total);
  }

  for (var i=0; i<d.length; i++){
    var p = (d[i].total*100)/totS;
    var t = d[i].status;

    h += '<div class="progress-bar progress-bar-'+ d[i].status +'" style="width: '+ p +'%">'+
      '<span class="sr-only">35% Complete (success)</span>'+
    '</div>';
  }

  $(".progressProjects").html(h);
  $(".progressProjects").animate({
    "width":"100%"
  }, 1700);
}

function loadChart(){
  var d = JSON.parse($.trim($("#mainDataStatusOT").val()));
  var l = [];
  var v = [];
  var c = [];

  var totS = 0;
  var avaS = 0;

  for (var i=0; i<d.length; i++){
    totS += parseInt(d[i].total);
    if (d[i].status  == "completa" || d[i].status  == "cancelada"){
      avaS += parseInt(d[i].total);
    }
  }

  var percentage = (avaS*100)/totS;
  percentage = Math.round(percentage);

  if (isNaN(percentage)) percentage = 0;

  $(".percentage").html(percentage + '<span>%</span>');

  for (var i=0; i<d.length; i++){
    if (d[i].status == "nueva"){
      c.push("#e2e2e2");
    }else if (d[i].status == "asignada"){
      c.push("#77d1f3");
    }else if (d[i].status == "completa"){
      c.push("#fbb03b");
    }else if (d[i].status == "ajustes"){
      c.push("#B6BD4B");
    }else if (d[i].status == "cancelada"){
      c.push("#d3145a");
    }
    l.push(d[i].status);
    v.push(d[i].total);
  }

  var ctx = $("#myChart");
  var data = {
    labels: l,
    datasets: [
      {
        data: v,
        backgroundColor: c,
        borderWidth: 0
      }
    ]
  };

  var options = {
    legend: {
      display: false
    },
    tooltips: {
      enabled: true,
      callbacks: {
          label: function(tooltipItem, data) {
            //get the concerned dataset
            var dataset = data.datasets[tooltipItem.datasetIndex];
            //calculate the total of this data set
            var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
              return previousValue + currentValue;
            });
            //get the current items value
            var currentValue = dataset.data[tooltipItem.index];
            //calculate the precentage based on the total and current item, also this does a rough rounding to give a whole number
            var precentage = Math.floor(((currentValue/total) * 100)+0.5);

            return precentage + "%";
          }
        }
    }
  };

  var myPieChart = new Chart(ctx,{
      type: 'doughnut',
      data: data,
      options: options
  });
}

function loadMetadataInfo(){
  var data = JSON.parse($.trim($("#metaDataOT").val()));
  if (data.length > 0){
    var cadHtml = '<div class="row">';
    for (var i=0; i<data.length; i++){
      cadHtml += '<div class="col-md-6">' + 
                  '<div class="form-group">' + 
                    '<div class="input-group">' + 
                      '<div class="disableAddon">'+ data[i].name +'</div>' + 
                        '<label class="form-control labelDisable">'+ data[i].value +'</label>' + 
                    '</div>' + 
                  '</div>' + 
                '</div>';
    }
    cadHtml += '</div>';
    $(".displayMetadata").html(cadHtml);
  }else{
    $("#renderMetaData").css("display", "none");
  }  
}

function renderFieldsView(){
  if (validateSerialize()){
    var nameField = $.trim($("#nameFiledDinamic").val());
    var posField = $.trim($("#positionFiledDinamic").val());
    var typeField = $.trim($("#typeFieldDinamic").val());
    var valuesField = [];

    if (typeField == "select" || typeField == "select_mult" || typeField == "checkbox" || typeField == "radio_button"){
      for (var i=0; i<contFields; i++){
        var value = {
          "value": $.trim($("#valDin_" + (i+1)).val()),
          "text": $.trim($("#texDin_" + (i+1)).val())
        }
        valuesField.push(value);
      }
    }

    var newField = {
      "name": nameField,
      "type": typeField,
      "position": posField,
      "values": valuesField
    }

    objSerField.push(newField);
    showSummaryFields();
  }
}

function showSummaryFields(){
  contFields = 0;
  var totF = objSerField.length;
  var cadHtml = "";
  for (var i=0; i<totF; i++){
    objBase = objSerField[i];
    // console.log(objBase);
    cadHtml += '<div class="label label-default">'+ objBase.name +' <i class="fa fa-trash deletePosObj" data-indice="'+ i +'" aria-hidden="true"></i></div>';
  }

  $(".contentTypeProjects .closeButton").click();
  $(".summaryFields").html(cadHtml);
  $(".deletePosObj").on("click", function(evt){
    var ind = $(this).data("indice");
    removePosObj(ind);
  });
  preloadInfoFormOT();
}

function removePosObj(ind){
  delete objSerField[ind];
  objSerField.clean(undefined);
  showSummaryFields();
  preloadInfoFormOT();
}

Array.prototype.clean = function(deleteValue) {
  for (var i = 0; i < this.length; i++) {
    if (this[i] == deleteValue) {         
      this.splice(i, 1);
      i--;
    }
  }
  return this;
};

function validateSerialize(){
  var nameField = $.trim($("#nameFiledDinamic").val());
  var posField = $.trim($("#positionFiledDinamic").val());
  var typeField = $.trim($("#typeFieldDinamic").val());
  var swErr = 0;


  if (nameField == ""){
    $("#nameFiledDinamic").parent().addClass("has-error")
    swErr = 1;
  }else{
    $("#nameFiledDinamic").parent().removeClass("has-error")
  }

  if (posField == "" || !$.isNumeric( posField )){
    $("#positionFiledDinamic").parent().addClass("has-error")
    swErr = 1;
  }else{
    $("#positionFiledDinamic").parent().removeClass("has-error")
  }

  if (typeField == ""){
    $("#typeFieldDinamic").parent().addClass("has-error")
    swErr = 1;
  }else{
    $("#typeFieldDinamic").parent().removeClass("has-error")
  }
  if (typeField == "select" || typeField == "select_mult" || typeField == "checkbox" || typeField == "radio_button"){
    if (contFields == 0){
      swErr = 1;
      $(".tableValuesFields table").addClass("has-error");
    }else{
      $(".tableValuesFields table").removeClass("has-error");
    }
  }

  if (swErr == 1){
    return false;
  }else{
    return true;
  }
}

function loadOptions(op){
  if (op == "select" || op == "select_mult" || op == "checkbox" || op == "radio_button"){
    $(".valFields").css("display", "block");
    contFields = 0;
    var cadHtml = '<tr><th width="20">#</th><th>Valor</th><th>Texto</th></tr>';
    $(".tableValuesFields table").html(cadHtml);
  }else{
    $(".valFields").css("display", "none");
  }
}

function addField(){
  contFields++;
  var cadHtml = '<tr>' + 
                  '<td>'+ contFields +'</td>' + 
                  '<td><input type="text" id="valDin_'+ contFields +'" value="null"></td>' + 
                  '<td><input type="text" id="texDin_'+ contFields +'" value="--Valor---"></td>' + 
                '</tr>';
  $(".tableValuesFields table").append(cadHtml);
}
    
function loadMaskAddField(){
  $("#nameFiledDinamic").val("");
  $("#positionFiledDinamic").val("");
  $("#typeFieldDinamic").val("");
  $(".valFields").css("display", "none");

	$(".maskFields").css({
		"display": "block",
    "opacity": "0"
  });
  $(".contentTypeProjects").css({
    "display": "none"
  });


  $(".maskFields").animate({
    "opacity": "1"
  }, 700, function(evt){
    $(".contentTypeProjects").css({ "display": "block" });
    $(".contentTypeProjects").addClass('animated bounceInDown');
  });
}

function renderFilesHtml(result){
  var swB = $.trim($("#activeBigMail").val());
  if (swB == 1){
    globalFiles = result;
    iniBigMail();
  }
  
	var cadHTML = '<table class="table table-bordered" role="presentation"><tbody class="files">';

	for (var i=0; i<result.files.length; i++){
		var obj = result.files[i];
    // console.log(obj);
    if (obj.thumbnailUrl){
      var thumbImg = "/files/" + obj.thumbnailUrl.split("/public/files/")[1];
    }else{
      var thumbImg = "/img/folder.png";
    }

		var mainImg = "/files/" + obj.url.split("/public/files/")[1];

		cadHTML += '<tr class="template-download fade in">'+
						'<td width="30%">'+
                '<span class="preview">'+
                  '<a data-gallery="" download="'+ obj.name +'" title="1.png" href="'+ mainImg +'"><img src="'+ thumbImg +'" class="img-responsive center-block"></a>'+
                '</span>'+
              '</td>'+
              '<td width="30%">'+
                '<p class="name">'+
                  '<a data-gallery="" download="'+ obj.name +'" title="1.png" href="'+ mainImg +'">'+ obj.name +'</a>'+
                '</p>'+
              '</td>'+
              '<td>'+
                '<span class="size">'+ (parseInt(obj.size)/1000) +' KB</span>'+
              '</td>'+
              '<td width="20"><a href="#" data-url="'+ decodeURIComponent(mainImg) +'" class="deleteFileByOT" data-type="DELETE"><i class="material-icons">delete</i></a></td>'+
              '<td width="20"><a href="'+ mainImg +'" target="_blank"><i class="material-icons">file_download</i></a></td>'+
          '</tr>';
	}

	cadHTML += '</tbody></table>';

	$("#filesByOtUser").html(cadHTML);

  $(".deleteFileByOT").on("click", function(evt){
    evt.preventDefault();
    var obj = $(this).parent().parent();
    var type = $(this).data("type");
    var file = $(this).data("url");
    deleteFileByOT(obj, type, file);
  });
}

function deleteFileByOT(obj, type, file){

  if (swRequestDeleteFile == 0){
    swRequestDeleteFile = 1;
    var fullPath = $.trim($("#urlDeleteFile").val());

    var dataString = {
      "key": "w8KRCHuIf78yPWeuJQht0yPzxSAi2ZZhLmP",
      "_token": $.trim($("input[name=_token]").val()),
      "type": type,
      "file": file
    };

    $.ajax({
        type: "POST",
        url: fullPath,
        data: dataString,
        datatype: 'json',
        cache: false,

        success: function(data){
          swRequestDeleteFile = 0;
          if (data.status == "delete_success"){
            console.log(data);
            // $(obj).remove();
            $(obj).fadeOut();
            // window.location.reload();
          }
        },
        error: function(e)
        {
          swRequestDeleteFile = 0;
        }
    });
  }
}
function unZipFiles(url, path){
  if (swRequestUnzipFiles == 0){
    swRequestUnzipFiles = 1;

    var recPss = $.trim($("#recoveryPasswordField").val());
    var dataString = {
        "key": "w8KRCHuIf78yPWeuJQht0yPzxSAi2ZZhLmP",
        "_token": $.trim($("input[name=_token]").val()),
        "urlFile": url
    };

    $.ajax({
        type: "POST",
        url: path,
        data: dataString,
        datatype: 'json',
        cache: false,

        success: function(data){
          swRequestUnzipFiles = 0;
          console.log(data);
        },
        error: function(e)
        {
          swRequestUnzipFiles = 0;
        }
    });
  }
}

function loadDataFormFileds(key){
  if (swRequestFields == 0){
    swRequestFields = 1;
    var fullPath = $.trim($("#urlAjaxAuxField").val());
    // showLoader();

    var recPss = $.trim($("#recoveryPasswordField").val());
    var dataString = {
        "key": "w8KRCHuIf78yPWeuJQht0yPzxSAi2ZZhLmP",
        "_token": $.trim($("input[name=_token]").val()),
        "keyType": key
    };

    $.ajax({
        type: "POST",
        url: fullPath,
        data: dataString,
        datatype: 'json',
        cache: false,

        success: function(data){
          swRequestFields = 0;
          // hideLoader();
          if (data.message == "success"){
            if (data.dataFields.length == 0){
              globalCountFieldsVal = 0;
              mainSWMetadata = 1;
              $("#mainDisplayFormFields").css("display", "none");
              $("#renderMetaData").css("display", "block");
              $(".displayFormDinamic").html("");
            }else{
              mainSWMetadata = 0;
              renderViewForm(data.dataFields, key);
            }
          }
        },
        error: function(e)
        {
          // hideLoader();
          // alert(e);
          swRequestFields = 0;
        }
    });
  }
}

function getFieldDinamic(fBase){
  switch(fBase.type){
    case "text":
      field = '<input type="text" name="'+ fBase.name +'" class="form-control">';
      break;
    case "select":
      field = '<select name="'+ fBase.name +'" class="form-control">';
      var arrV = fBase.values.split(",");
      var arrT = fBase.texts.split(",");
      for (var j=0; j<arrV.length; j++){
        field += '<option value="'+ arrV[j]  +'">'+ arrT[j] +'</option>';
      }
      field += '</select>';
      break;
    case "select_mult":
      field = '<select name="'+ fBase.name +'" class="form-control" multiple>';
      var arrV = fBase.values.split(",");
      var arrT = fBase.texts.split(",");
      for (var j=0; j<arrV.length; j++){
        field += '<option value="'+ arrV[j]  +'">'+ arrT[j] +'</option>';
      }
      field += '</select>';
      break;
    case "textarea":
      field = '<textarea name="'+ fBase.name +'" class="form-control" rows="3"></textarea>';
      break;
    case "checkbox":
      field = '';
      var arrV = fBase.values.split(",");
      var arrT = fBase.texts.split(",");
      for (var j=0; j<arrV.length; j++){
        field += '<div class="checkbox">' +
          '<label>' +
            '<input type="checkbox" name="'+ fBase.name +'" value="'+ arrV[j]  +'">' + 
            arrT[j] +
          '</label>' +
        '</div>';
      }
      break;
    case "radio_button":
      field = '';
      var arrV = fBase.values.split(",");
      var arrT = fBase.texts.split(",");
      for (var j=0; j<arrV.length; j++){
        field += '<div class="radio">' +
          '<label>' +
            '<input type="radio" name="'+ fBase.name +'" value="'+ arrV[j]  +'">' + 
            arrT[j] +
          '</label>' +
        '</div>';
      }
      break;
  }

  return field;
}

function renderViewForm(dataView, key){
  var nFileds = dataView.length;
  globalCountFieldsVal = nFileds;
  var cadHtml = "";
  var contBreak = 0;

  for (var i=0; i<nFileds; i++){
    var fBase = dataView[i];
    var field = getFieldDinamic(fBase);
    contBreak++;
    cadHtml += '<div class="col-md-6">'+
                  '<div class="form-group">' + 
                    '<div class="nameFieldDinamic">'+ fBase.name +'</div>' + field +
                  '</div>' + 
                '</div>';
    if (contBreak >= 2){
      contBreak = 0;
      cadHtml += '<div class="clearfix"></div>';
    }
  }

  $(".displayFormDinamic").html(cadHtml);
  $("#mainDisplayFormFields .panel-heading").html($("#otType option:selected").text());
  $("#mainDisplayFormFields").css("display", "block");
}

function loadCheckPermitsCurrentProfile(){
	var baseData = $.trim($("#basePermitsCurrentProfile").val());
	baseData = JSON.parse(baseData);

	for (var i=0; i<baseData.length; i++){
		$(":checkbox[value="+ baseData[i].id_action +"]").prop("checked","true");
	}
}

function loadMainPermits(){
	var baseData = $.trim($("#baseMainPermits").val());
	baseData = JSON.parse(baseData);


	for (var i=0; i<baseData.length; i++){
		var objBase = baseData[i];
		// console.log(objBase);
		var objD = $("div[data-action='" + objBase.action + "']").filter('[data-funcionality="'+ objBase.funcionality +'"]');
		objD.html('<label><input class="optionsPermit" type="checkbox" value="'+ objBase.id +'" name="optionsPermit[]" ></label>');
	}

	

	$("#btnSaveProfile").on("click", function(evt){
		evt.preventDefault();
		var cboxes = document.getElementsByName('optionsPermit[]');
	    var len = cboxes.length;
	    var perm = new Array();
	    for (var i=0; i<len; i++) {
	    	if (cboxes[i].checked){
	    		perm.push(cboxes[i].value);
	    	}
	    }
	    activateSaveForm(perm)
	});
}

function activateSaveForm(idPermits){
	$("#permsSelect").val(idPermits);
	$("#saveFormProfile").submit();
}

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

function updateMask(){
    var maskHeight = $(window).height();
    var maskWidth = $(window).width();
}