/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */

var swRequestFiles = 0;
$(document).ready(function(evt){
  // iniUploadDoc();
  iniNewFiles();

  $("#formUploadFile").submit(function(evt){
    var myFile = $('#file').prop('files');
    if (myFile.length <=0){
      evt.preventDefault();
    }
  })
});

function iniNewFiles(){
  if (swRequestFiles == 0){
      swRequestFiles = 1;

      var dataString = {
        idUser: $("#idUser").val(),
        idProject: $("#idProject").val(),
        idOt: $("#idOT").val()
      };

      $.ajax({
        type: "get",
        url: '/projects/loadfilesbyot/'+$("#idUser").val()+'/'+$("#idProject").val()+'/'+$("#idOT").val(),
        data: dataString,
        dataType: "json",
        cache: false,

        success: function(data){
          swRequestFiles = 0;
          console.log(data);
          loadDatafiles(data);
        }
      });
    }
}

function loadDatafiles(data){
  var lastHtml = '';
  var html = '<div class="table-responsive tableFiles">' +
                  '<table class="table">' +
                    '<tr>' +
                      '<th>Nombre</th>' +
                      '<th>Fecha</th>' +
                      '<th>Subido por</th>' +
                      '<th width="70"></th>' +
                    '</tr>';
  
  if (data.files.length>0){
    for (var i=0; i<data.files.length; i++){
      var objBase = data.files[i];
      html += '<tr><td>'+ objBase.title +'</td><td>'+ objBase.date +'</td><td>'+ objBase.user +'</td><td><i class="fa fa-trash-o" aria-hidden="true"></i><i class="fa fa-download" aria-hidden="true"></i></td></tr>';
    }

    html += '</table></div>';

    lastHtml+= '<div class="tableZone"><table class="table">' +
                    '<tr>' +
                      '<td><img src="/img/pic.png" class="img-responsive center-block"></td>' +
                      '<td><h1><strong>ÚLTIMO</strong> ARCHIVO</h1><div class="nameFile"><strong>'+ data.files[0].title +'</strong></div><strong>Fecha:</strong> '+ data.files[0].date +'</td>' +
                      '<td><strong>SUBIDO POR:</strong> '+ data.files[0].user +'</td>' +
                    '</tr></table></div><div class="btnDownloadFile"><i class="fa fa-download" aria-hidden="true"></i></div>';

    $(".displayDataform").html(html);
    $(".lastDoc").html(lastHtml);
    

  }
}

function iniUploadDoc(){
  var idUser = $("#idUser").val();
  var idProject = $("#idProject").val();
  var idOT = $("#idOT").val();

  // Initialize the jQuery File Upload widget:
  $('#fileupload').fileupload({
      // Uncomment the following to send cross-domain cookies:
      //xhrFields: {withCredentials: true},
      url: '/projects/loadfilesbyot/'+idUser+'/'+idProject+'/'+idOT,
      formData: {
          _token: $('input[name=_token]').val(),
      }
  });

  // Enable iframe cross-domain access via redirect option:
  $('#fileupload').fileupload(
      'option',
      'redirect',
      window.location.href.replace(
          /\/[^\/]*$/,
          '/cors/result.html?%s'
      )
  );

  if (window.location.hostname === 'blueimp.github.io') {
      
  } else {
      // Load existing files:
      $('#fileupload').addClass('fileupload-processing');
      $.ajax({
          // Uncomment the following to send cross-domain cookies:
          //xhrFields: {withCredentials: true},
          url: $('#fileupload').fileupload('option', 'url'),
          dataType: 'json',
          context: $('#fileupload')[0]
      }).always(function () {
          $(this).removeClass('fileupload-processing');
      }).done(function (result) {
          renderFilesHtml(result);
          // $(this).fileupload('option', 'done').call(this, $.Event('done'), {result: result});
      });
  }
}

$(function () { 'use strict'; });
