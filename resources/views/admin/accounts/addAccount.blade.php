@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2 mainLeftZone">
            @include('admin.partials.menuAdminLeft')
        </div>
        <div class="col-md-10 mainRightZone">
            <div class="textWelcome">
                NUEVA <strong>CUENTA</strong>
            </div>

            <form action="{{ route('saveaccount') }}" class="" method="POST" id="saveFormAccount">
                {!! csrf_field() !!}
                <br>
                
                @include('admin.partials.messages')

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputAmount">Nombre:</label>
                            <div class="input-group {{ $errors->has('nameAccount') ? ' has-error' : '' }}">
                              <div class="input-group-addon">Nombre</div>
                              <input type="text" name="nameAccount" class="form-control" id="nameAccount" placeholder="" value="{{ old('nameAccount') }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('stateAccount') ? ' has-error' : '' }}">
                            <label class="sr-only" for="exampleInputAmount">Estado:</label>
                            <div class="input-group">
                              <div class="input-group-addon">Estado:</div>
                              <select class="form-control" name="stateAccount" id="stateAccount"> 
                                <option value="">Seleccionar</option>
                                <option value="active">Activo</option>
                                <option value="inactive">Inactivo</option>
                              </select>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-6 col-md-offset-6">
                        <div class="form-group {{ $errors->has('idClient') ? ' has-error' : '' }}">
                            <label class="sr-only" for="exampleInputAmount">Cliente:</label>
                            <div class="input-group">
                              <div class="input-group-addon">Cliente:</div>
                              <select class="form-control" name="idClient" id="idClient"> 
                                <option value="">Seleccionar</option>
                                @foreach ($clients as $client)
                                    <option value="{{ $client->id }}">{{ $client->name }}</option>
                                @endforeach
                              </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <br><div class="lineSeparator"></div><br>
                        <div class="row">
                            <div class="col-md-3 col-md-offset-6">
                                <a class="btn btn-default btnSite" href="{{ route('loadaccounts') }}" role="button">CANCELAR <i class="fa fa-times"></i></a>
                            </div>
                            <div class="col-md-3">
                                <a class="btn btn-default btnSite" href="#" role="button" id="btnSaveAccount">GUARDAR <i class="fa fa-check"></i></a>
                            </div>
                            <br><br><br>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
