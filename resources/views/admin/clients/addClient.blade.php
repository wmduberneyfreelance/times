@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2 mainLeftZone">
            @include('admin.partials.menuAdminLeft')
        </div>
        <div class="col-md-10 mainRightZone">
            <div class="textWelcome">
                NUEVO <strong>CLIENTE</strong>
            </div>

            <form action="{{ route('saveclient') }}" class="" method="POST" id="saveFormClient">
                {!! csrf_field() !!}
                <br>
                
                @include('admin.partials.messages')

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputAmount">Nombre:</label>
                            <div class="input-group {{ $errors->has('nameClient') ? ' has-error' : '' }}">
                              <div class="input-group-addon">Nombre</div>
                              <input type="text" name="nameClient" class="form-control" id="nameClient" placeholder="" value="{{ old('nameClient') }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('stateClient') ? ' has-error' : '' }}">
                            <label class="sr-only" for="exampleInputAmount">Estado:</label>
                            <div class="input-group">
                              <div class="input-group-addon">Estado:</div>
                              <select class="form-control" name="stateClient" id="stateClient"> 
                                <option value="">Seleccionar</option>
                                <option value="active">Activo</option>
                                <option value="inactive">Inactivo</option>
                              </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <br><div class="lineSeparator"></div><br>
                        <div class="row">
                            <div class="col-md-3 col-md-offset-6">
                                <a class="btn btn-default btnSite" href="{{ route('loadclients') }}" role="button">CANCELAR <i class="fa fa-times"></i></a>
                            </div>
                            <div class="col-md-3">
                                <a class="btn btn-default btnSite" href="#" role="button" id="btnSaveClient">GUARDAR <i class="fa fa-check"></i></a>
                            </div>
                            <br><br><br>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
