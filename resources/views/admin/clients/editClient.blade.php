@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2 mainLeftZone">
            @include('admin.partials.menuAdminLeft')
        </div>
        <div class="col-md-10 mainRightZone">
            <div class="textWelcome">
                EDITAR <strong>CLIENTE</strong>
            </div>

            <form action="{{ route('editclient', $idClientUpdate) }}" class="" method="POST" id="editFormClient">
                {!! csrf_field() !!}
                <br>
                
                @include('admin.partials.messages')

                <br>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputAmount">Nombre:</label>
                            <div class="input-group {{ $errors->has('nameClient') ? ' has-error' : '' }}">
                              <div class="input-group-addon">Nombre</div>
                              <input type="text" name="nameClient" class="form-control" id="nameClient" placeholder="" value="{{ $client->name }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('stateClient') ? ' has-error' : '' }}">
                            <label class="sr-only" for="exampleInputAmount">Estado:</label>
                            <div class="input-group">
                              <div class="input-group-addon">Estado:</div>
                              <select class="form-control" name="stateClient" id="stateClient">
                                <option value="">Seleccionar</option>
                                @if ($client->status === "active")
                                    <option value="active" selected="selected">Activo</option>
                                    <option value="inactive">Inactivo</option>
                                @elseif ($client->status === "inactive")
                                    <option value="active">Activo</option>
                                    <option value="inactive" selected="selected">Inactivo</option>
                                @endif
                              </select>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <br><div class="lineSeparator"></div><br>
                        <div class="row">
                            <div class="col-md-3 col-md-offset-3">
                                <a class="btn btn-default btnSite" href="{{ route('loadusers') }}" role="button">CANCELAR <i class="fa fa-times"></i></a>
                            </div>
                            <div class="col-md-3">
                                <a class="btn btn-default btnSiteDanger" href="{{ route('destroyclient', $idClientUpdate) }}" role="button" id="btnDeleteProfile">ELIMINAR <i class="fa fa-trash-o"></i></a>
                            </div>
                            <div class="col-md-3">
                                <a class="btn btn-default btnSite" href="#" role="button" id="btnUpdateClient">ACTUALIZAR <i class="fa fa-refresh"></i></a>
                            </div>
                            <br><br><br>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
