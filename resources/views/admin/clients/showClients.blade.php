@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2 mainLeftZone">
            @include('admin.partials.menuAdminLeft')
        </div>
        <div class="col-md-10 mainRightZone">
            <div class="textWelcome">
                CLIENTES

                
                <div class="pull-right">
                    <a class="btn btn-default btnSite" href="{{ route('newclient') }}" role="button">NUEVO <i class="fa fa-plus"></i></a>
                </div>
                <div class="pull-right"><div style="width:10px; height:3px;"></div></div>
                <div class="pull-right">
                    <a class="btn btn-default btnSite" href="{{ route('newprofile') }}" role="button">CARGA MASIVA <i class="fa fa-upload"></i></a>
                </div>
            </div>

            @if (Session::has('message'))
                <p class="alert alert-success">
                    {{ Session::get('message') }}
                </p>

            @endif
            

            <div class="row">
                <div class="col-md-12">
                    <br>
                    <form class="form-inline pull-right" action="{{ route('loadclients') }}" method="GET" role="search">
                        <div class="form-group">
                          <label for="exampleInputName2">Buscar cliente</label>
                          <input type="text" class="form-control" id="searchNameClient" name="searchNameClient" placeholder="Nombre" value="{{ Request::get('searchNameClient') }}">
                        </div>
                        <button type="submit" class="btn btn-default btnSite2"><i class="fa fa-search"></i></button>
                    </form>
                </div>
            </div>
            <form class="">
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>Nombre</th>
                                    <th>Estado</th>
                                    <th width="90"></th>
                                    <th width="80"></th>
                                </tr>
                                 @foreach ($clients as $client)
                                    <tr data-id="{{ $client->id }}">
                                        <td>{{ $client->name }}</td>
                                        <td>
                                            @if ($client->status === "active")
                                                <div class="profileActive"></div> Activo
                                            @elseif ($client->status === "inactive")
                                                <div class="profileInactive"></div> Inactivo
                                            @endif
                                        </td>
                                        <td>
                                            <a href="#" class="linkSite btnDelete">
                                                Eliminar <i class="fa fa-trash-o"></i>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route('showclient', $client->id) }}" class="linkSite">
                                                Editar <i class="fa fa-pencil"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <div class="pagination">
                            {{ $clients->appends(Request::all())->render() }}
                        </div>
                    </div>
                </div>
            </form>
            <form action="{{ route('destroyprofile', ':USER_ID') }}" class="" method="POST" id="deleteFormProfile">
                {!! csrf_field() !!}
            </form>
        </div>
    </div>
</div>
@endsection
