@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2 mainLeftZone">
            @include('admin.partials.menuAdminLeft')
        </div>
        <div class="col-md-10 mainRightZone">
            <div class="textWelcome">
                EDITAR <strong>PERFIL</strong>
            </div>

            <form action="{{ route('editprofile', $idProfileUpdate) }}" class="" method="POST" id="editFormProfile">
                {!! csrf_field() !!}
                <br>
                
                @include('admin.partials.messages')

                <br>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputAmount">Perfil:</label>
                            <div class="input-group {{ $errors->has('nameProfile') ? ' has-error' : '' }}">
                              <div class="input-group-addon">Perfil</div>
                              <input type="text" name="nameProfile" class="form-control" id="nameProfile" placeholder="" value="{{ $profile->name }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('stateProfile') ? ' has-error' : '' }}">
                            <label class="sr-only" for="exampleInputAmount">Estado:</label>
                            <div class="input-group">
                              <div class="input-group-addon">Estado:</div>
                              <select class="form-control" name="stateProfile" id="stateProfile">
                                <option value="">Seleccionar</option>
                                @if ($profile->status === "active")
                                    <option value="active" selected="selected">Activo</option>
                                    <option value="inactive">Inactivo</option>
                                @elseif ($profile->status === "inactive")
                                    <option value="active">Activo</option>
                                    <option value="inactive" selected="selected">Inactivo</option>
                                @endif
                              </select>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 {{ $errors->has('descriptionProfile') ? ' has-error' : '' }}">
                        <textarea class="form-control" rows="3" placeholder="Descripción" name="descriptionProfile" id="descriptionProfile">{{ $profile->description }}</textarea>
                        <br><div class="lineSeparator"></div><br>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="textWelcome">
                            PERMISOS
                        </div>
                        <input type="hidden" id="baseMainPermits" value="{{ $permits }}">
                        <input type="hidden" id="basePermitsCurrentProfile" value="{{ $actionsProfile }}">
                        <input type="hidden" name="permsSelect" id="permsSelect">
                        
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>Funcionalidad</th>
                                    @foreach ($actions as $action)
                                        <th>{{ $action->action }}</th>
                                    @endforeach
                                </tr>
                                @foreach ($funcionalities as $funcionality)
                                    <tr>
                                        <td>{{ $funcionality->funcionality }}</td>
                                        @foreach ($actions as $action)
                                            <td>
                                                <div data-funcionality="{{ $funcionality->funcionality }}" data-action="{{ $action->action }}">
                                                </div>
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <br><div class="lineSeparator"></div><br>
                        <div class="row">
                            <div class="col-md-3 col-md-offset-3">
                                <a class="btn btn-default btnSite" href="{{ route('admin') }}" role="button">CANCELAR <i class="fa fa-times"></i></a>
                            </div>
                            <div class="col-md-3">
                                <a class="btn btn-default btnSiteDanger" href="{{ route('destroyprofile', $idProfileUpdate) }}" role="button" id="btnDeleteProfile">ELIMINAR <i class="fa fa-trash-o"></i></a>
                            </div>
                            <div class="col-md-3">
                                <a class="btn btn-default btnSite" href="#" role="button" id="btnUpdateProfile">ACTUALIZAR <i class="fa fa-refresh"></i></a>
                            </div>
                            <br><br><br>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript">
        loadMainPermits();
        loadCheckPermitsCurrentProfile();
    </script>
</div>
@endsection
