@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-2 mainLeftZone">
        @include('admin.partials.menuAdminLeft')
      </div>
      <div class="col-md-10 mainRightZone">
        <div class="textWelcome">
          EDITAR <strong>USUARIO</strong>
        </div>
        <form action="{{ route('edituser', $idUserUpdate) }}" class="" method="POST" id="editFormUser">
          {!! csrf_field() !!}
          <br>
          
          @include('admin.partials.messages')

          <br>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="sr-only" for="exampleInputAmount">Nombre:</label>
                <div class="input-group {{ $errors->has('nameUser') ? ' has-error' : '' }}">
                  <div class="input-group-addon">Nombre</div>
                  <input type="text" name="nameUser" class="form-control" id="nameUser" placeholder="" value="{{ $user->name }}">
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group {{ $errors->has('stateUser') ? ' has-error' : '' }}">
                <label class="sr-only" for="exampleInputAmount">Estado:</label>
                <div class="input-group">
                  <div class="input-group-addon">Estado:</div>
                  <select class="form-control" name="stateUser" id="stateUser">
                    <option value="">Seleccionar</option>
                    @if ($user->status === "active")
                      <option value="active" selected="selected">Activo</option>
                      <option value="inactive">Inactivo</option>
                    @elseif ($user->status === "inactive")
                      <option value="active">Activo</option>
                      <option value="inactive" selected="selected">Inactivo</option>
                    @endif
                  </select>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
              <div class="form-group">
                <div class="input-group {{ $errors->has('positionUser') ? ' has-error' : '' }}">
                  <div class="input-group-addon">Cargo:</div>
                  <input type="text" name="positionUser" class="form-control" id="positionUser" placeholder="" value="{{ $user->position }}">
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <div class="input-group {{ $errors->has('genderUser') ? ' has-error' : '' }}">
                  <div class="input-group-addon">Genero:</div>
                  <select class="form-control" name="genderUser" id="genderUser">
                    <option value="">Seleccionar</option>
                    @if ($user->gender === "masculine")
                      <option value="masculine" selected="selected">Masculino</option>
                      <option value="female">Femenino</option>
                    @elseif ($user->gender === "female")
                      <option value="masculine">Masculino</option>
                      <option value="female" selected="selected">Femenino</option>
                    @else
                      <option value="masculine">Masculino</option>
                      <option value="female">Femenino</option>
                    @endif
                  </select>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
              <div class="form-group">
                <div class="input-group {{ $errors->has('emailUser') ? ' has-error' : '' }}">
                  <div class="input-group-addon">Email:</div>
                  <input type="text" name="emailUser" class="form-control" id="emailUser" placeholder="" value="{{ $user->email }}">
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group {{ $errors->has('profileUser') ? ' has-error' : '' }}">
                <label class="sr-only" for="exampleInputAmount">Perfil:</label>
                <div class="input-group">
                  <div class="input-group-addon">Perfil:</div>
                  <select class="form-control" name="profileUser" id="profileUser"> 
                    <option value="">Seleccionar</option>
                    <option value="superadmin">Super administrador</option>
                    <option value="admin">Administrador</option>
                    <option value="moderator">Director</option>
                    <option value="executive">Ejecutivo</option>
                    <option value="developer">Desarrollador</option>
                    <option value="creative">Creativo</option>
                    <option value="user">Usuario general</option>
                    {{-- @foreach ($profiles as $profile)
                    profile
                      @if ($->id === $user->id_profile)
                        <option value="{{ $profile->id }}"  selected="selected">{{ $profile->name }}</option>
                      @else
                        <option value="{{ $profile->id }}">{{ $profile->name }}</option>
                      @endif
                    @endforeach --}}

                  </select>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="sr-only" for="exampleInputAmount">Contraseña:</label>
                <div class="input-group {{ $errors->has('passwordUser') ? ' has-error' : '' }}">
                  <div class="input-group-addon">Contraseña</div>
                  <input type="password" name="passwordUser" class="form-control" id="passwordUser" placeholder="" value="{{ old('passwordUser') }}">
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
              <br><div class="lineSeparator"></div><br>
              <div class="row">
                <div class="col-md-3 col-md-offset-3">
                  <a class="btn btn-default btnSite" href="{{ route('loadusers') }}" role="button">CANCELAR <i class="fa fa-times"></i></a>
                </div>
                <div class="col-md-3">
                  <a class="btn btn-default btnSiteDanger" href="{{ route('destroyuser', $idUserUpdate) }}" role="button" id="btnDeleteProfile">ELIMINAR <i class="fa fa-trash-o"></i></a>
                </div>
                <div class="col-md-3">
                  <a class="btn btn-default btnSite" href="#" role="button" id="btnUpdateUser">ACTUALIZAR <i class="fa fa-refresh"></i></a>
                </div>
                <br><br><br>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
