@extends('layouts.app')

@section('content')

<div class="maskFields">
  <div class="contentTypeProjects">
    <div class="closeButton"><i class="fa fa-times-circle" aria-hidden="true"></i></div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="textWelcome">
            AGREGAR NUEVO <strong>CAMPO</strong>
          </div>
          <br><br>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">Nombre del campo</div>
              <input type="text" class="form-control" id="nameFiledDinamic">
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-4">
          <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">Posición</div>
              <input type="number" min="1" max="50" class="form-control" id="positionFiledDinamic">
            </div>
          </div>
        </div>
        <div class="col-md-8">
          <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">Tipo de campo:</div>
              <select class="form-control" name="" id="typeFieldDinamic"> 
                <option value="">Seleccionar</option>
                <option value="text">Campo de texto</option>
                <option value="select">Lista desplegable selección única</option>
                <option value="select_mult">Lista desplegable selección múltiple</option>
                <option value="textarea">Area de texto</option>
                <option value="checkbox">Checkbox</option>
                <option value="radio_button">Botón de radio</option>
              </select>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12">
          <div class="valFields">
            <p>
              <i class="fa fa-th-list" aria-hidden="true"></i> Opciones
            </p>
            <div class="table-responsive tableValuesFields">
              <table class="table table-bordered">
                <tr>
                  <th width="20">#</th>
                  <th>Valor</th>
                  <th>Texto</th>
                </tr>
              </table>
              <a href="#" id="linkAddValueOptions"><i class="fa fa-plus" aria-hidden="true"></i> Agregar <i class="fa fa-level-up" aria-hidden="true"></i></a>
            </div>
          </div>
        </div>

        <div class="clearfix"></div>
        <div class="col-md-12">
          <br><div class="lineSeparator"></div><br>
            <div class="row">
             <div class="col-md-3 col-md-offset-6">
               <a class="btn btn-default btnSite closeMask" href="#" role="button">CANCELAR <i class="fa fa-times"></i></a>
             </div>
             <div class="col-md-3">
               <a class="btn btn-default btnSite" href="#" id="buttonSerializeFields" role="button">AGREGAR <i class="fa fa-check"></i></a>
             </div>
             <br><br><br>
           </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-2 mainLeftZone">
            @include('admin.partials.menuAdminLeft')
        </div>
        <div class="col-md-10 mainRightZone">
            <div class="textWelcome">
                NUEVO <strong>TIPO DE OT</strong>
            </div>

            <form action="{{ route('saveottype') }}" class="" method="POST" id="saveFormProjectType">
                {!! csrf_field() !!}
                <br>
                
                @include('admin.partials.messages')

                <input type="hidden" name="fieldsOtType" class="form-control" id="fieldsOtType" placeholder="" value="">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputAmount">Nombre:</label>
                            <div class="input-group {{ $errors->has('nameProType') ? ' has-error' : '' }}">
                              <div class="input-group-addon">Nombre</div>
                              <input type="text" name="nameProType" class="form-control" id="nameProType" placeholder="" value="{{ old('nameProType') }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group {{ $errors->has('stateProType') ? ' has-error' : '' }}">
                        <label class="sr-only" for="exampleInputAmount">Estado:</label>
                        <div class="input-group">
                          <div class="input-group-addon">Estado:</div>
                          <div class="select-style">
                            <select class="form-control" name="stateProType" id="stateProType"> 
                              <option value="">Seleccionar</option>
                              <option value="active">Activo</option>
                              <option value="inactive">Inactivo</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-6">
                      <div class="form-group {{ $errors->has('deparmentProType') ? ' has-error' : '' }}">
                        <div class="input-group">
                          <div class="input-group-addon">Area:</div>
                          <div class="select-style">
                            <select class="form-control" name="deparmentProType" id="deparmentProType"> 
                              <option value="">Seleccionar</option>
                              <option value="developer">Desarrollo</option>
                              <option value="creative">Creativos</option>
                              <option value="other">Otra</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group {{ $errors->has('emailDeparmentProType') ? ' has-error' : '' }}">
                              <div class="input-group-addon">Email area</div>
                              <input type="text" name="emailDeparmentProType" class="form-control" id="emailDeparmentProType" placeholder="" value="{{ old('emailDeparmentProType') }}">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 {{ $errors->has('descriptionProType') ? ' has-error' : '' }}">
	                    <textarea class="form-control" rows="3" placeholder="Descripción" name="descriptionProType" id="descriptionProType">{{ old('descriptionProType') }}</textarea>
	                    <br><div class="lineSeparator"></div><br>
	                </div>
                  <div class="clearfix"></div>
                  <div class="col-md-12">
                    <div class="pull-right buttonPreviewFields">
                      <i class="fa fa-free-code-camp" aria-hidden="true"></i>
                    </div>
                    <div class="subTitle">
                      Campos <strong>actuales</strong>
                    </div>
                    <div class="summaryFields">
                    </div>
                    <div class="mainPreviaFormTypeOT hidden">
                      <div class="subTitle">
                        PREVISUALIZACIÓN DE LOS <strong>CAMPOS</strong>
                      </div>
                      <div class="panel panel-primary">
                        <div class="panel-heading">Formulario</div>
                        <div class="panel-body">
                          <div class="prevFormRender"></div>
                        </div>
                      </div>
                    </div>
                    <div class="lineSeparator"></div>
                    <br><br>
                  </div>

                  
                  <div class="clearfix"></div>
                  <div class="col-sm-3">
                    <a class="btn btn-default btnSite" href="#" role="button" id="btnAddFieldTypeOT">
                      <i class="fa fa-plus" aria-hidden="true"></i> AGREGAR CAMPO
                    </a>
                  </div>
                 	<div class="clearfix"></div>
                  <div class="col-md-12">
                    <br><div class="lineSeparator"></div><br>
                    <div class="row">
                      <div class="col-md-3 col-md-offset-6">
                        <a class="btn btn-default btnSite" href="{{ route('loadtypesot') }}" role="button">CANCELAR <i class="fa fa-times"></i></a>
                      </div>
                      <div class="col-md-3">
                        <a class="btn btn-default btnSite" href="#" role="button" id="btnSaveProType">GUARDAR <i class="fa fa-check"></i></a>
                      </div>
                      <br><br><br>
                    </div>
                  </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
