@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-2 mainLeftZone">
          @include('admin.partials.menuAdminLeft')
      </div>
      <div class="col-md-10 mainRightZone">
        <div class="textWelcome">
        	EDITAR <strong>TIPO DE PROYECTO</strong>
        </div>

        <form action="{{ route('edittypeot', $idProjectTypeUpdate) }}" class="" method="POST" id="editFormProType">
          {!! csrf_field() !!}
          <br>
          @include('admin.partials.messages')
          <br>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="sr-only" for="exampleInputAmount">Nombre:</label>
                <div class="input-group {{ $errors->has('nameProType') ? ' has-error' : '' }}">
                  <div class="input-group-addon">Nombre</div>
                  <input type="text" name="nameProType" class="form-control" id="nameProType" placeholder="" value="{{ $projectType->name }}">
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group {{ $errors->has('stateProType') ? ' has-error' : '' }}">
                <label class="sr-only" for="exampleInputAmount">Estado:</label>
                <div class="input-group">
                  <div class="input-group-addon">Estado:</div>
                  <select class="form-control" name="stateProType" id="stateProType">
                    <option value="">Seleccionar</option>
                    @if ($projectType->status === "active")
                        <option value="active" selected="selected">Activo</option>
                        <option value="inactive">Inactivo</option>
                    @elseif ($projectType->status === "inactive")
                        <option value="active">Activo</option>
                        <option value="inactive" selected="selected">Inactivo</option>
                    @endif
                  </select>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
              <div class="form-group {{ $errors->has('deparmentProType') ? ' has-error' : '' }}">
                <div class="input-group">
                  <div class="input-group-addon">Area:</div>
                  <select class="form-control" name="deparmentProType" id="deparmentProType">
                    @if ($projectType->deparment === "developer")
                      <option value="">Seleccionar</option>
                      <option value="developer" selected="selected">Desarrollo</option>
                      <option value="creative">Creativos</option>
                      <option value="other">Otra</option>
                    @elseif ($projectType->deparment === "creative")
                      <option value="">Seleccionar</option>
                      <option value="developer">Desarrollo</option>
                      <option value="creative" selected="selected">Creativos</option>
                      <option value="other">Otra</option>
                    @elseif ($projectType->deparment === "other")
                      <option value="">Seleccionar</option>
                      <option value="developer">Desarrollo</option>
                      <option value="creative">Creativos</option>
                      <option value="other" selected="selected">Otra</option>
                    @else
                      <option value="" selected="selected">Seleccionar</option>
                      <option value="developer">Desarrollo</option>
                      <option value="creative">Creativos</option>
                      <option value="other">Otra</option>
                    @endif
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <div class="input-group {{ $errors->has('emailDeparmentProType') ? ' has-error' : '' }}">
                  <div class="input-group-addon">Email area:</div>
                  <input type="text" name="emailDeparmentProType" class="form-control" id="emailDeparmentProType" placeholder="" value="{{ $projectType->email }}">
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 {{ $errors->has('descriptionProType') ? ' has-error' : '' }}">
              <textarea class="form-control" rows="3" placeholder="Descripción" name="descriptionProType" id="descriptionProType">{{ $projectType->description }}</textarea>
              <br><div class="lineSeparator"></div><br>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
              <br><div class="lineSeparator"></div><br>
              <div class="row">
                <div class="col-md-3 col-md-offset-3">
                  <a class="btn btn-default btnSite" href="{{ route('loadtypesot') }}" role="button">CANCELAR <i class="fa fa-times"></i></a>
                </div>
                <div class="col-md-3">
                  <a class="btn btn-default btnSiteDanger" href="{{ route('destroytypeot', $idProjectTypeUpdate) }}" role="button" id="btnDeleteProfile">ELIMINAR <i class="fa fa-trash-o"></i></a>
                </div>
                <div class="col-md-3">
                  <a class="btn btn-default btnSite" href="#" role="button" id="btnUpdateProType">ACTUALIZAR <i class="fa fa-refresh"></i></a>
                </div>
                <br><br><br>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
