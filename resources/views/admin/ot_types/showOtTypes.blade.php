@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-2 mainLeftZone">
          @include('admin.partials.menuAdminLeft')
      </div>
      <div class="col-md-10 mainRightZone">
        <div class="textWelcome">
          TIPOS DE <strong>OT'S</strong>
          <div class="pull-right">
              <a class="btn btn-default btnSite" href="{{ route('newottype') }}" role="button">NUEVO <i class="fa fa-plus"></i></a>
          </div>
        </div>
        @if (Session::has('message'))
          <p class="alert alert-success">
            {{ Session::get('message') }}
          </p>
        @endif
        <div class="row">
          <div class="col-md-12">
            <br>
            <form class="form-inline pull-right" action="{{ route('loadtypesot') }}" method="GET" role="search">
              <div class="form-group">
                <label for="exampleInputName2">Buscar tipo de proyecto: </label>
                <input type="text" class="form-control" id="searchNameTypeP" name="searchNameTypeP" placeholder="Nombre" value="{{ Request::get('searchNameTypeP') }}">
              </div>
              <button type="submit" class="btn btn-default btnSite2"><i class="fa fa-search"></i></button>
            </form>
          </div>
        </div>
        <form class="">
          <br>
          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table table-hover">
                  <tr>
                    <th>Nombre</th>
                    <th>Area</th>
                    <th width="90">Estado</th>
                    <th width="80"></th>
                  </tr>
                   @foreach ($projectsTypes as $projectType)
                    <tr data-id="{{ $projectType->id }}">
                      <td>
                        <a href="{{ route('showottype', $projectType->id) }}" class="linkSite">
                          {{ $projectType->name }}
                        </a>
                      </td>
                      <td>
                        {{ $projectType->deparment }}
                      </td>
                      <td>
                        @if ($projectType->status === "active")
                          <div class="profileActive"></div> Activo
                        @elseif ($projectType->status === "inactive")
                          <div class="profileInactive"></div> Inactivo
                        @endif
                      </td>
                      <td>
                        <a href="{{ route('showottype', $projectType->id) }}" class="linkSite">
                          Editar <i class="fa fa-pencil"></i>
                        </a>
                      </td>
                    </tr>
                  @endforeach
                </table>
              </div>
              <div class="pagination">
                  {{ $projectsTypes->appends(Request::all())->render() }}
              </div>
            </div>
          </div>
        </form>
        <form action="{{ route('destroyprofile', ':USER_ID') }}" class="" method="POST" id="deleteFormProfile">
            {!! csrf_field() !!}
        </form>
      </div>
    </div>
</div>
@endsection
