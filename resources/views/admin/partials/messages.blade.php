<div class="row">
    <div class="col-md-12 mainCustomMessages">
        @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                Por favor válida la siguiente información:<br><br>

                <ul class="itemsErrors">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>


