@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2 mainLeftZone">
            @include('admin.partials.menuAdminLeft')
        </div>
        <div class="col-md-10 mainRightZone">
            <div class="textWelcome">
            	EDITAR <strong>TIPO DE CASO</strong>
            </div>

            <form action="{{ route('editpiecetype', $idPieceTypeUpdate) }}" class="" method="POST" id="editFormPieceType">
                {!! csrf_field() !!}
                <br>
                
                @include('admin.partials.messages')

                <br>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputAmount">Nombre:</label>
                            <div class="input-group {{ $errors->has('namePieceType') ? ' has-error' : '' }}">
                              <div class="input-group-addon">Nombre</div>
                              <input type="text" name="namePieceType" class="form-control" id="namePieceType" placeholder="" value="{{ $pieceType->name }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('statePieceType') ? ' has-error' : '' }}">
                            <label class="sr-only" for="exampleInputAmount">Estado:</label>
                            <div class="input-group">
                              <div class="input-group-addon">Estado:</div>
                              <select class="form-control" name="statePieceType" id="statePieceType">
                                <option value="">Seleccionar</option>
                                @if ($pieceType->status === "active")
                                    <option value="active" selected="selected">Activo</option>
                                    <option value="inactive">Inactivo</option>
                                @elseif ($pieceType->status === "inactive")
                                    <option value="active">Activo</option>
                                    <option value="inactive" selected="selected">Inactivo</option>
                                @endif
                              </select>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 {{ $errors->has('descriptionPieceType') ? ' has-error' : '' }}">
	                    <textarea class="form-control" rows="3" placeholder="Descripción" name="descriptionPieceType" id="descriptionPieceType">{{ $pieceType->description }}</textarea>
	                    <br><div class="lineSeparator"></div><br>
	                </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <br><div class="lineSeparator"></div><br>
                        <div class="row">
                            <div class="col-md-3 col-md-offset-3">
                                <a class="btn btn-default btnSite" href="{{ route('loadtypespieces') }}" role="button">CANCELAR <i class="fa fa-times"></i></a>
                            </div>
                            <div class="col-md-3">
                                <a class="btn btn-default btnSiteDanger" href="{{ route('destroypiecepeot', $idPieceTypeUpdate) }}" role="button" id="btnDeleteProfile">ELIMINAR <i class="fa fa-trash-o"></i></a>
                            </div>
                            <div class="col-md-3">
                                <a class="btn btn-default btnSite" href="#" role="button" id="btnUpdatePieceType">ACTUALIZAR <i class="fa fa-refresh"></i></a>
                            </div>
                            <br><br><br>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
