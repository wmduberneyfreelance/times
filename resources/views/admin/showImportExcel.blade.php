@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2 mainLeftZone">
            @include('admin.partials.menuAdminLeft')
        </div>
        <div class="col-md-10 mainRightZone">
            <div class="textWelcome">
                CARGAR <strong>Excel</strong>

                
                <div class="pull-right">
                    <a class="btn btn-default btnSite" href="{{ route('newuser') }}" role="button" id="btnSaveProfile">NUEVO <i class="fa fa-plus"></i></a>
                </div>
            </div>

            @if (Session::has('message'))
                <p class="alert alert-success">
                    {{ Session::get('message') }}
                </p>

            @endif
            

            <div class="row">
                <div class="col-md-12">
                    <br>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            @foreach ($dataExcel as $row)
                                <tr>
                                @foreach ($row as $subRow)
                                    <td>{{ $subRow }}</td>
                                @endforeach
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
            
            <form action="{{ route('destroyprofile', ':USER_ID') }}" class="" method="POST" id="deleteFormProfile">
                {!! csrf_field() !!}
            </form>
        </div>
    </div>
</div>
@endsection
