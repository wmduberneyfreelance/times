@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2 mainLeftZone">
            @include('admin.partials.menuAdminLeft')
        </div>
        <div class="col-md-10 mainRightZone">
            <div class="textWelcome">
                PERFILES DE <strong>USUARIOS</strong>

                <div class="pull-right">
                    <a class="btn btn-default btnSite" href="{{ route('newprofile') }}" role="button" id="btnSaveProfile">NUEVO <i class="fa fa-plus"></i></a>
                </div>
            </div>

            @if (Session::has('message'))
                <p class="alert alert-success">
                    {{ Session::get('message') }}
                </p>

            @endif
            

            <div class="row">
                <div class="col-md-12">
                    <br>
                    <form class="form-inline pull-right" action="{{ route('admin') }}" method="GET" role="search">
                        <div class="form-group">
                          <label for="exampleInputName2">Buscar perfil</label>
                          <input type="text" class="form-control" id="searchNameProfile" name="searchNameProfile" placeholder="Perfil" value="{{ Request::get('searchNameProfile') }}">
                        </div>
                        <button type="submit" class="btn btn-default btnSite2"><i class="fa fa-search"></i></button>
                    </form>
                </div>
            </div>
            <?php
                // echo "<pre>";
                // print_r($profiles);
                // echo "</pre>";
            ?>

            <form class="">
                <br>
                <div class="row">
                    <div class="col-md-12">

                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>Perfil</th>
                                    <th>Descripción</th>
                                    <th width="90">Estado</th>
                                    <th width="90"></th>
                                    <th width="80"></th>
                                </tr>

                                 @foreach ($profiles as $profile)
                                    <tr data-id="{{ $profile->id }}">
                                        <td>{{ $profile->name }}</td>
                                        <td>{{ $profile->description }}</td>
                                        <td>
                                            @if ($profile->status === "active")
                                                <div class="profileActive"></div> Activo
                                            @elseif ($profile->status === "inactive")
                                                <div class="profileInactive"></div> Inactivo
                                            @endif
                                        </td>
                                        <td>
                                            <a href="#" class="linkSite btnDelete">
                                                Eliminar <i class="fa fa-trash-o"></i>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route('showprofile', $profile->id) }}" class="linkSite">
                                                Editar <i class="fa fa-pencil"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <div class="pagination">
                            {{ $profiles->appends(Request::all())->render() }}
                        </div>
                    </div>
                </div>
            </form>
            <form action="{{ route('destroyprofile', ':USER_ID') }}" class="" method="POST" id="deleteFormProfile">
                {!! csrf_field() !!}
            </form>
        </div>
    </div>
</div>
@endsection
