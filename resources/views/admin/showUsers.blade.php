@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-2 mainLeftZone">
          @include('admin.partials.menuAdminLeft')
      </div>
      <div class="col-md-10 mainRightZone">
        <div class="textWelcome">
          USUARIOS
          <div class="pull-right">
            <a class="btn btn-default btnSite" href="{{ route('newuser') }}" role="button" id="btnSaveProfile">NUEVO <i class="fa fa-plus"></i></a>
          </div>
          <div class="pull-right"><div style="width:10px; height:3px;"></div></div>
          <div class="pull-right">
            <a class="btn btn-default btnSite" href="{{ route('newprofile') }}" role="button" id="btnSaveProfile">CARGA MASIVA <i class="fa fa-upload"></i></a>
          </div>
        </div>

        @if (Session::has('message'))
          <p class="alert alert-success">
            {{ Session::get('message') }}
          </p>
        @endif
        <div class="row">
          <div class="col-md-12">
            <br>
            <form class="form-inline pull-right" action="{{ route('loadusers') }}" method="GET" role="search">
              <div class="form-group">
                <input type="text" class="form-control" id="searchNameUser" name="searchNameUser" placeholder="Buscar usuario" value="{{ Request::get('searchNameUser') }}">
              </div>
              <button type="submit" class="btn btn-default btnSite2"><i class="fa fa-search"></i></button>
            </form>
          </div>
        </div>
        <form class="">
          <br>
          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table table-hover">
                  <tr>
                    <th>Nombre</th>
                    <th>Perfil</th>
                    <th>Estado</th>
                    <th width="80"></th>
                  </tr>
                   @foreach ($users as $user)
                    <tr data-id="{{ $user->id }}">
                      <td>
                        <a href="{{ route('showuser', $user->id) }}" class="linkSite">
                          {{ $user->name }}
                        </a>
                      </td>
                      <td>{{ $user->rol }}</td>
                      <td>
                        @if ($user->status === "active")
                          <div class="profileActive"></div> Activo
                        @elseif ($user->status === "inactive")
                          <div class="profileInactive"></div> Inactivo
                        @endif
                      </td>
                      <td>
                        <a href="{{ route('showuser', $user->id) }}" class="linkSite">
                          Editar <i class="fa fa-pencil"></i>
                        </a>
                      </td>
                    </tr>
                  @endforeach
                </table>
              </div>
              <div class="pagination">
                {{ $users->appends(Request::all())->render() }}
              </div>
            </div>
          </div>
        </form>
        <form action="{{ route('destroyprofile', ':USER_ID') }}" class="" method="POST" id="deleteFormProfile">
            {!! csrf_field() !!}
        </form>
      </div>
    </div>
  </div>
@endsection
