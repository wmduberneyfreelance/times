@extends('layouts.login')

@section('content')
  <div class="mainPatronLogin"></div>
  <div class="mainContentLogin">
    <div class="contentLogin">
      <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
        {!! csrf_field() !!}
        <br><br><img src="{{ asset("img/login/logo.png") }}" class="center-block img-responsive" id="bigotes"><br><br>


        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
          <div class="col-md-12">
            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Correo electrónico">

            @if ($errors->has('email'))
              <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
            @endif
          </div>
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
          <div class="col-md-12">
            <input type="password" class="form-control" name="password" placeholder="Cédula">

            @if ($errors->has('password'))
              <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
            @endif
          </div>
        </div>

        <div class="form-group">
          <div class="col-md-12">
            <button type="submit" class="btn btn-primary btnBigotes">
              ENTRAR
            </button>
          </div>
        </div>

        <div class="form-group">
          <div class="col-md-6">
            <div class="checkbox">
              <label class="whiteLink">
                <input type="checkbox" name="remember"> Recuerdame
              </label>
            </div>
          </div>
          <div class="col-md-6">
            <!-- <a class="btn btn-link whiteLink" href="{{ url('/password/reset') }}">Perdí mi contraseña</a> -->
          </div>
        </div>
      </form>
      <div class="pull-right">Powered by <a href="http://makesystems.com.co" target="_blank" style="color:#ffffff;">
        <img src="{{ asset("img/make.png") }}" width="100">
      </a></div>
      
    </div>
  </div>
@endsection
