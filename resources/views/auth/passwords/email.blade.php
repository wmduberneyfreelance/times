@extends('layouts.login')

<!-- Main Content -->
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="contentLogin">
                <br><br><img src="{{ asset("img/login/logo.png") }}" class="center-block img-responsive"><br><br>

                <p class="text-center">Ingresa tu correo electrónico para recuperar tu contraseña:</p>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                    {!! csrf_field() !!}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="col-md-12">
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Correo electrónico">

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btnBigotes">
                                RECUPERAR
                            </button>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6">
                            <a class="btn btn-link whiteLink" href="{{ url('/login') }}"><< Volver al ingreso</a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection
