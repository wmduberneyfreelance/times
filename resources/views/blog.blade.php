@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-2 mainLeftZone">
      @include('partials.leftZoneCalendar')
    </div>
    <div class="col-md-10 mainRightZone">
      <div class="textWelcome">
        <i class="fa fa-rss" aria-hidden="true"></i> FEEDBACK
      </div>
      <div class="row">
        <div class="col-md-6">
          <p>
            Trabajamos duro para mejorar la continuamente, dejanos tus comentarios de la plataforma y <strong>como podemos mejorar.</strong>
          </p>
          <img src="{{ asset("img/gifs/b3b963999a4514cc4e683bf5bdd1439a.gif") }}" class="img-responsive center-block">
        </div>
        <div class="col-md-6">
          DEJANOS TUS <strong>COMENTARIOS</strong>
          <form action="{{ route('savepost') }}" class="" method="POST" id="savePostform">
            {!! csrf_field() !!}

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <br>
                  <textarea class="form-control" rows="3" id="postMessage" name="postMessage"></textarea>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-md-4 col-md-offset-8">
                <div class="form-group">
                  <a class="btn btn-default btnSite" href="#" role="button" id="btnSavePostBlog">POSTEAR <i class="fa fa-check"></i></a>
                </div>
              </div>
            </div>
          </form>

        </div>
        <div class="clearfix"></div>
        <div class="col-md-12">
          <br><br>
          <div class="comments">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne" style="background-color: #4d4d4d; color: #fff;">
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_1">
                      Comentarios anteriores
                    </a>
                  </h4>
                </div>
                <div id="collapse_1" class="panel-collapse collapse in">
                  <div class="panel-body">
                    @foreach ($postsData as $post)
                      <div class="comment">
                        <h2>{{ $post->userName }}</h2>
                        <p>
                          {{ $post->post }}
                        </p>
                        <p class="date">
                          {{ $post->post_date }}
                        </p>
                      </div>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
