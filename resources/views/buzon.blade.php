@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-2 mainLeftZone">
      @include('partials.leftZoneCalendar')
    </div>
    <div class="col-md-10 mainRightZone">
      <div class="textWelcome">
        <i class="material-icons">chat</i> CHAT <strong>PRIVADO</strong>
      </div>
      <div class="row">
        <div class="container-fluid">
          <div class="col-sm-3 chat_sidebar">
            <div class="row">
              <div id="custom-search-input">
                <div class="input-group col-md-12">
                  <input type="text" class="search-query form-control" placeholder="Conversación" />
                  <button class="btn btn-danger" type="button">
                    <span class=" glyphicon glyphicon-search"></span>
                  </button>
                </div>
              </div>
              <div class="dropdown all_conversation hidden">
                <button class="dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-weixin" aria-hidden="true"></i> All Conversations
                  <span class="caret pull-right"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                  <li><a href="#"> All Conversation </a>
                    <ul class="sub_menu_ list-unstyled">
                      <li><a href="#"> All Conversation </a> </li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li><a href="#">Separated link</a></li>
                </ul>
              </div>
              <div class="member_list">
                <ul class="list-unstyled listMainUserChat">
                  @foreach ($dataUsers as $dataUser)
                    <?php
                      $nameUser = ucwords(strtolower($dataUser->name));
                    ?>
                    <li class="left clearfix itemUserChat" data-iduser="{{ $dataUser->id }}" data-nameuser="{{ $dataUser->name }}">
                      <span class="chat-img pull-left">
                        <img src="{{ asset("img/avatares/avatar_$dataUser->rol.png") }}" alt="User Avatar" class="img-circle">
                      </span>
                      <div class="chat-body clearfix">
                        <div class="header_sec">
                          <strong class="primary-font">{{ $nameUser }}</strong>
                          {{-- <strong class="pull-right">09:45AM</strong> --}}
                        </div>
                        <div class="contact_sec">
                          <strong class="primary-font">{{ $dataUser->rol }}</strong>
                          {{-- <span class="badge pull-right">0</span> --}}
                        </div>
                      </div>
                    </li>
                  @endforeach
                </ul>
              </div>
            </div>
          </div>
          <div class="col-sm-9 message_section">
            <div class="row">
              <div class="new_message_head">
                <div class="pull-left nameUserCurrent">
                  <i class="material-icons">record_voice_over</i> Estás hablando con <strong>WM Duberney</strong>
                </div>
                <div class="pull-right">
                  <div class="dropdown">
                    <button class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fa fa-cogs" aria-hidden="true"></i> Configuración
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                      <li><a href="#">Perfil</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="chat_area">
              </div>
              <div class="message_write">
                <form action="{{ route('buzonsend') }}" class="" method="POST" id="sendMessageChat">
                  {!! csrf_field() !!}
                  <textarea class="form-control" id="messageBuzon" name="messageBuzon" placeholder="Escribe un mensaje..."></textarea>
                  <input type="hidden" id="idReceivingUser" name="idReceivingUser" value="">
                  <input type="hidden" id="urlSendMessageBuzonAjax" value="{{ route('buzonsend') }}">
                  <input type="hidden" id="urlLoadMessageBuzonAjax" value="{{ route('buzonload') }}">

                  <div class="clearfix"></div>
                  <div class="chat_bottom">
                    <div class="row">
                      <div class="col-md-3 col-md-offset-6">
                        <div class="text-right textSending">
                          Enviado...
                          <div class="la-square-jelly-box la-dark la-sm pull-right loader">
                            <div></div>
                            <div></div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <a href="#" id="buttonSendMessageBuzon" class="btn btnSite">Enviar</a>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection