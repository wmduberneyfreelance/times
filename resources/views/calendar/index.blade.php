@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('js/vendor/calendar/fullcalendar.css') }}">
@endsection


@section('content')
<div class="maskNewEvent">
  <div class="newEvent">
    <div class="container-fluid">
      <form action="{{ route('calendarNewEventUser') }}" class="" method="POST" id="saveFormEventUser">
        {!! csrf_field() !!}
        <div class="row">
          <div class="col-md-12">
            <div class="textWelcome upperCase"><strong>CREAR Evento</strong>  personalizado</div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <input type="hidden" id="ajaxNewEventUser" value="{{ route('calendarNewEventUser') }}">
              <input type="hidden" id="ajaxEditEventUser" value="{{ route('editEventUser') }}">
              <input type="hidden" id="basePathOT" value="{{ route('loadotdetail', '1') }}">
              
              <textarea name="titleEvent" id="titleEvent" class="form-control" rows="3" placeholder="Agregar un titulo"></textarea>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="input-group">
              <div class="input-group-addon">Comienzo:</div>
              <div class='input-group date' id='datetimepicker1'>
                <input type="text" name="dateCalendarUser" class="form-control" id="dateCalendarUser" placeholder="">
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>
            <script type="text/javascript">
              $(function () {
                $('#datetimepicker1').datetimepicker({
                  format: 'YYYY-MM-DD HH:mm:00'
                });
              });
            </script>
          </div>
          <div class="col-md-6">
            <div class="input-group">
              <div class="input-group-addon">Fin:</div>
              <div class='input-group date' id='datetimepicker2'>
                <input type="text" name="dateCalendarUser2" class="form-control" id="dateCalendarUser2" placeholder="">
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>
            <script type="text/javascript">
              $(function () {
                $('#datetimepicker2').datetimepicker({
                  format: 'YYYY-MM-DD HH:mm:00'
                });
              });
            </script>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3 col-md-offset-6">
            <br><a class="btn btnSiteDanger closeMaskEvent" href="#"><i class="fa fa-chevron-left" aria-hidden="true"></i> CANCELAR</a>            
          </div>
          <div class="col-md-3">
            <br><a class="btn btn-default btnSiteOrange" href="#" role="button" id="btnAddEvent">AGREGAR <i class="fa fa-floppy-o" aria-hidden="true"></i></a>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
      <input type="hidden" value="{{ route('eventscalendar') }}" name="urlAjaxAuxCanlendar" id="urlAjaxAuxCanlendar">
      <div id='calendar'></div>
      <div class="plusCalendar">
        <i class="fa fa-plus" aria-hidden="true"></i>
      </div>
    </div>
  </div>
</div>
@endsection


@section('scripts')
  <script type="text/javascript" src="{{ asset('js/datepicker/moment.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/datepicker/bootstrap-datetimepicker.js') }}"></script>
  <script src="{{ asset('js/vendor/calendar/fullcalendar.min.js') }}"></script>
  <script type="text/javascript">
    swRequestCalendar = 0;
    $(document).ready(function(evt){
      iniCalendarAjax();
      $('#calendar').fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay,listWeek'
        },
        dayClick: function(date, jsEvent, view) {
          showAddNewEvent(date.format());
        },
        eventClick: function(calEvent, jsEvent, view) {
          console.log(calEvent);
          
        },
        eventDragStop: function( event, jsEvent, ui, view ) {
          // console.log(event);
        },

        eventDrop: function( event, delta, revertFunc, jsEvent, ui, view ) {
          console.log(event.start.format());
          updateTimeEvent(event.title, event.start.format(), event.end.format(), event.id);
        },

        eventResize: function( event, delta, revertFunc, jsEvent, ui, view ) {
          console.log(event);
          updateTimeEvent(event.title, "", event.end.format(), event.id);
        },
        eventMouseover: function(calEvent, jsEvent, view) {
            // $(this).css('background-color', 'red');
        },
        select: function (start, end, jsEvent, view) {
          console.log("oka");

          // var abc = prompt('Enter Title');
          // var allDay = !start.hasTime && !end.hasTime;
          // var newEvent = new Object();
          // newEvent.title = abc;
          // newEvent.start = moment(start).format();
          // newEvent.allDay = false;
          // $('#calendar').fullCalendar('renderEvent', newEvent);

        },


        allDayText: "Todo el día",
        buttonText: {
          today:    'Hoy',
          month:    'Mes',
          week:     'Semana',
          day:      'Día',
          list:     'Lista'
        },
        firstDay: 1,
        defaultView: "agendaWeek",
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        events: []
      })
    });

    function renderEventsCalendar(events){
      newEvents = [
      ]

      var totE = events.length;
      var baseURLEvent = $("#basePathOT").val();
      baseURLEvent = baseURLEvent.replace("loadot/1", "loadot/");
      
      for (var i = 0; i<totE; i++){
        // var edit = false;
        var edit = true;
        var col = '#254144'

        var evt = {
          title: events[i].message,
          start: events[i].date_start,
          end: events[i].date_done,
          editable: edit,
          color: col,
          id: events[i].id,
        }

        if (events[i].id_project == "" || events[i].id_project == 0){
          evt.editable = true;
          evt.color = '#1d1d1f'
        }else{
          var url = baseURLEvent + events[i].id_ot;
          evt.url = url
        }

        newEvents.push(evt);
      }

      console.log(newEvents);

      $('#calendar').fullCalendar( 'addEventSource', newEvents )
    }

    function iniCalendarAjax(){
      if (swRequestCalendar == 0){
        swRequestCalendar = 1;
        var fullPath = $.trim($("#urlAjaxAuxCanlendar").val());
        
        var dataString = {
          "key": "w8KRCHuIf78yPWeuJQht0yPzxSAi2ZZhLmP",
          "_token": $.trim($("input[name=_token]").val())
        };
        
        $.ajax({
          type: "POST",
          url: fullPath,
          data: dataString,
          datatype: 'json',
          cache: false,

          success: function(data){
            swRequestCalendar = 0;
            if (data.message == "success"){
              renderEventsCalendar(data.data)
            }
          }
        });
      }        
    }
  </script>
@endsection

