<!DOCTYPE html>
<html lang="es">
<head>
  <title>Bigotes</title>
  <meta charset="UTF-8" />
  <style type="text/css">
    td img {
      display: block;
    }
  </style>
</head>
<body bgcolor="#282828">
  <table cellpadding="0" cellspacing="0" width="100%">
    <tr>
      <td width="100%" align="center">
        <table style="display: inline-table;" border="0" cellpadding="0" cellspacing="0" width="650" bgcolor="#f0f0f0">
          <tr>
            <td style="background-color:#fff; padding: 10px" align="left" height="30px">
              <font face="Arial, Tahoma, Verdana" color="#773cbe" style="font-size: 14px;">
                Se ha reabierto una OT - Bigotes
              </font>
            </td>
          </tr>
          <tr>
           <td><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r1_c1_3.jpg" width="650" height="236" alt="" /></td>
          </tr>
          <tr>
           <td><table align="left" border="0" cellpadding="0" cellspacing="0" width="650">
            <tr>
              <td valign="top" style="background-color: #f0f0f0;"><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r2_c1_3.jpg" width="20" height="75" alt="" /></td>
              <td width="610" height="75" align="center" valign="top" style="background-color:#ffffff">
                <font face="Arial, Tahoma, Verdana" color="#773cbe" style="font-size: 18px;">
                  SE HA REABIERTO UNA<br>
                  <strong>ORDEN DE TRABAJO</strong>
                </font>
              </td>
              <td valign="top" style="background-color: #f0f0f0;"><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r2_c6_3.jpg" width="20" height="75" alt="" /></td>
            </tr>
          </table></td>
          </tr>
          <tr>
           <td><table align="left" border="0" cellpadding="0" cellspacing="0" width="650">
            <tr>
             <td valign="top" style="background-color: #f0f0f0;"><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r3_c1.jpg" width="20" height="218" alt="" /></td>
             <td><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r3_c2.jpg" width="26" height="218" alt="" /></td>
             <td><table align="left" border="0" cellpadding="0" cellspacing="0" width="531">
              <tr>
                <td width="531" height="178" align="left" valign="top" style="background-color:#ffffff">
                  <font face="Arial, Tahoma, Verdana" color="#737373" style="font-size: 14px; line-height: 18px;">
                    <strong style="color: #414141; font-size: 16px;">{{ $ower }},</strong><br><br>
                    Se ha reabierto una nueva orden de trabajo en el sistema por el usuario <strong>{{ $user->name }}</strong> y esta pendiente de asiganación.
                  </font>
                </td>
              </tr>
              <tr>
               <td><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r4_c3.jpg" width="531" height="40" alt="" /></td>
              </tr>
            </table></td>
             <td><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r3_c5.jpg" width="53" height="218" alt="" /></td>
             <td valign="top" style="background-color: #f0f0f0;"><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r3_c6.jpg" width="20" height="218" alt="" /></td>
            </tr>
          </table></td>
          </tr>
          <tr>
           <td><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r5_c1.jpg" width="650" height="26" alt="" /></td>
          </tr>
          <tr>
           <td><table align="left" border="0" cellpadding="0" cellspacing="0" width="650">
            <tr>
             <td><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r6_c1.jpg" width="46" height="52" alt="" /></td>
              <td width="531" height="52" align="left" valign="top" style="background-color:#f0f0f0">
                <font face="Arial, Tahoma, Verdana" color="#737373" style="font-size: 16px; line-height: 20px;">
                  <strong style="color: #414141; font-size: 16px;">POYECTO:</strong> {{ $project->name }} <br>
                  <strong style="color: #414141; font-size: 16px;">CUENTA:</strong> {{ $account->name }} <br>
                </font>
              </td>
             <td><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r6_c5.jpg" width="73" height="52" alt="" /></td>
            </tr>
          </table></td>
          </tr>
          <tr>
           <td><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r7_c1.jpg" width="650" height="18" alt="" /></td>
          </tr>
          <tr>
           <td><table align="left" border="0" cellpadding="0" cellspacing="0" width="650">
            <tr>
             <td><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r8_c1.jpg" width="46" height="43" alt="" /></td>
              <td>
                <a href="http://bigotes.bigholding.com.co/home" target="_blank">
                  <img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r8_c3.jpg" width="160" height="43" alt="" />
                </a>
              </td>
             <td><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r8_c4.jpg" width="444" height="43" alt="" /></td>
            </tr>
          </table></td>
          </tr>
          <tr>
           <td><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r9_c1.jpg" width="650" height="34" alt="" /></td>
          </tr>
          <tr>
           <td>
            <a href="http://bigotes.bigholding.com.co/home" target="_blank">
              <img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r10_c1.jpg" width="650" height="123" alt="" />
            </a>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>
