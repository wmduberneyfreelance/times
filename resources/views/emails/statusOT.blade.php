<!DOCTYPE html>
<html lang="es">
<head>
  <title>Bigotes</title>
  <meta charset="UTF-8" />
  <style type="text/css">
    td img {
      display: block;
    }
  </style>
</head>
<body bgcolor="#282828">
  <table cellpadding="0" cellspacing="0" width="100%">
    <tr>
      <td width="100%" align="center">
        <table style="display: inline-table;" border="0" cellpadding="0" cellspacing="0" width="650" bgcolor="#f0f0f0">
          <tr>
            <td style="background-color:#fff; padding: 10px" align="left" height="30px">
              <font face="Arial, Tahoma, Verdana" color="#0256CE" style="font-size: 14px;">
                Informe estado de Ot's - MAKE
              </font>
            </td>
          </tr>
          <tr>
           <td><img style="display: block; border: none;" src="http://makesystems.com.co/clientes/make/email/alert2_r1_c1_2.jpg" width="650" height="236" alt="" /></td>
          </tr>
          <tr>
           <td><table align="left" border="0" cellpadding="0" cellspacing="0" width="650">
            <tr>
              <td valign="top" style="background-color: #f0f0f0;"><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r2_c1_2.jpg" width="20" height="75" alt="" /></td>
              <td width="610" height="75" align="center" valign="top" style="background-color:#ffffff">
                <font face="Arial, Tahoma, Verdana" color="#0256CE" style="font-size: 18px;">
                  INFORME DIARIO DE GESTIÓN<br>
                  <strong>ORDENES DE TRABAJO</strong>
                </font>
              </td>
              <td valign="top" style="background-color: #f0f0f0;"><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r2_c6_2.jpg" width="20" height="75" alt="" /></td>
            </tr>
          </table></td>
          </tr>
          <tr>
           <td><table align="left" border="0" cellpadding="0" cellspacing="0" width="650">
            <tr>
             <td valign="top" style="background-color: #f0f0f0;"><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r3_c1.jpg" width="20" height="218" alt="" /></td>
             <td style="background-color:#ffffff"><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r3_c2.jpg" width="26" height="218" alt="" /></td>
             <td>
              <table align="left" border="0" cellpadding="0" cellspacing="0" width="531">
                <tr>
                  <td width="531" height="178" align="left" valign="top" style="background-color:#ffffff">
                    <font face="Arial, Tahoma, Verdana" color="#737373" style="font-size: 14px; line-height: 18px;">
                      <strong style="color: #414141; font-size: 16px;">{{ $ower }},</strong><br><br>
                      Las siguientes ordenes de trabajo no han sido completadas y estan vencidas:
                    </font>
                    <font face="Arial, Tahoma, Verdana" color="#000000" style="font-size: 14px; line-height: 18px;">
                      <br>
                      <br>
                      <table align="left" border="0" cellpadding="5" cellspacing="5" width="531">
                        @foreach ($otsPendientes as $otData)
                          <tr>
                            <td align="left" colspan="3">
                              <strong>OT</strong><br>
                              00{{ $otData->id }} - {{ $otData->name }}
                            </td>
                          </tr>
                          <tr>
                            <td align="left">
                              <strong>Fecha creación:</strong><br> {{ $otData->created_at }}</td>
                            <td align="left">
                              <strong>Fecha de entrega:</strong><br> {{ $otData->tentative_date }}
                            </td>
                            <td align="left">
                              <strong>Responsable:</strong><br> {{ $otData->userName }}
                            </td>
                          </tr>
                          <tr>
                            <td align="left" colspan="3">
                              <div style="height:2px; width:100%; background-color:#f0f0f0;"></div>
                            </td>
                          </tr>
                        @endforeach
                      </table
                    </font>
                  </td>
                </tr>
                <tr>
                <td><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r4_c3.jpg" width="531" height="40" alt="" /></td>
                </tr>
              </table>
             </td>
             <td style="background-color:#ffffff"><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r3_c5.jpg" width="53" height="218" alt="" /></td>
             <td valign="top" style="background-color: #f0f0f0;"><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r3_c6.jpg" width="20" height="218" alt="" /></td>
            </tr>
          </table></td>
          </tr>
          <tr>
           <td><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r5_c1.jpg" width="650" height="26" alt="" /></td>
          </tr>
          <tr>
           <td>


            </td>
          </tr>
          <tr>
           <td><img style="display: block; border: none;" src="http://bigholding.com.co/emails/bigotes/alert2_r7_c1.jpg" width="650" height="18" alt="" /></td>
          </tr>
          <tr>
           <td></td>
          </tr>
          <tr>
           <td><img style="display: block; border: none;" src="http://makesystems.com.co/clientes/make/email/alert2_r9_c1.jpg" width="650" height="34" alt="" /></td>
          </tr>
          <tr>
           <td>
            <a href="http://makesystems.com.co" target="_blank">
              <img style="display: block; border: none;" src="http://makesystems.com.co/clientes/make/email/alert2_r10_c1_v3.jpg" width="650" height="123" alt="" />
            </a>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>
