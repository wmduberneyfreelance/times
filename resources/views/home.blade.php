@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-2 mainLeftZone">
      @include('partials.leftZoneCalendar')
    </div>
    <div class="col-md-10 mainRightZone">
      <div class="textWelcome">
        <i class="fa fa-user" aria-hidden="true"></i> BIENVENIDO <strong>{{ Auth::user()->name }}</strong>
        {{-- <div class="lastConexion pull-right">Última conexión hoy 7:30am</div> --}}
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="totOTS">
            {{ count($ots) }}
          </div>
        </div>
        <div class="col-md-6">
          <div class="mainTableOts">
            <br>CASOS <strong>PENDIENTES</strong><br><br>

            <div class="table-responsive">
              <table class="table table-hover">
                <tr>
                  <th>Estado</th>
                  <th>OT</th>
                  <th></th>
                </tr>
                @foreach ($ots as $ot)
                  <?php 
                    $statusName = strtoupper($ot->status);
                  ?>
                  <tr>
                    <td>
                      <button type="button" class="btnStatusOt otSt{{ $ot->status }}" data-toggle="tooltip" data-placement="top" title="{{ $statusName }}"></button>
                    </td>
                    <td>
                      <a href="{{ route('loadotdetail', $ot->id) }}">
                        00{{ $ot->id }} - {{ $ot->name }}
                      </a>
                    </td>
                    <td>
                      <a href="{{ route('loadotdetail', $ot->id) }}">
                        Consultar <i class="fa fa-search"></i>
                      </a>
                    </td>
                  </tr>
                @endforeach
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
