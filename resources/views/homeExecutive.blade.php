@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-2 mainLeftZone">
      @include('partials.leftZoneCalendar')
    </div>
    <div class="col-md-10 mainRightZone">
      <div class="textWelcome">
        <i class="fa fa-user" aria-hidden="true"></i> BIENVENIDO <strong>{{ Auth::user()->name }}</strong>
        {{-- <div class="lastConexion pull-right">Última conexión hoy 7:30am</div> --}}
      </div>
      <div class="row">
        <div class="col-md-3">
          <img src="{{ asset("img/gifs/6bbb9fe4fe118fa6edc85c674ce91a7d.gif") }}" class="img-responsive center-block" width="200px">
        </div>
        <div class="col-md-9">
          <div class="mainTableOts">
            <br>ESTAS SON TUS ÚLTIMAS <strong> ORDENES DE TRABAJO</strong><br><br>

            <div class="table-responsive">
              <table class="table table-hover">
                <tr>
                  <th>Estado</th>
                  <th>Tipo de OT</th>
                  <th>Cuenta</th>
                  <th>Cliente</th>
                  <th>OT</th>
                  <th width="20"></th>
                </tr>
                @foreach ($ots as $ot)
                  <?php 
                    $statusName = strtoupper($ot->status);
                  ?>
                  <tr>
                    <td>
                      <button type="button" class="btnStatusOt otSt{{ $ot->status }}" data-toggle="tooltip" data-placement="top" title="{{ $statusName }}"></button>
                    </td>
                    <td> {{ $ot->nameTypeOT }} </td>
                    <td> {{ $ot->nameAccount }} </td>
                    <td> {{ $ot->nameClient }} </td>
                    <td>
                      <a href="{{ route('showOt', [$ot->id_project, $ot->id]) }}">
                        00{{ $ot->id }} - {{ $ot->name }}
                      </a>
                    </td>
                    <td>
                      <a href="{{ route('showOt', [$ot->id_project, $ot->id]) }}"><i class="fa fa-search"></i></a>
                    </td>
                  </tr>
                @endforeach
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
