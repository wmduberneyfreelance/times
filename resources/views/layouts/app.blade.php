<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>AGUDELO PELÁEZ - ABOGADOS</title>
    
    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,400,300,500,700' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/animate.css') }}" rel='stylesheet' type='text/css'>
    <link href="{{ asset('css/loaders.css') }}" rel='stylesheet' type='text/css'>
    <link href="{{ asset('css/font-awesome.css') }}" rel='stylesheet' type='text/css'>
    <link href="{{ asset('css/bootstrap.css') }}" rel='stylesheet' type='text/css'>
    <link href="{{ asset('css/datepicker/bootstrap-datetimepicker.css') }}" rel='stylesheet' type='text/css'>

    @yield('styles')

    <link href="{{ asset('css/main.css') }}" rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="{{ asset('js/html5shiv.js') }}"></script>
      <script src="{{ asset('js/respond.min.js') }}"></script>
    <![endif]-->


    <!-- JavaScripts -->
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    <script src="/js/vendor/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/main.js"></script>

</head>
<body id="app-layout">
  <nav class="navbar navbar-default headerAPLegal">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#spark-navbar-collapse">
          <span class="sr-only">Toggle Navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div class="mainBrand">
          <a href="{{ url('/home') }}">
            <img src="{{ asset("img/logoMain.png") }}" onerror="this.onerror=null; this.src='{{ asset("img/logoMain.png") }}'" width="132px">
          </a>
          <div class="nameUser">
            <i class="material-icons">verified_user</i>
            {{ Auth::user()->name }}
            <span>{{ Auth::user()->rol }}</span>
            <input type="hidden" id="genericColorRol" value="{{ Auth::user()->rol }}">
          </div>
        </div>
      </div>

      <div class="collapse navbar-collapse" id="spark-navbar-collapse">
        <!-- Right Side Of Navbar -->
        <ul class="nav navbar-nav navbar-right navBigotes">
          <!-- Authentication Links -->
          @if (Auth::guest())
            <li><a href="{{ url('/login') }}">Login</a></li>
            <li><a href="{{ url('/register') }}">Register</a></li>
          @else
            @if (Auth::user()->rol === 'superadmin')
              <li><a href="{{ url('/admin') }}" class="{{ strpos(Request::url(), 'com/admin') ? 'active' : '' }}">ADMINISTRADOR</a></li>
            @endif
            @if (Auth::user()->rol === 'superadmin' || Auth::user()->rol === 'moderator' || Auth::user()->rol === 'executive')
              <li><a href="{{ route('loadprojects') }}" class="{{ strpos(Request::url(), 'projects') ? 'active' : '' }}">PROYECTOS</a></li>
            @endif
              <li><a href="{{ route('loadots') }}"  class="{{ strpos(Request::url(), 'loadots') ? 'active' : '' }}">MIS CASOS</a></li>

              <li><a href="{{ route('blog') }}" class="{{ strpos(Request::url(), 'blog') ? 'active' : '' }}">FEEDBACK</a></li>

              <li><a href="{{ url('/logout') }}">CERRAR SESIÓN</a></li>
              {{-- <li>
                <a href="{{ route('buzon') }}"><i class="material-icons">message</i></a>
              </li>

              <li>
                <a href="#">
                   @if ($countNotifications > 0)
                    <span class="activeNot">
                      {{ $countNotifications }}
                      <i class="material-icons">notifications_active</i>
                    </span>
                   @else
                    <i class="material-icons">notifications </i>
                   @endif                  
                </a>
              </li>
               --}}
          @endif
        </ul>
      </div>
    </div>
  </nav>

  <div class="mainDisplay">
    @yield('content')
  </div>

  <div class="footer">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          Todos los derechos reservados APLegal 2018
        </div>
        <div class="col-md-4">
          <div class="pull-right">
            <img src="{{ asset("img/login/logo-purple.png") }}">
            <a href="http://aplegal.com.co/" class="purpleLink">www.aplegal.com.co</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  @yield('scripts')
</body>
</html>
