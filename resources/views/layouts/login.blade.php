<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>AGUDELO PELÁEZ - ABOGADOS</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="{{ asset('css/font-awesome.css') }}" rel='stylesheet' type='text/css'>
    <link href="{{ asset('css/bootstrap.css') }}" rel='stylesheet' type='text/css'>
    <link href="{{ asset('css/login.css') }}" rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="{{ asset('js/html5shiv.js') }}"></script>
      <script src="{{ asset('js/respond.min.js') }}"></script>
    <![endif]-->
</head>
<body>

  @yield('content')

  <div class="footer">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          Todos los derechos reservados AGUDELO PELÁEZ - ABOGADOS
        </div>
        <div class="col-md-4">
          <div class="pull-right">
            <img src="{{ asset("img/login/logo-purple.png") }}">
            <a href="http://aplegal.com.co/" class="purpleLink">legal.com.co</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- JavaScripts -->
  {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
  <script src="/js/vendor/jquery.min.js"></script>
  <script type="text/javascript" src="{{ asset('js/vendor/supersized.core.3.2.1.min.js') }}"></script>
  <script src="/js/bootstrap.min.js"></script>

  <script type="text/javascript">
    jQuery(function($){
      $.supersized({
       slides  :   [ {image : '{{ asset("img/login/background.png") }}', title : ''} ],
       vertical_center: 2
      });
    });

    $(window).bind("resize", function() {
      updateMask();
    });

    $(window).load(function(e){
      updateMask();
    });


    updateMask();

    function updateMask(){
      var maskHeight = $(window).height();
      var maskWidth = $(window).width();

      var mrgTop = ((maskHeight/2)-($(".contentLogin").height()/2))-40;

      $(".contentLogin").css({
        "margin-top": mrgTop + "px"
      });
    }



    var vid = document.getElementById("bgvid");
    var pauseButton = document.querySelector("#bigotes");

    function vidFade() {
      vid.classList.add("stopfade");
    }

    vid.addEventListener('ended', function()
    {
    // only functional if "loop" is removed 
    vid.pause();
    // to capture IE10
    vidFade();
    }); 


    pauseButton.addEventListener("click", function() {
      vid.classList.toggle("stopfade");
      if (vid.paused) {
        vid.play();
        pauseButton.innerHTML = "Pause";
      } else {
        vid.pause();
        pauseButton.innerHTML = "Paused";
      }
    })


  </script>

</body>
</html>
