

@extends('layouts.app')

@section('styles')
  <link rel="stylesheet" media="screen" type="text/css" href="{{ asset('css/colorpicker/layout.css') }}" />
  <link rel="stylesheet" media="screen" type="text/css" href="{{ asset('css/colorpicker/colorpicker.css') }}" />
@endsection

@section('content')
<div class="editorBigmail">
  <div class="buttonClose"><i class="material-icons">close</i></div>
  <br><br>
  <textarea class="form-control" rows="3" placeholder="Ingresa el texto..."></textarea>
  <table class="table">
    <tr>
      <td>Color de texto</td>
      <td width="20"><div class="colorSelector" id="textColor"><div></div></div></td>
    </tr>
    <tr>
      <td>Color de fondo</td>
      <td width="20"><div class="colorSelector" id="bgColor"><div></div></div></td>
    </tr>
  </table>
  <a class="btn btn-default btnSiteOrange buttonAceptEditor" href="#">ACEPTAR <i class="fa fa-refresh"></i></a>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-2 mainLeftZone">
      @include('partials.leftZoneCalendar')
    </div>
    <div class="col-md-10 mainRightZone">
      <div class="textWelcome">
        EDITOR BIG<strong>MAILS</strong>
      </div>
      @if (Session::has('message'))
        <p class="alert alert-success">
          {{ Session::get('message') }}
        </p>
      @endif
      <div class="row">
        <div class="col-md-12">
          <br>
          <form class="form-inline pull-right" action="{{ route('loadclientsmails') }}" method="GET" role="search">
            <div class="form-group">
              <input type="text" class="form-control" id="searchNameClient" name="searchNameClient" placeholder="Buscar cliente" value="{{ Request::get('searchNameClient') }}">
            </div>
            <button type="submit" class="btn btn-default btnSite2"><i class="fa fa-search"></i></button>
          </form>
        </div>
        <div class="clear-fix"></div>
        <div class="col-md-12">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
              <a href="#design" aria-controls="design" role="tab" data-toggle="tab">Diseño</a>
            </li>
            <li role="presentation">
              <a href="#current_code" aria-controls="current_code" role="tab" data-toggle="tab">Código actual</a>
            </li>
            <li role="presentation">
              <a href="#optimized_code" aria-controls="optimized_code" role="tab" data-toggle="tab">Código optimizado</a>
            </li>
            <li role="presentation">
              <a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab">Revisiones</a>
            </li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="design">              
              <div id="email-preview"></div>
            </div>
            <div role="tabpanel" class="tab-pane" id="current_code">
              hola
            </div>
            <div role="tabpanel" class="tab-pane" id="optimized_code">
              
            </div>
            <div role="tabpanel" class="tab-pane" id="reviews">
            </div>
           </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection



@section('scripts')
  {{-- <script type="text/javascript" src="{{ asset('js/colorpicker/jquery.js') }}"></script> --}}
  <script type="text/javascript" src="{{ asset('js/colorpicker/colorpicker.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/colorpicker/eye.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/colorpicker/utils.js') }}"></script>
  {{-- <script type="text/javascript" src="{{ asset('js/colorpicker/layout.js?ver=1.0.2') }}"></script> --}}

  <script type="text/javascript" src="{{ asset('js/bigmail.js') }}"></script>
  <script type="text/javascript">
    iniEditorBigMail();
  </script>
@endsection