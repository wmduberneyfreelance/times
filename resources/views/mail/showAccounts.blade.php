@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-2 mainLeftZone">
      @include('partials.leftZoneCalendar')
    </div>
    <div class="col-md-10 mainRightZone">
      <div class="textWelcome">
        CUENTAS - <strong>{{ $client->name }}</strong>
      </div>
      @if (Session::has('message'))
        <p class="alert alert-success">
          {{ Session::get('message') }}
        </p>
      @endif
      <div class="row">
        <div class="col-md-12">
          <br>
          <form class="form-inline pull-right" action="{{ route('loadclientsmails') }}" method="GET" role="search">
            <div class="form-group">
              <input type="text" class="form-control" id="searchNameClient" name="searchNameClient" placeholder="Buscar cuenta" value="{{ Request::get('searchNameClient') }}">
            </div>
            <button type="submit" class="btn btn-default btnSite2"><i class="fa fa-search"></i></button>
          </form>
        </div>
      </div>
      <form class="">
        <br>
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table class="table table-hover">
                <tr>
                    <th>Nombre</th>
                    <th>Estado</th>
                    <th width="120"></th>
                </tr>
                @foreach ($accounts as $account)
                  <tr data-id="{{ $account->id }}">
                    <td>
                      <a href="{{ route('showprojectsaccount', [$client->id, $account->id]) }}" class="linkSite">
                        {{ $account->name }}
                      </a>
                    </td>
                    <td>
                      @if ($account->status === "active")
                        <div class="profileActive"></div> Activo
                      @elseif ($account->status === "inactive")
                        <div class="profileInactive"></div> Inactivo
                      @endif
                    </td>
                    <td>
                      <a href="{{ route('showprojectsaccount', [$client->id, $account->id]) }}" class="linkSite">
                        Ver proyectos <i class="fa fa-table" aria-hidden="true"></i>
                      </a>
                    </td>
                  </tr>
                @endforeach
              </table>
            </div>
            <div class="pagination">
              {{ $accounts->appends(Request::all())->render() }}
            </div>
          </div>
        </div>
      </form>
      <form action="{{ route('destroyprofile', ':USER_ID') }}" class="" method="POST" id="deleteFormProfile">
          {!! csrf_field() !!}
      </form>
    </div>
  </div>
</div>
@endsection
