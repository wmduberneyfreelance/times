@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3 mainLeftZone">
      <div class="row">
        <div class="col-md-6 noPadding">
          <table class="tableStatusPro">
            <tr>
              <td width="30"><div class="statusProject new"></div></td>
              <td> Nuevo</td>
            </tr>
            <tr>
              <td><div class="statusProject complet"></div></td>
              <td> Completo</td>
            </tr>
          </table>
        </div>
        <div class="col-md-6 noPadding">
          <table class="tableStatusPro">
            <tr>
              <td width="30"><div class="statusProject trafic"></div></td>
              <td> En tráfico</td>
            </tr>
            <tr>
              <td><div class="statusProject cancel"></div></td>
              <td> Cancelado</td>
            </tr>
          </table>
        </div>
      </div>
      <br>
    </div>
    <div class="col-md-9 mainRightZone">
      <div class="textWelcome">
      	OT'S ASOCIADAS A <strong> BIGMAIL</strong>
      </div>
      <br>

      @if (Session::has('message'))
        <p class="alert alert-success">
					{!! Session::get('message') !!}
        </p>
      @endif
            

      <div class="row">
        <div class="col-md-12">
          <br>
          <form class="form-inline pull-right" action="{{ route('loadprojects') }}" method="GET" role="search">
            <div class="form-group">
              <input type="text" class="form-control" id="searchNameProject" name="searchNameProject" placeholder="Buscar orden de trabajo" value="{{ Request::get('searchNameProject') }}">
            </div>
            <button type="submit" class="btn btn-default btnSite2"><i class="fa fa-search"></i></button>
          </form>
        </div>
      </div>
        <form class="">
          <br>
          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table table-hover">
                    <tr>
                        <th width="50">Estado</th>
                        <th>Nombre</th>
                        <th>Cuenta</th>
                        <th>Tipo de OT</th>
                        <th>Fecha de entrega</th>
                        <th width="60"></th>
                    </tr>
                     @foreach ($otsData as $otData)
                        <tr>
                            <td><div class="statusProject {{ $otData->status }}"></div></td>
                            <td>
                              <a href="{{ route('loadotdetail', $otData->id) }}" class="linkSite">
                                {{ $otData->name }}
                              </a>
                            </td>
                            <td>{{ $otData->nameAccount }}</td>
                            <td>{{ $otData->nameTypeOT }}</td>
                            <td>{{ $otData->tentative_date }}</td>
                            <td>
                              <a href="{{ route('loadotdetail', $otData->id) }}" class="linkSite">
                                Ver <i class="fa fa-pencil"></i>
                              </a>
                            </td>
                        </tr>
                    @endforeach
                </table>
              </div>
              <div class="pagination">
                {{ $otsData->appends(Request::all())->render() }}
              </div>
          </div>
          </div>
        </form>
        <form action="{{ route('destroyprofile', ':USER_ID') }}" class="" method="POST" id="deleteFormProfile">
            {!! csrf_field() !!}
        </form>
        </div>
</div>
@endsection


@section('scripts')
    <script type="text/javascript" src="{{ asset('js/datepicker/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/datepicker/bootstrap-datetimepicker.js') }}"></script>
@endsection

