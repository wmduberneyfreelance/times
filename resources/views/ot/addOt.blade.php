@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/upload/blueimp-gallery.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/upload/jquery.fileupload.css') }}">
    <link rel="stylesheet" href="{{ asset('css/upload/jquery.fileupload-ui.css') }}">
    <!-- CSS adjustments for browsers with JavaScript disabled -->
    <noscript><link rel="stylesheet" href="{{ asset('css/upload/jquery.fileupload-noscript.css') }}"></noscript>
    <noscript><link rel="stylesheet" href="{{ asset('css/upload/jquery.fileupload-ui-noscript.css') }}"></noscript>
@endsection


@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-3 mainLeftZone">
      <div class="row">
        <div class="col-md-6 noPadding">
          <table class="tableStatusPro">
            <tr>
              <td width="30"><div class="statusProject new"></div></td>
              <td> Nuevo</td>
            </tr>
            <tr>
              <td><div class="statusProject complet"></div></td>
              <td> Completo</td>
            </tr>
            <tr>
              <td><div class="statusProject ajustes"></div></td>
              <td> Ajustes</td>
            </tr>
          </table>
        </div>
        <div class="col-md-6 noPadding">
          <table class="tableStatusPro">
            <tr>
              <td width="30"><div class="statusProject trafic"></div></td>
              <td> En tráfico</td>
            </tr>
            <tr>
              <td><div class="statusProject cancel"></div></td>
              <td> Cancelado</td>
            </tr>
          </table>
        </div>
      </div>
      <br>
    </div>
    <div class="col-md-9 mainRightZone">
      <div class="textWelcome">
      	NUEVA <strong>ORDEN DE TRABAJO</strong>
      </div>
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
          <a href="#generalDataOT" aria-controls="generalDataOT" role="tab" data-toggle="tab">General</a>
        </li>
        <li role="presentation">
          <a href="#documentsOT" aria-controls="documentsOT" role="tab" data-toggle="tab">Documentos</a>
        </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="generalDataOT">
          <div class="row">
            <form action="{{ route('saveot') }}" class="" method="POST" id="saveFormOT">
              {!! csrf_field() !!}
              
              <div class="col-md-12">
                <div class="subTitle">Información <strong>general</strong></div>
                @include('admin.partials.messages')
                <input type="hidden" value="{{ route('loaddetailtypesot') }}" name="urlAjaxAuxField" id="urlAjaxAuxField">
                <input type="hidden" value="{{ old('metadataOT') }}" name="metadataOT" id="metadataOT">
                <input type="hidden" value="{{ $idProject }}" name="idProject" id="idProject">
                <input type="hidden" value="{{ $idUser }}" name="idUser" id="idUser">
                <input type="hidden" value="{{ $idPieceByProject }}" name="idPieceByProject" id="idPieceByProject">
                <input type="hidden" value="" name="idOT" id="idOT">
              </div>
              <div class="clearfix"></div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="input-group {{ $errors->has('nameOt') ? ' has-error' : '' }}">
                    <div class="input-group-addon">Nombre:</div>
                    <input type="text" name="nameOt" class="form-control" id="nameOt" placeholder="" value="{{ old('nameOt') }}">
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group {{ $errors->has('tentativeDateOT') ? ' has-error' : '' }}">
                  <div class="input-group date"  id='datetimepicker1'>
                    <div class="input-group-addon">Fecha tentativa de entrega:</div>
                    <input type="text" name="tentativeDateOT" class="form-control" id="tentativeDateOT" placeholder="" value="{{ old('tentativeDateOT') }}">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                  </div>
                </div>
                <script type="text/javascript">
                    $(function () {
                        $('#datetimepicker1').datetimepicker({
                            "format": "YYYY-MM-DD"
                        });
                    });
                </script>
              </div>
              <div class="clearfix"></div>
              <div class="col-md-6">
                <div class="form-group  {{ $errors->has('priorityOT') ? ' has-error' : '' }}">
                  <div class="input-group">
                    <div class="input-group-addon">Prioridad:</div>
                    <div class="select-style">
                      <select class="form-control" name="priorityOT" id="priorityOT"> 
                        <option value="">--Seleccionar--</option>
                        <option value="baja">Baja</option>
                        <option value="media">Media</option>
                        <option value="alta">Alta</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group {{ $errors->has('complexityOT') ? ' has-error' : '' }}">
                  <div class="input-group">
                    <div class="input-group-addon">Complejidad de la tarea:</div>
                    <div class="select-style">
                      <select class="form-control" name="complexityOT" id="complexityOT"> 
                        <option value="">--Seleccionar--</option>
                        <option value="baja">Baja</option>
                        <option value="media">Media</option>
                        <option value="alta">Alta</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-md-6">
                <div class="form-group {{ $errors->has('otType') ? ' has-error' : '' }}">
                  <div class="input-group">
                    <div class="input-group-addon">Tipo de OT:</div>
                    <div class="select-style">
                      <select class="form-control" name="otType" id="otType"> 
                        <option value="">--Seleccionar--</option>
                        @foreach ($otTypes as $otType)
                          <option value="{{ $otType->id }}" data-ot="{{ $otType->name }}">{{ $otType->name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>                    
              <div class="col-md-12">
                <br>
                <div class="form-group" {{ $errors->has('descriptionOT') ? ' has-error' : '' }}">
                  <label for="exampleInputEmail1">
                    <strong><i class="material-icons">comment</i> DESCRIPCIÓN:</strong>
                  </label>
                  <textarea class="form-control ckeditor" name="descriptionOT" id="descriptionOT"></textarea>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-md-12">
                <div class="panel panel-default">
                  <div class="panel-heading"><strong><i class="material-icons">announcement</i> RECOMENDACIONES DEL CLIENTE:</strong></div>
                  <div class="panel-body">
                    <div class="form-group" {{ $errors->has('recommendationsOT') ? ' has-error' : '' }}">
                      {{-- <label for="exampleInputEmail1">Recomendaciones del cliente:</label> --}}
                      <textarea class="form-control" name="recommendationsOT" id="recommendationsOT"></textarea>
                    </div>
                  </div>
                </div>

                
              </div>
            </form>
            <div class="clearfix"></div>
            <div class="col-md-12">
              <div class="panel panel-primary" id="mainDisplayFormFields">
                <div class="panel-heading"></div>
                <div class="panel-body">
                  <form action="#" id="formDinamicOT">
                    <div class="displayFormDinamic"></div>
                  </form>
                </div>
              </div>
            </div>
          </div> 
        </div>
        <div role="tabpanel" class="tab-pane" id="documentsOT">
          <div class="row mainErrorDocuments">
            <div class="col-md-12">
              <div class="alert alert-danger" role="alert">
                Para habilitar esta opción primero debes completar la <strong>ORDERN DE TRABAJO</strong> y <strong>GUARDARLA.</strong>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-12">
          <br><br>
          <div class="row">
              <div class="col-md-3 col-md-offset-6">
                  <a class="btn btn-default btnSite" href="{{ route('showproject', $idProject) }}" role="button"> <i class="fa fa-chevron-left" aria-hidden="true"></i> REGRESAR </a>
              </div>
              <div class="col-md-3">
                  <a class="btn btn-default btnSiteOrange" href="#" role="button" id="btnSaveOt">GUARDAR <i class="fa fa-check"></i></a>
              </div>
              <br><br><br>
          </div>
      </div>
    </div>
    </div>
</div>
@endsection


@section('scripts')
  <script src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/datepicker/moment.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/datepicker/bootstrap-datetimepicker.js') }}"></script>
  <script type="text/javascript">
      $(document).ready(function(evt){
          $("#idAccount").on("change", function(evt){
              var nameClient = $(this).find(':selected').data('client');
              $("#nameClientSelected").val(nameClient);
          });
      });
  </script>

@endsection






