@extends('layouts.app')

@section('styles')
  <link rel="stylesheet" href="{{ asset('css/autocomplete/jquery.auto-complete.css') }}">
  <link rel="stylesheet" href="{{ asset('css/upload/blueimp-gallery.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/upload/jquery.fileupload.css') }}">
  <link rel="stylesheet" href="{{ asset('css/upload/jquery.fileupload-ui.css') }}">

  <!-- CSS adjustments for browsers with JavaScript disabled -->
  <noscript><link rel="stylesheet" href="{{ asset('css/upload/jquery.fileupload-noscript.css') }}"></noscript>
  <noscript><link rel="stylesheet" href="{{ asset('css/upload/jquery.fileupload-ui-noscript.css') }}"></noscript>
@endsection


@section('content')

<?php $nameUser = "" ?>

@if ($otData->status == "completa" || $otData->status == "cancelada")
  <div class="maskAdjustOT">
    <div class="formAdjustOT">
      <div class="container-fluid">
        <form action="{{ route('editotstatus') }}" class="" method="POST" id="updateFormAdjustOT">
          {!! csrf_field() !!}
          <input type="hidden" value="{{ $idProject }}" name="idProjectAdjust" id="idProjectAdjust">
          <input type="hidden" value="{{ $otData->id }}" name="idOtAdjust" id="idOtAdjust">

          <div class="row">
            <div class="col-md-12">
              <div class="textWelcome text-center">
                AJUSTAR <strong>ORDEN DE TRABAJO</strong>
              </div>
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="col-md-5">
              <img src="{{ asset("img/gifs/04b1ee172dbce744476b08084b1604e9.gif") }}" class="img-responsive center-block">
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <textarea class="form-control" rows="4" name="commentsAdjust" id="commentsAdjust" placeholder="Cuentanos que paso..."></textarea>
              </div>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
              <a class="btn btn-default btnSite btnCloseMaskAdjustOT" href="#" role="button"> <i class="fa fa-chevron-left" aria-hidden="true"></i> REGRESAR </a>
            </div>
            <div class="col-md-6">
              <a class="btn btn-default btnSiteOrange" href="#" role="button" id="btnAdjustOT">AJUSTAR <i class="fa fa-wrench" aria-hidden="true"></i></a>
            </div>
            <br><br><br>
          </div>
        </form>
      </div>
    </div>  
  </div>
@endif

<div class="container">
  <div class="row">
    <div class="col-md-3 mainLeftZone">
      @include('partials.leftZonePercentage')
    </div>
    <div class="col-md-9 mainRightZone">
      <div class="textWelcome upperCase">
        <strong>OT:</strong> 00{{ $otData->id }} - {{ $otData->name }}
      </div>
      <br><span class="nameAutoOT">Creada por: <strong>{{ $infoUserCreator->name }}</strong></span>
      <br><br>
      
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
          <a href="#generalDataOT" aria-controls="generalDataOT" role="tab" data-toggle="tab">General</a>
        </li>
        <li role="presentation">
          <a href="#documentsOT" aria-controls="documentsOT" role="tab" data-toggle="tab">Documentos</a>
        </li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="generalDataOT">
          <div class="row">
            @if ($user->rol == 'superadmin')
              <form action="{{ route('editot', $otData->id) }}" class="" method="POST" id="updateFormOT">
            @elseif ($user->rol == 'moderator')
              <form action="{{ route('editotModerator', $otData->id) }}" class="" method="POST" id="updateFormOT">
            @endif
              {!! csrf_field() !!}
              
              <div class="col-md-12">
                <div class="subTitle"><i class="fa fa-info-circle" aria-hidden="true"></i> Información <strong>general</strong></div>
                @include('admin.partials.messages')
                @if (Session::has('message'))
                  <p class="alert alert-success">
                    {!! Session::get('message') !!}
                  </p>
                @endif
                
                <input type="hidden" value="{{ route('loaddetailtypesot') }}" name="urlAjaxAuxField" id="urlAjaxAuxField">
                <input type="hidden" value="{{ old('metadataOT') }}" name="metadataOT" id="metadataOT">
                <input type="hidden" value="{{ $idProject }}" name="idProject" id="idProject">
                <input type="hidden" value="{{ $idUser }}" name="idUser" id="idUser">
                <input type="hidden" value="{{ $otData->id }}" name="idOT" id="idOT">
                <input type="hidden" value="{{ $path }}" name="publicPath" id="publicPath">
                <input type="hidden" value="{{ route('deleteFileByOt') }}" name="urlDeleteFile" id="urlDeleteFile">
                <input type="hidden" value="{{ route('publishComments') }}" name="urlPublishComments" id="urlPublishComments">

              </div>
              <div class="clearfix"></div>

              @foreach ($users as $userGlobal)
                @if ($userGlobal->id == $otData->id_user)
                  <?php $nameUser = $userGlobal->name; ?>
                @endif
              @endforeach

              @if ($user->rol == 'superadmin' || $user->rol == 'moderator')
                <div class="col-md-6">
                  <div class="form-group {{ $errors->has('otStatus') ? ' has-error' : '' }}">
                    <div class="input-group">
                      <div class="input-group-addon">Estado:</div>
                        <div class="select-style">
                          <select class="form-control" name="otStatus" id="otStatus"> 
                            <option value="">--Seleccionar--</option>
                            @if ($otData->status == "nueva")
                              <option value="nueva" selected="selected">Nueva</option>
                              <option value="asignada">Asignada</option>
                              <option value="completa">Completa</option>
                              <option value="ajustes">Ajustes</option>
                              <option value="cancelada">Cancelada</option>
                            @elseif ($otData->status == "asignada")
                              <option value="nueva">Nueva</option>
                              <option value="asignada" selected="selected">Asignada</option>
                              <option value="completa">Completa</option>
                              <option value="ajustes">Ajustes</option>
                              <option value="cancelada">Cancelada</option>
                            @elseif ($otData->status == "completa")
                              <option value="nueva">Nueva</option>
                              <option value="asignada">Asignada</option>
                              <option value="completa" selected="selected">Completa</option>
                              <option value="ajustes">Ajustes</option>
                              <option value="cancelada">Cancelada</option>
                            @elseif ($otData->status == "ajustes")
                              <option value="nueva">Nueva</option>
                              <option value="asignada">Asignada</option>
                              <option value="completa">Completa</option>
                              <option value="ajustes" selected="selected">Ajustes</option>
                              <option value="cancelada">Cancelada</option>
                            @elseif ($otData->status == "cancelada")
                              <option value="nueva">Nueva</option>
                              <option value="asignada">Asignada</option>
                              <option value="completa">Completa</option>
                              <option value="ajustes">Ajustes</option>
                              <option value="cancelada" selected="selected">Cancelada</option>
                            @endif
                          </select>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="input-group {{ $errors->has('userAsignOt') ? ' has-error' : '' }}">
                      <div class="input-group-addon">Encargado:</div>
                      <input type="hidden" class="form-control" name="userAsignOt"  id="userAsignOt" placeholder="" value="{{ $otData->id_user }}">
                      <input type="text" name="q" autofocus  class="form-control" id="advancedUserName" placeholder="" value="<?php echo $nameUser; ?>">
                    </div>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="input-group {{ $errors->has('dateCalendarUser') ? ' has-error' : '' }}">
                      <div class="input-group-addon">Fecha calendario:</div>
                      <div class='input-group date' id='datetimepicker1'>
                        <input type="text" name="dateCalendarUser" class="form-control" id="dateCalendarUser" placeholder="" value="{{ $otDataTime['date_assigned'] }}">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                      </div>
                    </div>
                  </div>
                  <script type="text/javascript">
                    $(function () {
                      $('#datetimepicker1').datetimepicker({
                        format: 'YYYY-MM-DD HH:mm:00'
                      });
                    });
                  </script>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="input-group {{ $errors->has('hourCalendarUser') ? ' has-error' : '' }}">
                      <div class="input-group-addon">Horas programadas:</div>
                      <div class='input-group'>
                        <input type="number" name="hourCalendarUser" class="form-control" id="hourCalendarUser" placeholder="" value="{{ $otDataTime['hours'] }}">
                        <span class="input-group-addon">
                          <span class="glyphicon glyphicon-time"></span>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">
                  <div class="form-group">
                    <textarea class="form-control" rows="3" name="commentsGeneral" id="commentsGeneral" placeholder="Ingresa tus comentarios..."></textarea>
                  </div>                  
                </div>

                <div class="clearfix"></div>
                <div class="col-md-12">
                  <div class="lineSeparator"></div>
                  <br><br>
                </div>
              @endif
              @if ($user->rol == 'executive')
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="input-group">
                      <div class="disableAddon">Estado:</div>
                      <label class="form-control labelDisable">{{ $otData->status }}</label>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="input-group">
                      <div class="disableAddon">Encargado:</div>
                      <label class="form-control labelDisable">{{ $nameUser }}</label>
                    </div>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="input-group">
                      <div class="disableAddon">Fecha calendario:</div>
                      <label class="form-control labelDisable">{{ $otDataTime['date_assigned'] }}</label>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="input-group">
                      <div class="disableAddon">Horas programadas:</div>
                      <label class="form-control labelDisable">{{ $otDataTime['hours'] }}</label>
                    </div>
                  </div>
                </div>
              @endif
              <div class="clearfix"></div>
              <div class="col-md-6">
                <div class="form-group">
                  <div class="input-group {{ $errors->has('nameOt') ? ' has-error' : '' }}">
                    @if ($user->rol == 'superadmin')
                      <div class="input-group-addon">Nombre:</div>
                        <input type="text" name="nameOt" class="form-control" id="nameOt" placeholder="" value="{{ $otData->name }}">
                    @else
                      <div class="disableAddon">Nombre:</div>
                        <label class="form-control labelDisable">{{ $otData->name }}</label>
                    @endif
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group {{ $errors->has('tentativeDateOT') ? ' has-error' : '' }}">
                  <div class="input-group date"  id='datetimepicker2'>
                    @if ($user->rol == 'superadmin')
                      <div class="input-group-addon">Fecha tentativa de entrega:</div>
                    @else
                      <div class="disableAddon">Fecha tentativa de entrega:</div>
                    @endif
                    @if ($user->rol == 'superadmin')
                      <input type="text" name="tentativeDateOT" class="form-control" id="tentativeDateOT" placeholder="" value="{{ $otData->tentative_date }}">
                      <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                    @else
                      <label class="form-control labelDisable">{{ $otData->tentative_date }}</label>
                    @endif
                  </div>
                </div>
                @if ($user->rol == 'superadmin')
                  <script type="text/javascript">
                      $(function () {
                          $('#datetimepicker2').datetimepicker({
                              "format": "YYYY-MM-DD"
                          });
                      });
                  </script>
                @endif
              </div>
              <div class="clearfix"></div>
              <div class="col-md-6">
                <div class="form-group  {{ $errors->has('priorityOT') ? ' has-error' : '' }}">
                  <div class="input-group">
                    @if ($user->rol == 'superadmin')
                      <div class="input-group-addon">Prioridad:</div>
                    @else
                      <div class="disableAddon">Prioridad:</div>
                    @endif
                    @if ($user->rol == 'superadmin')
                      <div class="select-style">
                        <select class="form-control" name="priorityOT" id="priorityOT"> 
                          <option value="">--Seleccionar--</option>
                          @if ($otData->priority == "baja")
                            <option value="baja" selected="selected">Baja</option>
                            <option value="media">Media</option>
                            <option value="alta">Alta</option>
                          @elseif ($otData->priority == "media")
                            <option value="baja">Baja</option>
                            <option value="media" selected="selected">Media</option>
                            <option value="alta">Alta</option>
                          @else
                            <option value="baja">Baja</option>
                            <option value="media">Media</option>
                            <option value="alta" selected="selected">Alta</option>
                          @endif
                        </select>
                      </div>
                    @else
                      <label class="form-control labelDisable">{{ $otData->priority }}</label>
                    @endif
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group {{ $errors->has('complexityOT') ? ' has-error' : '' }}">
                  <div class="input-group">
                    @if ($user->rol == 'superadmin')
                      <div class="input-group-addon">Complejidad de la tarea:</div>
                    @else
                      <div class="disableAddon">Complejidad de la tarea:</div>
                    @endif
                    @if ($user->rol == 'superadmin')
                      <div class="select-style">
                        <select class="form-control" name="complexityOT" id="complexityOT"> 
                          <option value="">--Seleccionar--</option>
                          @if ($otData->complexity == "baja")
                            <option value="baja" selected="selected">Baja</option>
                            <option value="media">Media</option>
                            <option value="alta">Alta</option>
                          @elseif ($otData->complexity == "media")
                            <option value="baja">Baja</option>
                            <option value="media" selected="selected">Media</option>
                            <option value="alta">Alta</option>
                          @else
                            <option value="baja">Baja</option>
                            <option value="media">Media</option>
                            <option value="alta" selected="selected">Alta</option>
                          @endif
                          
                        </select>
                      </div>
                    @else
                      <label class="form-control labelDisable">{{ $otData->complexity }}</label>
                    @endif
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-md-6">
                <div class="form-group {{ $errors->has('otType') ? ' has-error' : '' }}">
                  <div class="input-group">
                    @if ($user->rol == 'superadmin')
                      <div class="input-group-addon">Tipo de OT:</div>
                    @else
                      <div class="disableAddon">Tipo de OT:</div>
                    @endif
                    @if ($user->rol == 'superadmin')
                      <div class="select-style">
                        <select class="form-control" name="otType" id="otType"> 
                          <option value="">--Seleccionar--</option>
                          @foreach ($otTypes as $otType)
                            @if ($otType->id == $otData->id_ot_type)
                              <option value="{{ $otType->id }}" selected="selected" data-ot="{{ $otType->name }}">{{ $otType->name }}</option>
                            @else
                                <option value="{{ $otType->id }}" data-ot="{{ $otType->name }}">{{ $otType->name }}</option>
                            @endif
                          @endforeach
                        </select>
                      </div>
                    @else
                      @foreach ($otTypes as $otType)
                        @if ($otType->id == $otData->id_ot_type)
                          <label class="form-control labelDisable">{{ $otType->name }}</label>
                        @endif
                      @endforeach
                    @endif
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>                    
              <div class="col-md-12">
                <div class="form-group" {{ $errors->has('descriptionOT') ? ' has-error' : '' }}">
                  @if ($user->rol == 'superadmin')
                    <label for="exampleInputEmail1">Descripción:</label>
                  @else
                    <div class="disableAddon">Descripción:</div>
                  @endif
                  
                  @if ($user->rol == 'superadmin')
                    <textarea class="form-control ckeditor" name="descriptionOT" id="descriptionOT">{{ $otData->description }}</textarea>
                  @else
                    <div class="labelDisable">
                      {!! $otData->description !!}
                    </div>
                  @endif
                </div>
              </div>
              <div class="clearfix"></div>                    
              <div class="col-md-12">
                <div class="form-group" {{ $errors->has('recommendationsOT') ? ' has-error' : '' }}">
                  @if ($user->rol == 'superadmin')
                    <label for="exampleInputEmail1">Recomendaciones del cliente:</label>
                  @else
                    <div class="disableAddon">Recomendaciones del cliente:</div>
                  @endif
                  @if ($user->rol == 'superadmin')
                    <textarea class="form-control" name="recommendationsOT" id="recommendationsOT">{{ $otData->recommendations }}</textarea>
                  @else
                    <div class="labelDisable">{{ $otData->recommendations }}</div>
                  @endif
                </div>
              </div>
            </form>
            <div class="clearfix"></div>
            <div class="col-md-12">
              <div class="panel panel-primary" id="renderMetaData">
                <div class="panel-heading">Información adicional</div>
                <div class="panel-body">
                  <input type="hidden" id="metaDataOT" value="{{ $otData->metadata }}">
                  <div class="displayMetadata">
                  </div>
                  <script type="text/javascript">
                    loadMetadataInfo();
                  </script>
                </div>
              </div>
              <div class="comments">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne" style="background-color: #155263; color: #fff;">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_1">
                          Comentarios
                        </a>
                      </h4>
                    </div>
                    <div id="collapse_1" class="panel-collapse collapse in">
                      <div class="panel-body">
                        @foreach ($comments as $comment)
                          <div class="comment">
                            <h2>{{ $comment->name_user }}</h2>
                            <p>
                              {{ $comment->comments }}
                            </p>
                            <p class="date">
                              {{ $comment->updated_at }}
                            </p>
                          </div>
                        @endforeach

                        <br><br>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <textarea class="form-control" rows="3" name="commentsByUser" id="commentsByUser" placeholder="Ingresa tus comentarios..."></textarea>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                          <div class="col-md-11">
                            <div class="checkbox text-right" style="margin-top: 5px">
                              <label>
                                <input type="checkbox" id="checkNotifyComments"> Notificar comentario por email
                              </label>
                            </div>
                          </div>
                          <div class="col-md-1">
                            <a class="btn btn-default btnSiteOrange btnSubmitCommentsUser" href="#" role="button"><i class="fa fa-commenting" aria-hidden="true"></i></a>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="panel panel-primary" id="mainDisplayFormFields">
                <div class="panel-heading"></div>
                <div class="panel-body">
                  <form action="#" id="formDinamicOT">
                    <div class="displayFormDinamic"></div>
                  </form>
                </div>
              </div>
            </div>
          </div> 
        </div>
        <div role="tabpanel" class="tab-pane" id="documentsOT">
          <div class="row mainDocuments">

            <div class="col-md-12">
              <div class="subTitle">
                DOCUMENTOS <strong>COMPLEMENTARIOS</strong>
              </div>
              <div class="lastDoc">
                
              </div>
              <div class="displayDataform">
              </div>

              <form method="post" action="/projects/uploadfile/{{ $idUser }}/{{ $idProject }}/{{ $otData->id }}" accept-charset="UTF-8" enctype="multipart/form-data" id="formUploadFile">
                
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                <input type="hidden" name="origin" value="pro" />
                <div class="form-group">
                  <div class="col-md-8">
                    <input type="file" class="btn btn-default btnSite" name="file[]" id="file" data-multiple-caption="{count} files selected" multiple >
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-4">
                    <button type="submit" class="btn btn-default btnSiteOrange">
                      <i class="fa fa-upload" aria-hidden="true"></i> Subir archivos 
                    </button>
                  </div>
                </div>
              </form>
          </div>
        </div>
      </div>
      <div class="col-md-12">
          <br><br>
          <div class="row">
            @if ($user->rol == 'superadmin')
              <div class="col-md-3">
                  <a class="btn btn-default btnSiteDanger" href="{{ route('deleteOT', [$otData->id_project, $otData->id ]) }}" role="button">
                    <i class="fa fa-frown-o" aria-hidden="true"></i> ELIMINAR
                  </a>
              </div>
              <div class="col-md-3 col-md-offset-0">
            @else
              @if ($otData->status == "completa" || $otData->status == "cancelada")
                <div class="col-md-3 col-md-offset-3">
              @else
                <div class="col-md-3 col-md-offset-6">
              @endif
            @endif
                  <a class="btn btn-default btnSite" href="{{ route('showproject', $idProject) }}" role="button">
                    <i class="fa fa-chevron-left" aria-hidden="true"></i> REGRESAR
                  </a>
              </div>
              @if ($otData->status == "completa" || $otData->status == "cancelada")
                <div class="col-md-3">
                    <a class="btn btn-default btnSiteDanger btnLoadMaskAdjustOT" href="#" role="button">
                      <i class="fa fa-frown-o" aria-hidden="true"></i> AJUSTES
                    </a>
                </div>
              @endif

              <div class="col-md-3">
                  <a class="btn btn-default btnSiteOrange" href="#" role="button" id="btnEditOt">ACTUALIZAR <i class="fa fa-check"></i></a>
              </div>
              <br><br><br>
          </div>
      </div>
    </div>
    </div>
</div>
@endsection


@section('scripts')
  <script src="{{ asset('/vendors/ckeditor/ckeditor.js') }}"></script>
  <script src="{{ asset('js/vendor/jquery.auto-complete.js') }}"></script>
  <script src="{{ asset('js/vendor/jquery.ui.widget.js') }}"></script>
  <script src="{{ asset('js/upload/tmpl.min.js') }}"></script>
  <script src="{{ asset('js/upload/load-image.all.min.js') }}"></script>
  <script src="{{ asset('js/upload/canvas-to-blob.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/upload/jquery.blueimp-gallery.min.js') }}"></script>
  <script src="{{ asset('js/upload/jquery.iframe-transport.js') }}"></script>
  <script src="{{ asset('js/upload/jquery.fileupload.js') }}"></script>
  <script src="{{ asset('js/upload/jquery.fileupload-process.js') }}"></script>
  <script src="{{ asset('js/upload/jquery.fileupload-image.js') }}"></script>
  <script src="{{ asset('js/upload/jquery.fileupload-audio.js') }}"></script>
  <script src="{{ asset('js/upload/jquery.fileupload-video.js') }}"></script>
  <script src="{{ asset('js/upload/jquery.fileupload-validate.js') }}"></script>
  <script src="{{ asset('js/upload/jquery.fileupload-ui.js') }}"></script>
  <script src="{{ asset('js/upload/mainUpload.js') }}"></script>


  <!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
  <!--[if (gte IE 8)&(lt IE 10)]>
  <script src="{{ asset('js/cors/jquery.xdr-transport.js') }}"></script>
  <![endif]-->
  <script type="text/javascript" src="{{ asset('js/datepicker/moment.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/datepicker/bootstrap-datetimepicker.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/vendor/Chart.js') }}"></script>

  <script type="text/javascript">
    $(document).ready(function(evt){
      $("#idAccount").on("change", function(evt){
        var nameClient = $(this).find(':selected').data('client');
        $("#nameClientSelected").val(nameClient);
      });

      $('#advancedUserName').autoComplete({
          minChars: 0,
          source: function(term, suggest) {
              term = term.toLowerCase();
              var choices = [
                  @foreach ($users as $userGlobal)
                    ['{{ $userGlobal->name }}', '{{ $userGlobal->id }}'],
                  @endforeach
              ];
              var suggestions = [];
              for (i = 0; i < choices.length; i++)
                  if (~(choices[i][0] + ' ' + choices[i][1]).toLowerCase().indexOf(term)) suggestions.push(choices[i]);
              suggest(suggestions);
          },
          renderItem: function(item, search) {
              search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
              var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
              return '<div class="autocomplete-suggestion" data-langname="' + item[0] + '" data-lang="' + item[1] + '" data-val="' + search + '"> ' + item[0].replace(re, "<b>$1</b>") + '</div>';
          },
          onSelect: function(e, term, item) {
              // console.log('Item "' + item.data('langname') + ' (' + item.data('lang') + ')" selected by ' + (e.type == 'keydown' ? 'pressing enter or tab' : 'mouse click') + '.');
              $('#advancedUserName').val(item.data('langname'));
              $('#userAsignOt').val(item.data('lang'));
          }
      });



    });
  </script>

@endsection






