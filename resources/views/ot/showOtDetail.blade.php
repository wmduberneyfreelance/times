@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/upload/blueimp-gallery.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/upload/jquery.fileupload.css') }}">
    <link rel="stylesheet" href="{{ asset('css/upload/jquery.fileupload-ui.css') }}">
    <!-- CSS adjustments for browsers with JavaScript disabled -->
    <noscript><link rel="stylesheet" href="{{ asset('css/upload/jquery.fileupload-noscript.css') }}"></noscript>
    <noscript><link rel="stylesheet" href="{{ asset('css/upload/jquery.fileupload-ui-noscript.css') }}"></noscript>
@endsection

@section('content')
  <div class="maskBigmail">
    <div id="email-preview"></div>
  </div>
  <div class="maskDoneOT">
    <div class="formDoneOT">
      <div class="container-fluid">
        <form action="{{ route('editotstatusdone') }}" class="" method="POST" id="updateFormDoneOT">
          {!! csrf_field() !!}
          <input type="hidden" value="{{ $otData[0]->id }}" name="idOtAdjust" id="idOtAdjust">
          <input type="hidden" value="{{ $otData[0]->id_project }}" name="idProject" id="idProject">
          <input type="hidden" value="{{ $idUser }}" name="idUser" id="idUser">
          <input type="hidden" value="{{ $otData[0]->id }}" name="idOT" id="idOT">
          <input type="hidden" value="{{ route('publishComments') }}" name="urlPublishComments" id="urlPublishComments">
          <input type="hidden" value="{{ route('deleteFileByOt') }}" name="urlDeleteFile" id="urlDeleteFile">


          <div class="row">
            <div class="col-md-12">
              <div class="textWelcome text-center">
                TERMINAR <strong>ORDEN DE TRABAJO</strong>
              </div>
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="col-md-5">
              <img src="{{ asset("img/gifs/211df5b35c7f7e9e66dcc698919959e4.gif") }}" class="img-responsive center-block">
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <textarea class="form-control" rows="4" name="commentsAdjust" id="commentsAdjust" placeholder="Tienes comentarios de esta OT..."></textarea>
              </div>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
              <a class="btn btn-default btnSite btnCloseMaskDoneOT" href="#" role="button"> <i class="fa fa-chevron-left" aria-hidden="true"></i> REGRESAR </a>
            </div>
            <div class="col-md-6">
              <a class="btn btn-default btnSiteOrange" href="#" role="button" id="btnDoneOT">COMPLETAR <i class="fa fa-check-square-o" aria-hidden="true"></i></a>
            </div>
            <br><br><br>
          </div>
        </form>
      </div>
    </div>  
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-3 mainLeftZone">
        @include('partials.leftZonePercentage')
      </div>
      <div class="col-md-9 mainRightZone">
        <div class="textWelcome">
          <strong>PROYECTO: </strong> {{ $otData[0]->nameProject }}
        </div>

        <br><br>
        @if (Session::has('message'))
          <p class="alert alert-success">
            {!! Session::get('message') !!}
          </p>
        @endif

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active">
            <a href="#generalDataOT" aria-controls="generalDataOT" role="tab" data-toggle="tab">General</a>
          </li>
          <li role="presentation">
            <a href="#documentsOT" aria-controls="documentsOT" role="tab" data-toggle="tab">Documentos</a>
          </li>
          @if($otData[0]->bigmail == 1)
            <li role="presentation">
              <a href="#bigmailOT" aria-controls="bigmailOT" role="tab" data-toggle="tab">Tracker</a>
            </li>
          @endif
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="generalDataOT">
            <div class="row">
              <div class="col-md-12">
                @if ($user->rol == 'superadmin' || $user->rol == 'developer')
                  <label class="switch">
                    <input type="hidden" name="urlAjaxBigmail" id="urlAjaxBigmail" value="{{ route('setbigmail') }}">
                    <input type="hidden" name="valueIsBigMail" id="valueIsBigMail" value="{{ $otData[0]->bigmail }}">
                    @if($otData[0]->bigmail == 0)
                      <input type="checkbox" name="isBigMail" id="isBigMail">
                      <div class="slider round"></div>
                      <span class="text">ACTIVAR TRACKER</span>
                    @else
                      <input type="checkbox" checked="checked" name="isBigMail" id="isBigMail">
                      <div class="slider round"></div>
                      <span class="text">DESACTIVAR TRACKER</span>
                    @endif
                    
                    <div class="loader">
                      <div class="la-ball-scale-ripple-multiple la-dark">
                        <div></div>
                        <div></div>
                        <div></div>
                      </div>
                    </div>
                  </label>
                @endif
                
                <div class="subTitle">Información <strong>general</strong></div>

                <div class="grayZone">
                  <h2><strong>Nombre:</strong>  00{{ $otData[0]->id }} - {{ $otData[0]->name }}</h2>
                  <p>
                    <strong>Tipo de OT:</strong>  {{ $otData[0]->nameTypeOT }}<br>
                    <strong>Cliente:</strong>  {{ $otData[0]->nameClient }}<br>
                    <strong>Cuenta:</strong>  {{ $otData[0]->nameAccount }}<br>
                    <strong>Fecha tentativa de entrega: </strong> {{ $otData[0]->tentative_date }} <br>
                    <strong>Ejecutivo:</strong>  {{ $executive->name }}<br>
                  </p>
                </div>
                <h3 class="blueTitle">Descripción:</h3>
                <div class="panel panel-default">
                  <div class="panel-body withEditor">
                    {!! $otData[0]->description !!}
                  </div>
                </div>
                <h3 class="blueTitle">Recomendaciones del cliente:</h3>
                <p>
                  {!! $otData[0]->recommendations !!}
                </p>
                <br><br>
                <div class="panel panel-primary panel-gray" id="renderMetaData">
                  <div class="panel-heading">Información adicional</div>
                  <div class="panel-body">
                    <input type="hidden" id="metaDataOT" value="{{ $otData[0]->metadata }}">
                    <div class="displayMetadata">
                    </div>
                    <script type="text/javascript">
                      loadMetadataInfo();
                    </script>
                  </div>
                </div>

                <div class="comments">
                  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                      <div class="panel-heading" role="tab" id="headingOne" style="background-color: #4d4d4d; color: #fff;">
                        <h4 class="panel-title">
                          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_1">
                            Comentarios
                          </a>
                        </h4>
                      </div>
                      <div id="collapse_1" class="panel-collapse collapse in">
                        <div class="panel-body">
                          @foreach ($comments as $comment)
                            <div class="comment">
                              <h2>{{ $comment->name_user }}</h2>
                              <p>
                                {{ $comment->comments }}
                              </p>
                              <p class="date">
                                {{ $comment->updated_at }}
                              </p>
                            </div>
                          @endforeach
                          <br><br>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <textarea class="form-control" rows="3" name="commentsByUser" id="commentsByUser" placeholder="Ingresa tus comentarios..."></textarea>
                              </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-11">
                              <div class="checkbox text-right" style="margin-top: 5px">
                                <label>
                                  <input type="checkbox" id="checkNotifyComments"> Notificar comentario por email
                                </label>
                              </div>
                            </div>
                            <div class="col-md-1">
                              <a class="btn btn-default btnSiteOrange btnSubmitCommentsUser" href="#" role="button"><i class="fa fa-commenting" aria-hidden="true"></i></a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="documentsOT">
            <div class="row mainDocuments">
              <div class="col-md-12">
                <div class="subTitle">
                  DOCUMENTOS <strong>COMPLEMENTARIOS</strong>
                </div>
                <div class="lastDoc"></div>
                <div class="displayDataform">
                </div>


                <form method="post" action="/projects/uploadfile/{{ $idUser }}/{{ $otData[0]->id_project }}/{{ $otData[0]->id }}" accept-charset="UTF-8" enctype="multipart/form-data" id="formUploadFile">
                  
                  <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                  <input type="hidden" name="origin" value="ot" />
                  <div class="form-group">
                    <div class="col-md-8">
                      <input type="file" class="btn btn-default btnSite" name="file[]" id="file" data-multiple-caption="{count} files selected" multiple >
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-md-4">
                      <button type="submit" class="btn btn-default btnSiteOrange">
                        <i class="fa fa-upload" aria-hidden="true"></i> Subir archivos 
                      </button>
                    </div>
                  </div>
                </form>

              </div>
              <div class="clearfix"></div>
              <div class="col-md-12">
                <div class="table-responsive" id="filesByOtUser">
                </div>
              </div>
            </div>
          </div>
          @if($otData[0]->bigmail == 1)
            <div role="tabpanel" class="tab-pane" id="bigmailOT">
              <div class="row">
                <div class="col-md-12">
                  <input type="hidden" value="1" id="activeBigMail" name="activeBigMail">
                  <input type="hidden" id="urlUnzipBigmail" name="urlUnzipBigmail" value="{{ route('unzipfilesbyot') }}">

                  <div class="subTitle">
                    Bienvenido a <strong>TRACKER</strong>
                  </div>
                  <div class="panel panel-default panelTracking">
                    <div class="panel-heading">
                      <h3 class="panel-title">Registro de actividades:</h3>
                    </div>
                    <div class="panel-body">
                      <div class="container-fluid">
                        <div class="row">
                          <br><br>
                          <div class="col-md-8">
                            <form>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Descripción de la tarea</label>
                                <textarea class="form-control textAreaTask" id="textTaskHere"></textarea>
                                <input type="hidden" name="urlAjaxTracker" id="urlAjaxTracker" value="{{ route('settracker') }}">
                              </div>
                            </form>
                          </div>
                          <div class="col-md-4">
                            <div id="timer">
                              <div class="reloj">
                                <div id="hour">00</div>
                                <div class="divider">:</div>
                                <div id="minute">00</div>
                                <div class="divider">:</div>
                                <div id="second">00</div>
                              </div>
                              <button id="btn-comenzar">Comenzar</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <br><br>
                      <input type="hidden" id="timeCurrentTracker" value="{{ $trackerOpen }}">
                      <table class="table table-striped">
                        <tr>
                          <th>Tarea</th>
                          <th>Tiempo</th>
                          <th>Estado</th>
                        </tr>
                        @foreach ($trackers as $tracker)
                        <tr>
                            <td>{{ $tracker->message }}</td>
                            <td>{{ $tracker->time }}</td>
                            <td>
                              @if($tracker->open == 1)
                                Abierta
                              @else
                                Cerrada
                              @endif
                              
                            </td>
                        </tr>
                        @endforeach
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @endif          
        </div>

        <div class="col-md-12">
          <br><br>
          <div class="row">
            <div class="col-md-3 col-md-offset-6">
              <a class="btn btn-default btnSite" href="{{ route('loadots') }}" role="button"> <i class="fa fa-chevron-left" aria-hidden="true"></i> REGRESAR </a>
            </div>
            <div class="col-md-3">
              <a class="btn btn-default btnSiteOrange btnLoadMaskDoneOT" href="#" role="button">COMPLETAR <i class="fa fa-check-square-o" aria-hidden="true"></i></a>
            </div>
            <br><br><br>
          </div>
        </div>
    </div>
  </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/vendor/jquery.ui.widget.js') }}"></script>
    <script src="{{ asset('js/upload/tmpl.min.js') }}"></script>
    <script src="{{ asset('js/upload/load-image.all.min.js') }}"></script>
    <script src="{{ asset('js/upload/canvas-to-blob.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/upload/jquery.blueimp-gallery.min.js') }}"></script>
    <script src="{{ asset('js/upload/jquery.iframe-transport.js') }}"></script>
    <script src="{{ asset('js/upload/jquery.fileupload.js') }}"></script>
    <script src="{{ asset('js/upload/jquery.fileupload-process.js') }}"></script>
    <script src="{{ asset('js/upload/jquery.fileupload-image.js') }}"></script>
    <script src="{{ asset('js/upload/jquery.fileupload-audio.js') }}"></script>
    <script src="{{ asset('js/upload/jquery.fileupload-video.js') }}"></script>
    <script src="{{ asset('js/upload/jquery.fileupload-validate.js') }}"></script>
    <script src="{{ asset('js/upload/jquery.fileupload-ui.js') }}"></script>
    <script src="{{ asset('js/upload/mainUpload.js') }}"></script>
    <!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
    <!--[if (gte IE 8)&(lt IE 10)]>
    <script src="{{ asset('js/cors/jquery.xdr-transport.js') }}"></script>
    <![endif]-->
    <script type="text/javascript" src="{{ asset('js/datepicker/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/datepicker/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/Chart.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bigmail.js') }}"></script>
    <script type="text/javascript">
      loadChart();
    </script>

@endsection


