@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-2 mainLeftZone">
      @include('partials.leftZoneCalendar')
    </div>
    <div class="col-md-10 mainRightZone">
      <div class="textWelcome">
      	ORDENES <strong>DE TRABAJO</strong>
      </div>
      <br>

      @if (Session::has('message'))
        <p class="alert alert-success">
					{!! Session::get('message') !!}
        </p>
      @endif
            

      <form class="" action="{{ route('loadprojects') }}" method="GET" role="search">
        <div class="row">
          <div class="col-md-6">
            <label class="switch pull-left">
              {{-- <input type="hidden" name="urlAjaxBigmail" id="urlAjaxBigmail" value="{{ route('setbigmail') }}">
              <input type="hidden" name="valueIsBigMail" id="valueIsBigMail" value="{{ $otData[0]->bigmail }}">
              @if($otData[0]->bigmail == 0)
                <input type="checkbox" checked="checked" name="historyUser" id="historyUser">
              @else --}}
                <input type="checkbox" name="historyUser" id="historyUser">
                
              {{-- @endif --}}
              <div class="slider round"></div>
              <span class="text2">Histórico OTS</span>
              <div class="loader">
                <div class="la-ball-scale-ripple-multiple la-dark">
                  <div></div>
                  <div></div>
                  <div></div>
                </div>
              </div>
            </label>
          </div>
          <div class="col-md-6">
            <div class="form-inline pull-right">
              <div class="form-group">
                <input type="text" class="form-control" id="searchNameProject" name="searchNameProject" placeholder="Buscar orden de trabajo" value="{{ Request::get('searchNameProject') }}">
              </div>
              <button type="submit" class="btn btn-default btnSite2"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div>
      </form>

        <form class="">
          <br>
          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table table-hover">
                    <tr>
                        <th width="50">Estado</th>
                        <th>Nombre</th>
                        <th>Cuenta</th>
                        <th>Tipo de OT</th>
                        <th>Fecha de entrega</th>
                        <th width="20"></th>
                    </tr>
                     @foreach ($otsData as $otData)
                        <tr>
                            <td><div class="statusProject {{ $otData->status }}"></div></td>
                            <td>
                              <a href="{{ route('loadotdetail', $otData->id) }}" class="linkSite">
                                00{{ $otData->id }} - {{ $otData->name }}
                              </a>
                            </td>
                            <td>{{ $otData->nameAccount }}</td>
                            <td>{{ $otData->nameTypeOT }}</td>
                            <td>{{ $otData->tentative_date }}</td>
                            <td>
                              <a href="{{ route('loadotdetail', $otData->id) }}" class="linkSite">
                                <i class="fa fa-eye" aria-hidden="true"></i>
                              </a>
                            </td>
                        </tr>
                    @endforeach
                </table>
              </div>
              <div class="pagination">
                {{ $otsData->appends(Request::all())->render() }}
              </div>
          </div>
          </div>
        </form>
        <form action="{{ route('destroyprofile', ':USER_ID') }}" class="" method="POST" id="deleteFormProfile">
            {!! csrf_field() !!}
        </form>
        </div>
</div>
@endsection


@section('scripts')
    <script type="text/javascript" src="{{ asset('js/datepicker/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/datepicker/bootstrap-datetimepicker.js') }}"></script>
@endsection

