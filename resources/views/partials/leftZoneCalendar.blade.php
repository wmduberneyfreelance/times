<a href="{{ route('calendar') }}" class="currentDay">
    <i class="material-icons">perm_contact_calendar</i> HOY {{ $today }} <br>
    <strong class="big">{{ $numberDay }}</strong>
    <strong>{{ $month }}</strong>
    {{ $year }}
</a>
