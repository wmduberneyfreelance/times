<div class="row">
  <div class="col-md-6 noPadding">
    <table class="tableStatusPro">
        <tr>
            <td width="30"><div class="statusProject new"></div></td>
            <td> Nuevo</td>
        </tr>
        <tr>
            <td><div class="statusProject complet"></div></td>
            <td> Completo</td>
        </tr>
        <tr>
          <td><div class="statusProject ajustes"></div></td>
          <td> Ajustes</td>
        </tr>
    </table>
  </div>
  <div class="col-md-6 noPadding">
    <table class="tableStatusPro">
        <tr>
            <td width="30"><div class="statusProject trafic"></div></td>
            <td> En tráfico</td>
        </tr>
        <tr>
            <td><div class="statusProject cancel"></div></td>
            <td> Cancelado</td>
        </tr>
    </table>
  </div>
  <div class="clearfix"></div>
  <div class="col-md-8 col-md-offset-2">
    <div class="mainDisplayPercentage">
      <br>
      <canvas id="myChart" width="150" height="150"></canvas>
      <div class="percentage"></div>
      <div class="textStatusP">
        Progreso <br><strong>del proyecto</strong>
      </div>
      <input type="hidden" value="{{ json_encode($dataStatusOTS) }}" id="mainDataStatusOT">
      <script type="text/javascript">
        $(document).ready(function(evt){
          loadChart();
        });
      </script>
    </div>
  </div>
</div>
<br>