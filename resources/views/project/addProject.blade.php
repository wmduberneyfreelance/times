@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-3 mainLeftZone">
        <div class="row">
          <div class="col-md-6 noPadding">
            <table class="tableStatusPro">
              <tr>
                <td width="30"><div class="statusProject new"></div></td>
                <td> Nuevo</td>
              </tr>
              <tr>
                <td><div class="statusProject complet"></div></td>
                <td> Completo</td>
              </tr>
            </table>
          </div>
          <div class="col-md-6 noPadding">
            <table class="tableStatusPro">
              <tr>
                <td width="30"><div class="statusProject trafic"></div></td>
                <td> En tráfico</td>
              </tr>
              <tr>
                <td><div class="statusProject cancel"></div></td>
                <td> Cancelado</td>
              </tr>
            </table>
          </div>
        </div>
        <br>
      </div>
      <div class="col-md-9 mainRightZone">
        <div class="textWelcome">
          NUEVO <strong>PROYECTO</strong>
        </div>

        <form action="{{ route('saveproject') }}" class="" method="POST" id="saveFormProject">
          {!! csrf_field() !!}
          <br>
          
          @include('admin.partials.messages')

          <div class="row">
            <div class="col-md-6">
              <div class="form-group {{ $errors->has('idAccount') ? ' has-error' : '' }}">
                <label class="sr-only" for="exampleInputAmount">Cuenta:</label>
                <div class="input-group">
                  <div class="input-group-addon">Cuenta:</div>
                  <div class="select-style">
                    <select class="form-control" name="idAccount" id="idAccount"> 
                      <option value="">Seleccionar</option>
                      @foreach ($accounts as $account)
                          <option value="{{ $account->id }}" data-client="{{ $account->nameClient }}">{{ $account->name }}</option>
                      @endforeach
                    </select>
                  </div>
                  </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group {{ $errors->has('idClient') ? ' has-error' : '' }}">
                <label class="sr-only" for="exampleInputAmount">Cliente:</label>
                <div class="input-group">
                  <div class="input-group-addon">Cliente:</div>
                  <input type="text" name="nameClientSelected" class="form-control" id="nameClientSelected" readonly="true">
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="sr-only" for="exampleInputAmount">Nombre:</label>
                <div class="input-group {{ $errors->has('nameProject') ? ' has-error' : '' }}">
                  <div class="input-group-addon">Nombre</div>
                  <input type="text" name="nameProject" class="form-control" id="nameProject" placeholder="" value="{{ old('nameProject') }}">
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="sr-only" for="exampleInputAmount">Fecha tentativa de entrega::</label>
                <div class="input-group date {{ $errors->has('tentativeDateProject') ? ' has-error' : '' }}"  id='datetimepicker1'>
                  <div class="input-group-addon">Fecha tentativa de entrega:</div>
                  <input type="text" name="tentativeDateProject" class="form-control" id="tentativeDateProject" placeholder="" value="{{ old('tentativeDateProject') }}">
                  <span class="input-group-addon">
                      <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
              <script type="text/javascript">
                $(function () {
                  $('#datetimepicker1').datetimepicker({
                    "format": "YYYY-MM-DD"
                  });
                });
              </script>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
              <br><div class="lineSeparator"></div><br>
              <div class="row">
                <div class="col-md-3 col-md-offset-6">
                  <a class="btn btn-default btnSite" href="{{ route('loadprojects') }}" role="button">CANCELAR <i class="fa fa-times"></i></a>
                </div>
                <div class="col-md-3">
                  <a class="btn btn-default btnSiteOrange" href="#" role="button" id="btnSaveProject">GUARDAR <i class="fa fa-check"></i></a>
                </div>
                <br><br><br>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection


@section('scripts')
    <script type="text/javascript" src="{{ asset('js/datepicker/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/datepicker/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(evt){
            $("#idAccount").on("change", function(evt){
                var nameClient = $(this).find(':selected').data('client');
                $("#nameClientSelected").val(nameClient);
            });
        });
    </script>
@endsection






