@extends('layouts.app')

@section('content')
  <div class="maskPiecesTypes">
    <div class="formPieceType">
      <div class="container-fluid">
        <form action="{{ route('addPieceByProject') }}" class="" method="POST" id="addFormPieceByProject">
          {!! csrf_field() !!}
          <input type="hidden" value="{{ $project->id }}" name="idProjectPieceType" id="idProjectPieceType">
          <div class="row">
            <div class="col-md-12">
              <div class="textWelcome text-center">
                AGREGAR <strong>PIEZA</strong>
              </div>
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="col-md-6">
              <div class="form-group {{ $errors->has('idPieceType') ? ' has-error' : '' }}">
                <div class="input-group">
                  <div class="input-group-addon">Pieza:</div>
                  <div class="select-style">
                    <select class="form-control" name="idPieceType" id="idPieceType"> 
                      <option value="">Seleccionar</option>
                      @foreach ($piecesTypes as $pieceType)
                        <option value="{{ $pieceType->id }}">{{ $pieceType->name }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="sr-only" for="exampleInputAmount">Nombre:</label>
                <div class="input-group {{ $errors->has('namePiece') ? ' has-error' : '' }}">
                  <div class="input-group-addon">Nombre</div>
                  <input type="text" name="namePiece" class="form-control" id="namePiece" placeholder="" value="" maxlength="80">
                </div>
              </div>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
              <a class="btn btn-default btnSite btnCloseMaskPiece" href="#" role="button"> <i class="fa fa-times" aria-hidden="true"></i> CANCELAR </a>
            </div>
            <div class="col-md-6">
              <a class="btn btn-default btnSiteOrange" href="#" role="button" id="btnAddPieceByProject">AGREGAR <i class="fa fa-plus"></i></a>
            </div>
            <br><br><br>
          </div>
        </form>
      </div>
    </div>  
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-3 mainLeftZone">
        @include('partials.leftZonePercentage')
      </div>
      <div class="col-md-9 mainRightZone">
        <div class="textWelcome upperCase">
          <strong>Proyecto:</strong> {{ $project->name }}
        </div>

        <div class="progress progressProjects">
          <div class="progress-bar progress-bar-asignada" style="width: 33.33%">
            <span class="sr-only">35% Complete (success)</span>
          </div>
          <div class="progress-bar progress-bar-completa" style="width: 33.33%">
            <span class="sr-only">20% Complete (warning)</span>
          </div>
          <div class="progress-bar progress-bar-cancelada" style="width: 33.33%">
            <span class="sr-only">10% Complete (danger)</span>
          </div>
        </div>
        <script type="text/javascript">
          iniProgressBar();
        </script>

        <form action="{{ route('editproject', $idProjectUpdate) }}" class="" method="POST" id="editFormProject">
          {!! csrf_field() !!}
          <br>
          <br>
          
          @include('admin.partials.messages')
          @if (Session::has('message'))
            <p class="alert alert-success">
              {!! Session::get('message') !!}
            </p>
          @endif
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="sr-only" for="exampleInputAmount">Nombre:</label>
                  <div class="input-group {{ $errors->has('nameProject') ? ' has-error' : '' }}">
                    <div class="input-group-addon">Nombre</div>
                    <input type="text" name="nameProject" class="form-control" id="nameProject" placeholder="" value="{{ $project->name }}">
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group {{ $errors->has('idAccount') ? ' has-error' : '' }}">
                  <label class="sr-only" for="exampleInputAmount">Cuenta:</label>
                  <div class="input-group">
                    <div class="input-group-addon">Cuenta:</div>
                    <div class="select-style">
                      <select class="form-control" name="idAccount" id="idAccount"> 
                        <option value="">Seleccionar</option>
                        @foreach ($accounts as $account)
                            @if ($account->id === $project->id_account)
                                <option value="{{ $account->id }}" selected="selected" data-client="{{ $account->nameClient }}">{{ $account->name }}</option>
                            @else
                                <option value="{{ $account->id }}" data-client="{{ $account->nameClient }}">{{ $account->name }}</option>
                            @endif
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            	<div class="col-md-6">
            		<div class="form-group {{ $errors->has('idClient') ? ' has-error' : '' }}">
          		    <label class="sr-only" for="exampleInputAmount">Cliente:</label>
          		    <div class="input-group">
          		      <div class="input-group-addon">Cliente:</div>
          		      <input type="text" name="nameClientSelected" class="form-control" id="nameClientSelected" readonly="true" value="{{ $nameClient }}">
          		    </div>
            		</div>
            	</div>
              <div class="clearfix"></div>
              <div class="col-md-6">
                <div class="form-group {{ $errors->has('statusProject') ? ' has-error' : '' }}">
                  <div class="input-group">
                    <div class="input-group-addon">Estado:</div>
                    <div class="select-style">
                      <select class="form-control" name="statusProject" id="statusProject"> 
                        @if ($project->status === 'new')
                          <option value="new" selected="selected">Nuevo</option>
                          <option value="trafic">En tráfico</option>
                          <option value="complet">Completo</option>
                          <option value="cancel">Cancelado</option>
                        @elseif ($project->status === 'trafic')
                          <option value="new">Nuevo</option>
                          <option value="trafic" selected="selected">En tráfico</option>
                          <option value="complet">Completo</option>
                          <option value="cancel">Cancelado</option>
                        @elseif ($project->status === 'complet')
                          <option value="new">Nuevo</option>
                          <option value="trafic">En tráfico</option>
                          <option value="complet" selected="selected">Completo</option>
                          <option value="cancel">Cancelado</option>
                        @elseif ($project->status === 'cancel')
                          <option value="new">Nuevo</option>
                          <option value="trafic">En tráfico</option>
                          <option value="complet">Completo</option>
                          <option value="cancel" selected="selected">Cancelado</option>
                        @endif
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="sr-only" for="exampleInputAmount">Fecha tentativa de entrega::</label>
                  <div class="input-group date {{ $errors->has('tentativeDateProject') ? ' has-error' : '' }}"  id='datetimepicker1'>
                    <div class="input-group-addon">Fecha tentativa de entrega:</div>
                    <input type="text" name="tentativeDateProject" class="form-control" id="tentativeDateProject" placeholder="" value="{{ $project->tentative_date }}">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                  </div>
                </div>
                <script type="text/javascript">
                  $(function () {
                    $('#datetimepicker1').datetimepicker({
                      "format": "YYYY-MM-DD"
                    });
                  });
                </script>
              </div>
              <div class="clearfix"></div>
              <div class="col-md-12">
                <br><br>
                <div class="row">
                  @if ($user->rol == 'superadmin')
                    <div class="col-md-3 col-md-offset-3">
                      <a class="btn btnSiteDanger" href="{{ route('destroyproject', $project->id) }}">
                        <i class="fa fa-trash-o"></i>
                        Eliminar
                      </a>
                    </div>
                    <div class="col-md-3">
                  @else
                    <div class="col-md-3 col-md-offset-6">
                  @endif
                    <a class="btn btn-default btnSite" href="{{ route('loadprojects') }}" role="button"> <i class="fa fa-chevron-left" aria-hidden="true"></i> REGRESAR </a>
                  </div>
                  <div class="col-md-3">
                    <a class="btn btn-default btnSiteOrange" href="#" role="button" id="btnUpdateProject">ACTUALIZAR <i class="fa fa-refresh"></i></a>
                  </div>
                  <br><br><br>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="col-md-12">
                <br><div class="lineSeparator"></div><br>
                <div class="row">
                  <div class="col-md-12">
                    <div class="textWelcome">
                      <strong>CASOS </strong> ASOCIADOS
                    </div>

                    <div class="modal fade" id="modalDeletePieza" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
                      <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content">
                          <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <div class="textWelcome text-center">
                              ELIMINAR <strong>PIEZA</strong>
                            </div>
                            <p class="text-center">Estás apunto de eliminar una pieza ¿Realmente quieres eliminarla?</p>
                          </div>
                          <div class="modal-footer">
                            <a class="btn btn-primary" id="linkDeletePieza" href="#">
                              Eliminar
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                      @foreach ($piecesTypesByProject as $pieceTypeByProject)
                        <?php $countOTPendientes = 0; ?>
                        @foreach ($ots as $ot)
                          @if ($pieceTypeByProject->id === $ot->id_piece_by_project)
                            @if ($ot->status == 'nueva' || $ot->status == 'ajustes')
                              <?php $countOTPendientes++; ?>
                            @endif
                          @endif
                        @endforeach

                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="headingOne" style="background-color: {{ $colors[$iColor] }}; color: #fff;">
                            <h4 class="panel-title">
                              @if ($user->rol == 'superadmin')
                                <div class="pull-left actionPieza">
                                  <i class="fa fa-trash-o" aria-hidden="true" data-toggle="modal" data-target="#modalDeletePieza" data-url="{{ route('deletePieza', [$project->id, $pieceTypeByProject->id ]) }}"></i>
                                  <i class="fa fa-pencil" aria-hidden="true"></i>  
                                </div>
                              @endif
                              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_{{ $pieceTypeByProject->id }}">
                                {{ $pieceTypeByProject->namePiece }} - {{ $pieceTypeByProject->name }}
                                @if ($countOTPendientes > 0)
                                  <span class="label label-big pull-right">{{ $countOTPendientes }}</span>
                                @endif
                              </a>
                            </h4>
                          </div>
                          <div id="collapse_{{ $pieceTypeByProject->id }}" class="panel-collapse collapse">
                            <div class="panel-body">
                              <div class="table-responsive">
                                <table class="table table-hover" border="0">
                                  <tr>
                                    <th width="50">Estado</th>
                                    <th>Nombre</th>
                                    <th width="65">Versión</th>
                                    <th width="115">Fecha aprox.</th>
                                    <th width="20"></th>
                                  </tr>
                                   @foreach ($ots as $ot)
                                    @if ($pieceTypeByProject->id === $ot->id_piece_by_project)
                                      <?php 
                                        $statusName = strtoupper($ot->status);
                                      ?>
                                      <tr>
                                        <td><div class="statusProject {{ $ot->status }}"  data-toggle="tooltip" data-placement="top" title="{{ $statusName }}"></div></td>
                                        <td>
                                          @if (preg_match('/Diseño/', $ot->nameTypeOT) || preg_match('/Animación/', $ot->nameTypeOT))
                                            <i class="material-icons">color_lens</i>
                                          @elseif (preg_match('/Corte/', $ot->nameTypeOT) || preg_match('/Generación/', $ot->nameTypeOT) || preg_match('/Montaje/', $ot->nameTypeOT) || preg_match('/Actualización/', $ot->nameTypeOT) || preg_match('/Maquetación/', $ot->nameTypeOT))
                                            <i class="material-icons">dvr</i>
                                          @elseif (preg_match('/Copy/', $ot->nameTypeOT))
                                            <i class="material-icons">description</i>
                                          @else 
                                            <i class="material-icons">library_books</i>
                                          @endif
                                          

                                          {{-- {{ $ot->nameTypeOT }} --}}
                                          <a href="{{ route('showOt', [$project->id, $ot->id ] ) }}" class="linkSite">
                                            00{{ $ot->id }} - {{ $ot->name }}
                                          </a>
                                        </td>
                                        <td align="center">{{ $ot->version }}</td>
                                        <td>{{ $ot->tentative_date }}</td>
                                        <td>
                                          <a href="{{ route('showOt', [$project->id, $ot->id ] ) }}" class="linkSite">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                          </a>
                                        </td>
                                      </tr>
                                    @endif
                                  @endforeach
                                </table>
                              </div>
                              <div class="col-md-3 col-md-offset-9">
                                <br>
                                <a class="btn btn-default btnSiteOrange" href="{{ route('newOt', [$project->id,$pieceTypeByProject->id]) }}" role="button">
                                    AGREGAR OT <i class="fa fa-plus"></i>
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                        <?php
                          $iColor++;
                          if ($iColor>(count($colors)-1))
                            $iColor = 0;
                        ?>
                      @endforeach
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-md-12 hidden">
                    <div class="textWelcome">
                        <strong>ORDENES DE TRABAJO</strong> ASOCIADAS
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-md-12 hidden">
                    <div class="table-responsive">
                      <table class="table table-hover" border="0">
                        <tr>
                          <th width="50">Estado</th>
                          <th>Nombre</th>
                          <th>Fecha aprox.</th>
                          <th width="20"></th>
                        </tr>
                         @foreach ($ots as $ot)
                            <tr>
                                <td><div class="statusProject {{ $ot->status }}"></div></td>
                                <td>{{ $ot->name }}</td>
                                <td>{{ $ot->tentative_date }}</td>
                                <td>
                                  <a href="{{ route('showOt', [$project->id, $ot->id ] ) }}" class="linkSite">
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                  </a>
                                </td>
                            </tr>
                        @endforeach
                      </table>
                    </div>
                    <div class="pagination">
                        {{ $ots->appends(Request::all())->render() }}
                    </div>
                  </div>
                  <div class="col-md-3 col-md-offset-9">
                    <br>
                    <a class="btn btn-default btnSiteOrange btnLoadMaskPiece" href="#" role="button">
                        AGREGAR PIEZA <i class="fa fa-plus"></i>
                    </a>
                  </div>
                </div>
                <br><br>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/datepicker/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/datepicker/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/Chart.js') }}"></script>

    <script type="text/javascript">
      $(document).ready(function(evt){
        $("#idAccount").on("change", function(evt){
          var nameClient = $(this).find(':selected').data('client');
          $("#nameClientSelected").val(nameClient);
        });
      });
    </script>
@endsection
