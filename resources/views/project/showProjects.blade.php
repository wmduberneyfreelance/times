@extends('layouts.app')

@section('content')

  <?php $accountsArray[""] = "- Cuenta -" ?>
  @foreach ($accounts as $account)
    <?php $accountsArray[$account->id] = $account->name; ?>
  @endforeach

  <div class="container">
    {{-- <form action="{{ route('loadprojects') }}" class="" method="GET"> --}}
    {!! Form::model(Request::all(), [ 'route' => "loadprojects", 'method' => 'GET', 'id' => 'mainFormShowProjects' ]) !!}
    <form action="{{ route('loadprojects') }}" class="" method="GET">
      <div class="row">
        <div class="col-md-3 mainLeftZone">
          <div class="relativeContent">
            <div class="fixedZone">
              <div class="row">
                <div class="col-md-6 noPadding">
                  <table class="tableStatusPro">
                    <tr>
                      <td width="30"><div class="statusProject new"></div></td>
                      <td> Nuevo</td>
                    </tr>
                    <tr>
                      <td><div class="statusProject complet"></div></td>
                      <td> Completo</td>
                    </tr>
                  </table>
                </div>
                <div class="col-md-6 noPadding">
                  <table class="tableStatusPro">
                    <tr>
                      <td width="30"><div class="statusProject trafic"></div></td>
                      <td> En tráfico</td>
                    </tr>
                    <tr>
                      <td><div class="statusProject cancel"></div></td>
                      <td> Cancelado</td>
                    </tr>
                  </table>
                </div>
              </div>
              <br>
              <div class="textTitle">Filtro</div>
              <br>
              <div class="formFilter">
                <div class="form-group">
                  <div class="select-style">
                    {!! Form::select('accountProject', $accountsArray, null, ['class' => 'form-control', "id" => "accountProject"]) !!}
                  </div>

                  <br><strong>CUENTA</strong><br>
                  <div class="filterScroll">
                    <ul class="filterAccount">
                      @foreach ($accounts as $account)
                        @if (Request::get('accountProject') == $account->id)
                          <li>
                            <a href="#" class="active linkFilterAccount" data-id="{{ $account->id }}">
                              {{ $account->name }}
                            </a>
                          </li>
                        @else
                          <li>
                            <a href="#" class="linkFilterAccount" data-id="{{ $account->id }}">
                              {{ $account->name }}
                            </a>
                          </li>
                        @endif
                      @endforeach
                    </ul>
                  </div>
                </div>
              </div>
            </div>  
          </div>
          
        </div>
        <div class="col-md-9 mainRightZone">
          <div class="textWelcome">
            PROYECTOS
            <div class="pull-right">
              <a class="btn btn-default btnSiteOrange" href="{{ route('newproject') }}" role="button">NUEVO <i class="fa fa-plus"></i></a>
            </div>
          </div>

          @if (Session::has('message'))
            <p class="alert alert-success">
              {!! Session::get('message') !!}
            </p>
          @endif

          <div class="row">
            <div class="col-md-4 col-md-offset-8">
              <br>
              <table class="table">
                <tr>
                  <td>
                    <div class="form-group">
                      <input type="text" class="form-control" id="searchNameProject" name="searchNameProject" placeholder="Buscar proyecto" value="{{ Request::get('searchNameProject') }}">
                    </div>
                  </td>
                  <td>
                    <button type="submit" class="btn btn-default btnSite2"><i class="fa fa-search"></i></button>
                  </td>
                </tr>
              </table>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table table-hover tableProjects">
                  <tr>
                      <th width="50">Estado</th>
                      <th>Cliente</th>
                      <th>Nombre</th>
                      <th>Cuenta</th>
                      <th width="102"><span class="label label-danger">Pendientes</span></th>
                  </tr>
                   @foreach ($projects as $project)
                    <tr class="{{ ($project->totalOTS > 0) ? 'trStrong' : '' }}">
                      <td><div class="statusProject {{ $project->status }}"></div></td>
                      <td>{{ $project->nameClient }}</td>
                      <td>
                        <a href="{{ route('showproject', $project->id) }}" class="linkSite">
                          {{ $project->name }}
                        </a>
                      </td>
                      <td>{{ $project->nameAccount }}</td>
                      <td align="center">{{ $project->totalOTS }}</td>
                    </tr>
                  @endforeach
                </table>
              </div>
              <div class="pagination">
                {{ $projects->appends(Request::all())->render() }}
              </div>
            </div>
          </div>
        </div>
      </div>
    {{-- </form> --}}
    {!! Form::close() !!}
  </div>
@endsection
